package org.eclipsefoundation.api;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.api.model.ProjectData;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;

import io.quarkus.oidc.client.filter.OidcClientFilter;

@OidcClientFilter
@Path("projects")
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
public interface ProjectAPI {

    @GET
    @RolesAllowed("fdb_read_projects")
    Response getProjects(@BeanParam BaseAPIParameters baseParams, @QueryParam("organizationID") String organizationID);

    @GET
    @RolesAllowed("fdb_read_projects")
    @Path("{projectID}")
    ProjectData getProject(@BeanParam BaseAPIParameters baseParams, @PathParam("projectID") String projectID);

}
