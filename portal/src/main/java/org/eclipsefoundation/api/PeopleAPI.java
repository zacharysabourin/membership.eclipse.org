package org.eclipsefoundation.api;

import java.util.List;

import javax.annotation.Nullable;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import io.quarkus.security.Authenticated;

@OidcClientFilter
@Path("people")
@Authenticated
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
public interface PeopleAPI {

    @GET
    @RolesAllowed("fdb_read_people")
    Response getPeople(@BeanParam BaseAPIParameters baseParams, @BeanParam PeopleRequestParams params);

    @AutoValue
    @JsonDeserialize(builder = AutoValue_PeopleAPI_PeopleRequestParams.Builder.class)
    public abstract static class PeopleRequestParams {
        @Nullable
        @QueryParam("email")
        public abstract String getEmail();

        @Nullable
        @QueryParam("id")
        public abstract String getUsername();

        @Nullable
        @QueryParam("first_name")
        public abstract String getFirstName();

        @Nullable
        @QueryParam("last_name")
        public abstract String getLastName();

        @Nullable
        @QueryParam("organization_id")
        public abstract String getOrganizationID();

        @Nullable
        @QueryParam("project_relation")
        public abstract String getRelation();

        @Nullable
        @QueryParam("ids")
        public abstract List<String> getIds();

        public static Builder builder() {
            return new AutoValue_PeopleAPI_PeopleRequestParams.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setEmail(@Nullable String email);

            public abstract Builder setUsername(@Nullable String username);

            public abstract Builder setFirstName(@Nullable String firstName);

            public abstract Builder setLastName(@Nullable String lastName);

            public abstract Builder setOrganizationID(@Nullable String organizationID);

            public abstract Builder setRelation(@Nullable String relation);

            public abstract Builder setIds(@Nullable List<String> ids);

            public abstract PeopleRequestParams build();
        }
    }

}
