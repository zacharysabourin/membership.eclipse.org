package org.eclipsefoundation.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

import org.eclipsefoundation.core.namespace.UrlParameterNamespace;

@Singleton
public class FoundationDBParameterNames implements UrlParameterNamespace {

    public static final UrlParameter USER_NAME = new UrlParameter("username");
    public static final UrlParameter RELATION = new UrlParameter("relation");
    public static final UrlParameter ORGANIZATION_ID = new UrlParameter("organizationID");
    public static final UrlParameter TYPE = new UrlParameter("type");
    public static final UrlParameter EMAIL = new UrlParameter("email");
    public static final UrlParameter LEVELS = new UrlParameter("levels");
    public static final UrlParameter WORKING_GROUP = new UrlParameter("working_group");
    public static final UrlParameter DOCUMENT_IDS = new UrlParameter("documentIDs");

    private static final List<UrlParameter> params = Collections.unmodifiableList(
            Arrays.asList(USER_NAME, RELATION, ORGANIZATION_ID, TYPE, LEVELS, EMAIL, WORKING_GROUP, DOCUMENT_IDS));

    @Override
    public List<UrlParameter> getParameters() {
        return new ArrayList<>(params);
    }

}
