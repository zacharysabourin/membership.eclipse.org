package org.eclipsefoundation.api.model;

import java.util.Date;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_PeopleProjectData.Builder.class)
public abstract class PeopleProjectData {
    public abstract String getProjectID();

    public abstract String getPersonID();

    public abstract String getRelation();

    public abstract Date getActiveDate();
    @Nullable
    public abstract Date getInactiveDate();

    public abstract boolean getEditBugs();

    public static Builder builder() {
        return new AutoValue_PeopleProjectData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setProjectID(String projectID);

        public abstract Builder setPersonID(String personID);

        public abstract Builder setRelation(String relation);

        public abstract Builder setActiveDate(Date activeDate);

        public abstract Builder setInactiveDate(@Nullable Date inactiveDate);

        public abstract Builder setEditBugs(boolean editBugs);

        public abstract PeopleProjectData build();
    }
}
