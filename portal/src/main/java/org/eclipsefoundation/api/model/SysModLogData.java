package org.eclipsefoundation.api.model;

import java.time.ZonedDateTime;
import java.util.Date;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_SysModLogData.Builder.class)
public abstract class SysModLogData {
    @Nullable
    public abstract Integer getLogID();

    public abstract String getLogTable();

    public abstract String getPK1();

    @Nullable
    public abstract String getPK2();

    public abstract String getLogAction();

    public abstract String getPersonId();

    public abstract ZonedDateTime getModDateTime();

    public static Builder builder() {
        return new AutoValue_SysModLogData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setLogID(@Nullable Integer logID);

        public abstract Builder setLogTable(String logTable);

        public abstract Builder setPK1(String pK1);

        public abstract Builder setPK2(@Nullable String pK2);

        public abstract Builder setLogAction(String logAction);

        public abstract Builder setPersonId(String personId);

        public abstract Builder setModDateTime(ZonedDateTime modDateTime);

        public abstract SysModLogData build();
    }
}
