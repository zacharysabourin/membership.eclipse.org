package org.eclipsefoundation.api.model;

import java.util.Date;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_OrganizationAddressData.Builder.class)
public abstract class OrganizationAddressData {

    public abstract int getOrganizationID();
    public abstract int getAddressID();
    @Nullable
    public abstract String getAddress1();
    @Nullable
    public abstract String getAddress2();
    @Nullable
    public abstract String getAddress3();
    @Nullable
    public abstract String getCity();
    @Nullable
    public abstract String getProvStateRegion();
    @Nullable
    public abstract String getPostalCode();
    @Nullable
    public abstract String getCCode();
    @Nullable
    public abstract Date getTimestamp();

    public static Builder builder() {
        return new AutoValue_OrganizationAddressData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setOrganizationID(int organizationID);
        public abstract Builder setAddressID(int addressID);
        public abstract Builder setAddress1(@Nullable String address1);
        public abstract Builder setAddress2(@Nullable String address2);
        public abstract Builder setAddress3(@Nullable String address3);
        public abstract Builder setCity(@Nullable String city);
        public abstract Builder setProvStateRegion(@Nullable String provStateRegion);
        public abstract Builder setPostalCode(@Nullable String postalCode);
        public abstract Builder setCCode(@Nullable String cCode);
        public abstract Builder setTimestamp(@Nullable Date timestamp);
        public abstract OrganizationAddressData build();
    }
}
