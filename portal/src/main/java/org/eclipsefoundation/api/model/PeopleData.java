package org.eclipsefoundation.api.model;

import java.util.Date;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_PeopleData.Builder.class)
public abstract class PeopleData {

    public abstract String getPersonID();

    public abstract String getFname();

    public abstract String getLname();

    public abstract String getType();

    public abstract boolean isMember();

    public abstract String getEmail();

    @Nullable
    public abstract String getPhone();

    @Nullable
    public abstract String getFax();

    @Nullable
    public abstract String getMobile();

    @Nullable
    public abstract String getComments();

    @Nullable
    public abstract String getProvisioning();

    public abstract boolean isUnixAcctCreated();

    public abstract boolean getIssuesPending();

    @Nullable
    public abstract String getMemberPassword();

    @Nullable
    public abstract Date getMemberSince();

    @Nullable
    public abstract String getLatitude();

    @Nullable
    public abstract String getLongitude();

    @Nullable
    public abstract String getBlog();

    @Nullable
    public abstract String getScrmGuid();

    public static Builder builder() {
        return new AutoValue_PeopleData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setPersonID(String personID);

        public abstract Builder setFname(String fname);

        public abstract Builder setLname(String lname);

        public abstract Builder setType(String type);

        public abstract Builder setMember(boolean member);

        public abstract Builder setEmail(String email);

        public abstract Builder setPhone(@Nullable String phone);

        public abstract Builder setFax(@Nullable String fax);

        public abstract Builder setMobile(@Nullable String mobile);

        public abstract Builder setComments(@Nullable String comments);

        public abstract Builder setProvisioning(@Nullable String provisioning);

        public abstract Builder setUnixAcctCreated(boolean unixAcctCreated);

        public abstract Builder setIssuesPending(boolean issuesPending);

        public abstract Builder setMemberPassword(@Nullable String memberPassword);

        public abstract Builder setMemberSince(@Nullable Date memberSince);

        public abstract Builder setLatitude(@Nullable String latitude);

        public abstract Builder setLongitude(@Nullable String longitude);

        public abstract Builder setBlog(@Nullable String blog);

        public abstract Builder setScrmGuid(@Nullable String scrmGuid);

        public abstract PeopleData build();
    }
}
