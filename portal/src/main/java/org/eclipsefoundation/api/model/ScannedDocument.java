package org.eclipsefoundation.api.model;

import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

public abstract class ScannedDocument {
    public abstract double getVersion();
    public abstract Date getReceivedDate();
    @Nullable
    public abstract Date getExpirationDate();
    @Nullable
    public abstract List<Byte> getScannedDocumentBLOB();
    @Nullable
    public abstract String getScannedDocumentMime();
    @Nullable
    public abstract Integer getScannedDocumentBytes();
    @Nullable
    public abstract String getScannedDocumentFileName();
    @Nullable
    public abstract String getComments();

    public abstract static class Builder<T extends Builder<T>> {
        public abstract T setVersion(double version);
        public abstract T setReceivedDate(Date receivedDate);
        public abstract T setExpirationDate(@Nullable Date expirationDate);
        public abstract T setScannedDocumentBLOB(@Nullable List<Byte> scannedDocumentBLOB);
        public abstract T setScannedDocumentMime(@Nullable String scannedDocumentMime);
        public abstract T setScannedDocumentBytes(@Nullable Integer scannedDocumentBytes);
        public abstract T setScannedDocumentFileName(@Nullable String scannedDocumentFileName);
        public abstract T setComments(@Nullable String comments);
    }
}
