package org.eclipsefoundation.api.model;

import java.util.Date;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

import io.quarkus.runtime.annotations.RegisterForReflection;

@AutoValue
@RegisterForReflection
@JsonDeserialize(builder = AutoValue_OrganizationData.Builder.class)
public abstract class OrganizationData {

    public abstract int getOrganizationID();
    @Nullable
    public abstract String getName1();
    @Nullable
    public abstract String getName2();
    @Nullable
    public abstract String getPhone();
    @Nullable
    public abstract String getFax();
    @Nullable
    public abstract String getComments();
    @Nullable
    public abstract Date getMemberSince();
    @Nullable
    public abstract String getLatLong();
    @Nullable
    public abstract String getScrmGUID();
    @Nullable
    public abstract Date getTimestamp();

    public static Builder build() {
        return new AutoValue_OrganizationData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setOrganizationID(int organizationID);
        public abstract Builder setName1(@Nullable String name1);
        public abstract Builder setName2(@Nullable String name2);
        public abstract Builder setPhone(@Nullable String phone);
        public abstract Builder setFax(@Nullable String fax);
        public abstract Builder setComments(@Nullable String comments);
        public abstract Builder setMemberSince(@Nullable Date memberSince);
        public abstract Builder setLatLong(@Nullable String latLong);
        public abstract Builder setScrmGUID(@Nullable String scrmGUID);
        public abstract Builder setTimestamp(@Nullable Date timestamp);
        public abstract OrganizationData build();
    }
}
