package org.eclipsefoundation.api.model;

import java.util.Date;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_OrganizationMembershipData.Builder.class)
public abstract class OrganizationMembershipData {
    public abstract int getOrganizationID();

    public abstract String getRelation();

    @Nullable
    public abstract Date getEntryDate();

    @Nullable
    public abstract Date getExpiryDate();

    @Nullable
    public abstract String getComments();

    @Nullable
    public abstract Boolean getIssuesPending();

    public abstract int getDuesTier();

    @Nullable
    public abstract String getRenewalProb();

    @Nullable
    public abstract Integer getInvoiceMonth();

    public static Builder builder() {
        return new AutoValue_OrganizationMembershipData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setOrganizationID(int organizationID);

        public abstract Builder setRelation(String relation);

        public abstract Builder setEntryDate(@Nullable Date entryDate);

        public abstract Builder setExpiryDate(@Nullable Date expiryDate);

        public abstract Builder setComments(@Nullable String comments);

        public abstract Builder setIssuesPending(@Nullable Boolean issuesPending);

        public abstract Builder setDuesTier(int duesTier);

        public abstract Builder setRenewalProb(@Nullable String renewalProb);

        public abstract Builder setInvoiceMonth(@Nullable Integer invoiceMonth);

        public abstract OrganizationMembershipData build();
    }
}
