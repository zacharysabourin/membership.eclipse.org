package org.eclipsefoundation.api.model;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_EnhancedOrganizationContactData.Builder.class)
public abstract class EnhancedOrganizationContactData {

    public abstract int getOrganizationId();
    public abstract PeopleData getPerson();
    public abstract List<SysRelationData> getRelations();

    public static Builder builder() {
        return new AutoValue_EnhancedOrganizationContactData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setOrganizationId(int organizationId);
        public abstract Builder setPerson(PeopleData name);
        public abstract Builder setRelations(List<SysRelationData> relations);
        public abstract EnhancedOrganizationContactData build();
    }
}
