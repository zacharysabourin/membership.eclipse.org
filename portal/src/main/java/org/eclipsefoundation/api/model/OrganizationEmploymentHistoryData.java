package org.eclipsefoundation.api.model;

import java.util.Date;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_OrganizationEmploymentHistoryData.Builder.class)
public abstract class OrganizationEmploymentHistoryData {

    public abstract int getOrganizationID();
    public abstract String getPersonID();
    public abstract Date getEffectiveDate();
    @Nullable
    public abstract Date getExpirationDate();

    public static Builder builder() {
        return new AutoValue_OrganizationEmploymentHistoryData.Builder();
    }

    
    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setOrganizationID(int organizationID);
        public abstract Builder setPersonID(String personID);
        public abstract Builder setEffectiveDate(Date effectiveDate);
        public abstract Builder setExpirationDate(@Nullable Date expirationDate);
        public abstract OrganizationEmploymentHistoryData build();
    }
}
