package org.eclipsefoundation.api.model;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_OrganizationContactData.Builder.class)
public abstract class OrganizationContactData {

    public abstract int getOrganizationID();
    public abstract String getPersonID();
    public abstract String getRelation();
    @Nullable
    public abstract String getComments();
    @Nullable
    public abstract String getTitle();

    public static Builder builder() {
        return new AutoValue_OrganizationContactData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setOrganizationID(int organizationID);
        public abstract Builder setPersonID(String personID);
        public abstract Builder setRelation(String relation);
        public abstract Builder setComments(@Nullable String comments);
        public abstract Builder setTitle(@Nullable String title);
        public abstract OrganizationContactData build();
    }
}
