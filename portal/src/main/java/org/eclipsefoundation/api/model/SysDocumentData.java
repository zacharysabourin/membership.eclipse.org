package org.eclipsefoundation.api.model;

import java.util.List;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_SysDocumentData.Builder.class)
public abstract class SysDocumentData {

    public abstract String getDocumentID();

    public abstract double getVersion();
    @Nullable
    public abstract List<Byte> getDocumentBLOB();

    public abstract boolean isActive();

    public abstract String getDescription();

    public abstract String getComments();

    public abstract String getType();

    public abstract String getContentType();

    public static Builder builder() {
        return new AutoValue_SysDocumentData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setDocumentID(String documentID);

        public abstract Builder setVersion(double version);

        public abstract Builder setDocumentBLOB(@Nullable List<Byte> documentBLOB);

        public abstract Builder setActive(boolean isActive);

        public abstract Builder setDescription(String description);

        public abstract Builder setComments(String comments);

        public abstract Builder setType(String type);

        public abstract Builder setContentType(String contentType);

        public abstract SysDocumentData build();
    }
}
