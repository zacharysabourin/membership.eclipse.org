package org.eclipsefoundation.api.model;

import java.util.Date;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_OrganizationRelationData.Builder.class)
public abstract class OrganizationRelationData {
    public abstract int getOrganizationID();

    public abstract String getRelation();

    @Nullable
    public abstract Date getEntryDate();

    public static Builder builder() {
        return new AutoValue_OrganizationRelationData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setOrganizationID(int organizationID);

        public abstract Builder setRelation(String relation);

        public abstract Builder setEntryDate(@Nullable Date entryDate);

        public abstract OrganizationRelationData build();
    }
}
