package org.eclipsefoundation.api.model;

import java.util.Date;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_OrganizationDocumentData.Builder.class)
public abstract class OrganizationDocumentData extends ScannedDocument {
    public abstract int getOrganizationID();
    public abstract String getDocumentID();
    public abstract Date getEffectiveDate();
    @Nullable
    public abstract Float getDues();
    @Nullable
    public abstract String getCurrency();
    @Nullable
    public abstract String getRelation();
    @Nullable
    public abstract Integer getInvoiceMonth();

    public static Builder builder() {
        return new AutoValue_OrganizationDocumentData.Builder();
    }
    
    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder extends ScannedDocument.Builder<Builder> {
        public abstract Builder setOrganizationID(int organizationID);
        public abstract Builder setDocumentID(String documentID);
        public abstract Builder setEffectiveDate(Date effectiveDate);
        public abstract Builder setDues(@Nullable Float dues);
        public abstract Builder setCurrency(@Nullable String currency);
        public abstract Builder setRelation(@Nullable String relation);
        public abstract Builder setInvoiceMonth(@Nullable Integer invoiceMonth);
        public abstract OrganizationDocumentData build();
    }
}
