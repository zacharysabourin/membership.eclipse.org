package org.eclipsefoundation.api.model;

import java.util.List;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_MemberOrganizationsData.Builder.class)
public abstract class MemberOrganizationsData {
    public abstract int getOrganizationId();

    public abstract String getName();

    public abstract List<SysRelationData> getRelations();

    public abstract List<MemberDocument> getDocuments();

    public static Builder builder() {
        return new AutoValue_MemberOrganizationsData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setOrganizationId(int organizationID);

        public abstract Builder setName(String name);

        public abstract Builder setRelations(List<SysRelationData> relations);

        public abstract Builder setDocuments(List<MemberDocument> documents);

        public abstract MemberOrganizationsData build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_MemberOrganizationsData_MemberDocument.Builder.class)
    public abstract static class MemberDocument {
        public abstract String getDocumentId();

        @Nullable
        public abstract String getRelation();

        public static Builder builder() {
            return new AutoValue_MemberOrganizationsData_MemberDocument.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setDocumentId(String documentId);

            public abstract Builder setRelation(@Nullable String relation);

            public abstract MemberDocument build();
        }
    }
}
