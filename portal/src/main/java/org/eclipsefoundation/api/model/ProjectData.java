package org.eclipsefoundation.api.model;

import java.util.Date;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ProjectData.Builder.class)
public abstract class ProjectData {
    public abstract String getProjectID();

    public abstract String getName();

    public abstract int getLevel();

    public abstract String getParentProjectID();

    public abstract String getDescription();

    public abstract String getUrlDownload();

    public abstract String getUrlIndex();

    @Nullable
    public abstract Date getDateActive();

    public abstract int getSortOrder();

    public abstract boolean isActive();
    @Nullable
    public abstract String getBugsName();

    @Nullable
    public abstract String getProjectPhase();

    public abstract float getDiskQuotaGB();

    public abstract boolean isComponent();

    public abstract boolean isStandard();

    public static Builder builder() {
        return new AutoValue_ProjectData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setProjectID(String projectID);

        public abstract Builder setName(String name);

        public abstract Builder setLevel(int level);

        public abstract Builder setParentProjectID(String parentProjectID);

        public abstract Builder setDescription(String description);

        public abstract Builder setUrlDownload(String urlDownload);

        public abstract Builder setUrlIndex(String urlIndex);

        public abstract Builder setDateActive(@Nullable Date dateActive);

        public abstract Builder setSortOrder(int sortOrder);

        public abstract Builder setActive(boolean isActive);

        public abstract Builder setBugsName(@Nullable String bugsName);

        public abstract Builder setProjectPhase(@Nullable String projectPhase);

        public abstract Builder setDiskQuotaGB(float diskQuotaGB);

        public abstract Builder setComponent(boolean isComponent);

        public abstract Builder setStandard(boolean isStandard);

        public abstract ProjectData build();
    }
}
