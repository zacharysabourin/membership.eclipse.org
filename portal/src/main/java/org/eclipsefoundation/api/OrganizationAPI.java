package org.eclipsefoundation.api;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.BeanParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.api.model.OrganizationContactData;
import org.eclipsefoundation.api.model.OrganizationData;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import io.quarkus.security.Authenticated;

@OidcClientFilter
@Authenticated
@Path("organizations")
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
public interface OrganizationAPI {

    @GET
    @RolesAllowed("fdb_read_organization")
    Response getOrganizations(@BeanParam BaseAPIParameters baseParams, @BeanParam OrganizationRequestParams params);

    @GET
    @Path("{id}")
    @RolesAllowed("fdb_read_organization")
    OrganizationData getOrganization(@PathParam("id") String id);

    @GET
    @Path("{id}/memberships")
    @RolesAllowed("fdb_read_organization")
    Response getOrganizationMembership(@PathParam("id") String id, @BeanParam BaseAPIParameters baseParams,
            @BeanParam OrganizationRequestParams params);

    @GET
    @Path("contacts")
    @RolesAllowed("fdb_read_organization_employment")
    Response getOrganizationContacts(@BeanParam BaseAPIParameters baseParams,
            @BeanParam OrganizationRequestParams params);

    @GET
    @Path("{id}/contacts")
    @RolesAllowed("fdb_read_organization_employment")
    Response getOrganizationContactsWithSearch(@PathParam("id") String id, @BeanParam BaseAPIParameters baseParams,
            @BeanParam OrganizationRequestParams params);

    @PUT
    @Path("{id}/contacts")
    @RolesAllowed("fdb_write_organization_employment")
    OrganizationContactData updateOrganizationContacts(@PathParam("id") String id, OrganizationContactData contact);

    @GET
    @Path("{id}/contacts/{personID}")
    @RolesAllowed("fdb_read_organization_employment")
    Response getOrganizationContact(@PathParam("id") String id, @PathParam("personID") String personID,
            @BeanParam BaseAPIParameters baseParams, @BeanParam OrganizationRequestParams params);

    @DELETE
    @Path("{id}/contacts/{personID}/{relation}")
    @RolesAllowed("fdb_delete_organization_employment")
    Response removeOrganizationContacts(@PathParam("id") String id, @PathParam("personID") String personID,
            @PathParam("relation") String relation);

    @GET
    @Path("{id}/documents")
    @RolesAllowed("fdb_read_organization_documents")
    Response getOrganizationDocuments(@PathParam("id") String id, @BeanParam BaseAPIParameters baseParams,
            @BeanParam OrganizationRequestParams params);

    @GET
    @Path("{id}/employment_histories")
    @RolesAllowed("fdb_read_organization_employment")
    Response getOrganizationEmploymentHistory(@PathParam("id") String id, @BeanParam BaseAPIParameters baseParams);

    @AutoValue
    @JsonDeserialize(builder = AutoValue_OrganizationAPI_OrganizationRequestParams.Builder.class)
    public abstract static class OrganizationRequestParams {

        @Nullable
        @QueryParam("id")
        public abstract String getOrganizationID();

        @Nullable
        @QueryParam("personID")
        public abstract String getPersonID();

        @Nullable
        @QueryParam("email")
        public abstract String getEmail();

        @Nullable
        @QueryParam("fName")
        public abstract String getFirstName();

        @Nullable
        @QueryParam("lName")
        public abstract String getLastName();

        @Nullable
        @QueryParam("relation")
        public abstract String getRelation();

        @Nullable
        @QueryParam("working_group")
        public abstract String getWorkingGroup();

        @Nullable
        @QueryParam("is_not_expired")
        public abstract Boolean getIsNotExpired();

        @Nullable
        @QueryParam("ids")
        public abstract List<String> getIds();

        @Nullable
        @QueryParam("documentIDs")
        public abstract List<String> getDocumentIds();

        @Nullable
        @QueryParam("levels")
        public abstract List<String> getLevels();

        public static Builder builder() {
            return new AutoValue_OrganizationAPI_OrganizationRequestParams.Builder().setIsNotExpired(true);
        }

        /**
         * Checks the internal parameters to check if the parameters are all empty.
         * 
         * @return true if no params are set, false otherwise
         */
        public boolean isEmpty() {
            return StringUtils.isBlank(getEmail()) && StringUtils.isBlank(getFirstName())
                    && StringUtils.isBlank(getLastName()) && StringUtils.isBlank(getOrganizationID())
                    && StringUtils.isBlank(getPersonID()) && StringUtils.isBlank(getRelation())
                    && (getIds() == null || getIds().isEmpty())
                    && (getDocumentIds() == null || getDocumentIds().isEmpty())
                    && (getLevels() == null || getLevels().isEmpty());
        }

        /**
         * Generate a parameter map from the current request, passing through any set parameters to the bean mapping.
         * 
         * @param w the wrapper for the current request.
         * @return a bean mapping with values populated from the current request
         */
        public static Builder populateFromWrap(RequestWrapper w) {
            // default levels for membership calls
            List<String> levels = w.getParams(FoundationDBParameterNames.LEVELS);
            return builder().setPersonID(w.getFirstParam(FoundationDBParameterNames.USER_NAME).orElseGet(() -> null))
                    .setOrganizationID(
                            w.getFirstParam(FoundationDBParameterNames.ORGANIZATION_ID).orElseGet(() -> null))
                    .setRelation(w.getFirstParam(FoundationDBParameterNames.RELATION).orElseGet(() -> null))
                    .setEmail(w.getFirstParam(FoundationDBParameterNames.EMAIL).orElseGet(() -> null))
                    .setWorkingGroup(w.getFirstParam(FoundationDBParameterNames.WORKING_GROUP).orElseGet(() -> null))
                    .setDocumentIds(w.getParams(FoundationDBParameterNames.DOCUMENT_IDS))
                    .setLevels(levels != null && !levels.isEmpty() ? levels : Arrays.asList("SD", "AP", "AS", "OHAP"))
                    .setIds(w.getParams(DefaultUrlParameterNames.IDS));
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            @QueryParam("id")
            public abstract Builder setOrganizationID(@Nullable String organizationID);

            @QueryParam("personID")
            public abstract Builder setPersonID(@Nullable String personID);

            @QueryParam("email")
            public abstract Builder setEmail(@Nullable String email);

            @QueryParam("fName")
            public abstract Builder setFirstName(@Nullable String firstName);

            @QueryParam("lName")
            public abstract Builder setLastName(@Nullable String lastName);

            @QueryParam("relation")
            public abstract Builder setRelation(@Nullable String relation);

            @QueryParam("working_group")
            public abstract Builder setWorkingGroup(@Nullable String workingGroup);

            @QueryParam("ids")
            public abstract Builder setIds(@Nullable List<String> ids);

            @QueryParam("documentIDs")
            public abstract Builder setDocumentIds(@Nullable List<String> documentIds);

            @QueryParam("levels")
            public abstract Builder setLevels(@Nullable List<String> levels);

            @QueryParam("is_not_expired")
            public abstract Builder setIsNotExpired(@Nullable Boolean isNotExpired);

            public abstract OrganizationRequestParams build();
        }
    }
}
