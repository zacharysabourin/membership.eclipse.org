package org.eclipsefoundation.api;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.api.OrganizationAPI.OrganizationRequestParams;
import org.eclipsefoundation.api.model.MemberOrganizationsData;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import io.quarkus.security.Authenticated;

@Path("views")
@Authenticated
@OidcClientFilter
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
public interface ComplexAPI {

    @GET
    @Path("members")
    @RolesAllowed({ "fdb_read_organization", "fdb_read_organization_documents" })
    public Response getMembers(@BeanParam BaseAPIParameters params, @BeanParam OrganizationRequestParams orgParams);

    @GET
    @Path("members")
    @RolesAllowed({ "fdb_read_organization", "fdb_read_organization_documents" })
    public List<MemberOrganizationsData> getMember(@QueryParam("id") String id);

    @GET
    @Path("contacts")
    @RolesAllowed({ "fdb_read_organization_employment", "fdb_read_people" })
    public Response getContacts(@BeanParam BaseAPIParameters params, @QueryParam("organization_id") String orgId,
            @QueryParam("person_id") String personId);
}
