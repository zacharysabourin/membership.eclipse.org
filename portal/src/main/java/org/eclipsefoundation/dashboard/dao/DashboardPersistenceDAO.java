package org.eclipsefoundation.dashboard.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.eclipsefoundation.persistence.dao.impl.BaseHibernateDao;

/**
 * Allows for Dashboard entities to be retrieved.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class DashboardPersistenceDAO extends BaseHibernateDao {

    @Named("dashboard")
    @Inject
    EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
}
