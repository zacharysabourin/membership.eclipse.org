package org.eclipsefoundation.dashboard.namespaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

import org.eclipsefoundation.core.namespace.UrlParameterNamespace;

@Singleton
public class DashboardParameterNames implements UrlParameterNamespace {

    public static final UrlParameter LOGIN = new UrlParameter("login");
    public static final UrlParameter ORGANIZATION_ID = new UrlParameter("organization_id");
    public static final UrlParameter PROJECT = new UrlParameter("project");
    public static final UrlParameter PERIOD = new UrlParameter("period");
    public static final UrlParameter FROM_PERIOD = new UrlParameter("from_period");
    public static final UrlParameter HAS_ORGANIZATION = new UrlParameter("has_organization");

    private static final List<UrlParameter> params = Collections
            .unmodifiableList(Arrays.asList(LOGIN, PROJECT, PERIOD, ORGANIZATION_ID, FROM_PERIOD, HAS_ORGANIZATION));

    @Override
    public List<UrlParameter> getParameters() {
        return new ArrayList<>(params);
    }

}
