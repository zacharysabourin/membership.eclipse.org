package org.eclipsefoundation.dashboard.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

/**
 * CommitterAffiliation table record. As this is never read in directly as an entity, no filters have been created.
 * 
 * @author Martin Lowe
 *
 */
@Table
@Entity
public class CommitterAffiliation extends BareNode implements Serializable {
    public static final DtoTable TABLE = new DtoTable(CommitterAffiliation.class, "ca");

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Id
    @ManyToOne
    @JoinColumn(name = "id")
    private Committer committer;
    private Integer orgId;
    private String orgName;
    private Date activeDate;
    private Date inactiveDate;
    private Date ts;

    @Override
    public Committer getId() {
        return getCommitter();
    }

    public Committer getCommitter() {
        return this.committer;
    }

    /**
     * @param committer the id to set
     */
    public void setCommitter(Committer committer) {
        this.committer = committer;
    }

    /**
     * @return the orgId
     */
    public Integer getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    /**
     * @return the orgName
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * @param orgName the orgName to set
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * @return the activeDate
     */
    public Date getActiveDate() {
        return activeDate;
    }

    /**
     * @param activeDate the activeDate to set
     */
    public void setActiveDate(Date activeDate) {
        this.activeDate = activeDate;
    }

    /**
     * @return the inactiveDate
     */
    public Date getInactiveDate() {
        return inactiveDate;
    }

    /**
     * @param inactiveDate the inactiveDate to set
     */
    public void setInactiveDate(Date inactiveDate) {
        this.inactiveDate = inactiveDate;
    }

    /**
     * @return the ts
     */
    public Date getTs() {
        return ts;
    }

    /**
     * @param ts the ts to set
     */
    public void setTs(Date ts) {
        this.ts = ts;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(activeDate, committer, inactiveDate, orgId, orgName, ts);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        CommitterAffiliation other = (CommitterAffiliation) obj;
        return Objects.equals(activeDate, other.activeDate) && Objects.equals(committer, other.committer)
                && Objects.equals(inactiveDate, other.inactiveDate) && Objects.equals(orgId, other.orgId)
                && Objects.equals(orgName, other.orgName) && Objects.equals(ts, other.ts);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CommitterAffiliation [committer=");
        builder.append(committer);
        builder.append(", orgId=");
        builder.append(orgId);
        builder.append(", orgName=");
        builder.append(orgName);
        builder.append(", activeDate=");
        builder.append(activeDate);
        builder.append(", inactiveDate=");
        builder.append(inactiveDate);
        builder.append(", ts=");
        builder.append(ts);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class CommitterAffilliationFilter implements DtoFilter<CommitterAffiliation> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            return builder.build(TABLE);
        }

        @Override
        public Class<CommitterAffiliation> getType() {
            return CommitterAffiliation.class;
        }
    }
}
