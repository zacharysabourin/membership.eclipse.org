package org.eclipsefoundation.dashboard.dto;

import java.util.Date;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

@Entity
@Table
public class Project extends BareNode {
    public static final DtoTable TABLE = new DtoTable(Project.class, "p");
    @Id
    private String id;
    private String parentId;
    @Column(name = "short")
    private String shortID;
    private String name;
    private String license;
    private String specification;
    private String url;
    private Date start;

    @Override
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the parendId
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * @param parentId the parendId to set
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the shortID
     */
    public String getShortID() {
        return shortID;
    }

    /**
     * @param shortID the shortID to set
     */
    public void setShortID(String shortID) {
        this.shortID = shortID;
    }

    /**
     * @return the license
     */
    public String getLicense() {
        return license;
    }

    /**
     * @param license the license to set
     */
    public void setLicense(String license) {
        this.license = license;
    }

    /**
     * @return the specification
     */
    public String getSpecification() {
        return specification;
    }

    /**
     * @param specification the specification to set
     */
    public void setSpecification(String specification) {
        this.specification = specification;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the start
     */
    public Date getStart() {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart(Date start) {
        this.start = start;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(id, license, name, parentId, shortID, specification, start, url);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Project other = (Project) obj;
        return Objects.equals(id, other.id) && Objects.equals(license, other.license)
                && Objects.equals(name, other.name) && Objects.equals(parentId, other.parentId)
                && Objects.equals(shortID, other.shortID) && Objects.equals(specification, other.specification)
                && Objects.equals(start, other.start) && Objects.equals(url, other.url);
    }


    @Singleton
    public static class ProjectFilter implements DtoFilter<Project> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            return builder.build(TABLE);
        }
        @Override
        public Class<Project> getType() {
            return Project.class;
        }
    }
}
