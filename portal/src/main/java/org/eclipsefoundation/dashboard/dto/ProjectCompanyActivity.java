package org.eclipsefoundation.dashboard.dto;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.dashboard.namespaces.DashboardParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

@Table
@Entity
public class ProjectCompanyActivity extends BareNode {
    public static final DtoTable TABLE = new DtoTable(ProjectCompanyActivity.class, "pca");

    @EmbeddedId
    private ProjectCompanyActivityCompositeId compositeId;
    private String company;
    private int count;

    /**
     * @return the id
     */
    @Override
    public ProjectCompanyActivityCompositeId getId() {
        return getCompositeId();
    }

    /**
     * @return the compositeId
     */
    public ProjectCompanyActivityCompositeId getCompositeId() {
        return compositeId;
    }

    /**
     * @param compositeId the compositeId to set
     */
    public void setCompositeId(ProjectCompanyActivityCompositeId compositeId) {
        this.compositeId = compositeId;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    @Embeddable
    public static class ProjectCompanyActivityCompositeId implements Serializable {
        @ManyToOne
        @JoinColumn(name = "project")
        private Project project;
        private Integer orgId;

        /**
         * @return the project
         */
        public Project getProject() {
            return project;
        }

        /**
         * @param project the project to set
         */
        public void setProject(Project project) {
            this.project = project;
        }

        /**
         * @return the orgId
         */
        public Integer getOrgId() {
            return orgId;
        }

        /**
         * @param orgId the orgId to set
         */
        public void setOrgId(Integer orgId) {
            this.orgId = orgId;
        }
    }

    @Singleton
    public static class ProjectCompanyActivityFilter implements DtoFilter<ProjectCompanyActivity> {

        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // project check
                String project = params.getFirst(DashboardParameterNames.PROJECT.getName());
                if (project != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(
                            TABLE.getAlias() + ".compositeId.project.id = ?", new Object[] { project }));
                }
                // organization ID check
                String organizationID = params.getFirst(DashboardParameterNames.ORGANIZATION_ID.getName());
                if (organizationID != null) {
                    stmt.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeId.committer.orgId = ?",
                                    new Object[] { Integer.valueOf(organizationID) }));
                }
            }
            String hasOrganization = params.getFirst(DashboardParameterNames.HAS_ORGANIZATION.getName());
            if (Boolean.parseBoolean(hasOrganization)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeId.orgId is not null",
                        new Object[] {}));
            }
            return stmt;
        }

        @Override
        public Class<ProjectCompanyActivity> getType() {
            return ProjectCompanyActivity.class;
        }

    }
}
