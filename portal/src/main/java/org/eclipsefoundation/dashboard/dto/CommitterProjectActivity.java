package org.eclipsefoundation.dashboard.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.dashboard.namespaces.DashboardParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

@Entity
@Table
public class CommitterProjectActivity extends BareNode {
    public static final DtoTable TABLE = new DtoTable(CommitterProjectActivity.class, "cpa");

    @EmbeddedId
    private CommitterProjectActivityID compositeID;
    private int count;

    @Override
    public Object getId() {
        return getCompositeID();
    }

    /**
     * @return the compositeID
     */
    public CommitterProjectActivityID getCompositeID() {
        return compositeID;
    }

    /**
     * @param compositeID the compositeID to set
     */
    public void setCompositeID(CommitterProjectActivityID compositeID) {
        this.compositeID = compositeID;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(compositeID, count);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        CommitterProjectActivity other = (CommitterProjectActivity) obj;
        return Objects.equals(compositeID, other.compositeID) && count == other.count;
    }

    @Embeddable
    public static class CommitterProjectActivityID implements Serializable {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;
        @ManyToOne
        @JoinColumn(name = "login", referencedColumnName = "id")
        private CommitterAffiliation committer;
        @ManyToOne
        @JoinColumn(name = "project")
        private Project project;
        private String period;

        /**
         * @return the committer
         */
        public CommitterAffiliation getCommitter() {
            return committer;
        }

        /**
         * @param committer the committer to set
         */
        public void setCommitter(CommitterAffiliation committer) {
            this.committer = committer;
        }

        /**
         * @return the project
         */
        public Project getProject() {
            return project;
        }

        /**
         * @param project the project to set
         */
        public void setProject(Project project) {
            this.project = project;
        }

        /**
         * @return the period
         */
        public String getPeriod() {
            return period;
        }

        /**
         * @param period the period to set
         */
        public void setPeriod(String period) {
            this.period = period;
        }

        @Override
        public int hashCode() {
            return Objects.hash(committer, period, project);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            CommitterProjectActivityID other = (CommitterProjectActivityID) obj;
            return Objects.equals(committer, other.committer) && Objects.equals(period, other.period)
                    && Objects.equals(project, other.project);
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append("CommitterProjectActivityID [committer=");
            builder.append(committer);
            builder.append(", project=");
            builder.append(project);
            builder.append(", period=");
            builder.append(period);
            builder.append("]");
            return builder.toString();
        }
    }

    @Singleton
    public static class CommitterProjectActivityFilter implements DtoFilter<CommitterProjectActivity> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // login check
                String login = params.getFirst(DashboardParameterNames.LOGIN.getName());
                if (login != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(
                            TABLE.getAlias() + ".compositeID.committer.id = ?", new Object[] { login }));
                }
                // project check
                String project = params.getFirst(DashboardParameterNames.PROJECT.getName());
                if (project != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(
                            TABLE.getAlias() + ".compositeID.project.id = ?", new Object[] { project }));
                }
                // organization ID check
                String organizationID = params.getFirst(DashboardParameterNames.ORGANIZATION_ID.getName());
                if (organizationID != null) {
                    stmt.addClause(
                            new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeID.committer.orgId = ?",
                                    new Object[] { Integer.valueOf(organizationID) }));
                }
                // period check
                String period = params.getFirst(DashboardParameterNames.PERIOD.getName());
                if (period != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeID.period = ?",
                            new Object[] { period }));
                }

                // period after check - will have multiple entries, for each project + login as this isn't a projection
                // that we can add the fields for
                String fromPeriod = params.getFirst(DashboardParameterNames.FROM_PERIOD.getName());
                if (fromPeriod != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeID.period >= ?",
                            new Object[] { fromPeriod }));
                }
            }
            String hasOrganization = params.getFirst(DashboardParameterNames.HAS_ORGANIZATION.getName());
            if (Boolean.parseBoolean(hasOrganization)) {
                stmt.addClause(new ParameterizedSQLStatement.Clause(
                        TABLE.getAlias() + ".compositeID.committer.orgId is not null", new Object[] {}));
            }
            return stmt;
        }

        @Override
        public Class<CommitterProjectActivity> getType() {
            return CommitterProjectActivity.class;
        }

    }
}
