package org.eclipsefoundation.dashboard.model.mapper;

import org.eclipsefoundation.dashboard.dto.CommitterProjectActivity;
import org.eclipsefoundation.dashboard.model.CommitterProjectActivityData;
import org.eclipsefoundation.membership.portal.model.mappers.BaseEntityMapper;
import org.eclipsefoundation.membership.portal.model.mappers.BaseEntityMapper.QuarkusMappingConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = QuarkusMappingConfig.class)
public interface CommitterProjectActivityMapper
        extends BaseEntityMapper<CommitterProjectActivity, CommitterProjectActivityData> {

    @Mapping(source = "compositeID.committer.committer.id", target = "login" )
    @Mapping(source = "compositeID.project.id", target = "project")
    @Mapping(source = "compositeID.period", target = "period")
    CommitterProjectActivityData toModel(CommitterProjectActivity dtoEntity);
}
