package org.eclipsefoundation.dashboard.model;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

import io.quarkus.runtime.annotations.RegisterForReflection;

@AutoValue
@RegisterForReflection
@JsonDeserialize(builder = AutoValue_CommitterProjectActivityData.Builder.class)
public abstract class CommitterProjectActivityData {
    public abstract String getLogin();

    @Nullable
    public abstract String getProject();

    public abstract String getPeriod();

    public abstract int getCount();

    public static Builder builder() {
        return new AutoValue_CommitterProjectActivityData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setLogin(String login);

        public abstract Builder setProject(@Nullable String project);

        public abstract Builder setPeriod(String period);

        public abstract Builder setCount(int count);

        public abstract CommitterProjectActivityData build();

    }
}
