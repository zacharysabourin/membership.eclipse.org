package org.eclipsefoundation.application.dao;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.eclipsefoundation.persistence.dao.impl.BaseHibernateDao;

/**
 * Allows for Dashboard entities to be retrieved.
 * 
 * @author Martin Lowe
 *
 */
@ApplicationScoped
public class REMPersistenceDAO extends BaseHibernateDao {

    @Named("rem")
    @Inject
    EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
}
