package org.eclipsefoundation.membership.portal.namespace;

/**
 * Defined types of contacts that can be added within a form.
 * 
 * @author Martin Lowe
 *
 */
public enum ContactTypes {
    WORKING_GROUP, COMPANY, MARKETING, ACCOUNTING, SIGNING;
}
