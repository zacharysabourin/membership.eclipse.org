/**
 * Copyright (c) 2021 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.resources;

import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CR;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CRA;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.DE;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.EMPLY;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.MA;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.api.FoundationDBParameterNames;
import org.eclipsefoundation.api.SysAPI;
import org.eclipsefoundation.api.model.PeopleData;
import org.eclipsefoundation.api.model.ProjectData;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.dashboard.dao.DashboardPersistenceDAO;
import org.eclipsefoundation.dashboard.dto.CommitterProjectActivity;
import org.eclipsefoundation.dashboard.model.mapper.CommitterProjectActivityMapper;
import org.eclipsefoundation.dashboard.namespaces.DashboardParameterNames;
import org.eclipsefoundation.eclipsedb.dto.OrganizationInformation;
import org.eclipsefoundation.eclipsedb.dto.OrganizationProducts;
import org.eclipsefoundation.eclipsedb.namespaces.EclipseDBParameterNames;
import org.eclipsefoundation.membership.portal.helper.ImageFileHelper;
import org.eclipsefoundation.membership.portal.model.EnhancedPersonData;
import org.eclipsefoundation.membership.portal.model.MemberOrganization;
import org.eclipsefoundation.membership.portal.model.OrganizationActivity;
import org.eclipsefoundation.membership.portal.model.OrganizationProductData;
import org.eclipsefoundation.membership.portal.model.OrganizationYearlyActivity;
import org.eclipsefoundation.membership.portal.model.WorkingGroupMap.WorkingGroup;
import org.eclipsefoundation.membership.portal.model.mappers.OrganizationProductMapper;
import org.eclipsefoundation.membership.portal.namespace.ImageStoreFormat;
import org.eclipsefoundation.membership.portal.namespace.ImageStoreFormat.ImageStoreFormats;
import org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType;
import org.eclipsefoundation.membership.portal.request.RolesAllowed;
import org.eclipsefoundation.membership.portal.request.model.OrganizationInfoUpdateRequest;
import org.eclipsefoundation.membership.portal.request.model.OrganizationLogoUpdateRequest;
import org.eclipsefoundation.membership.portal.service.ImageStoreService;
import org.eclipsefoundation.membership.portal.service.WorkingGroupsService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.Authenticated;
import io.undertow.server.handlers.form.MultiPartParserDefinition.FileTooLargeException;

/**
 * Allows for external organizations data to be retrieved and displayed.
 */
@Path("organizations")
@Produces(MediaType.APPLICATION_JSON)
public class OrganizationResource extends AbstractRESTResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(OrganizationResource.class);

    private static final Pattern PERIOD_DATE_FORMAT = Pattern.compile("^[0-9]{4}(0[0-9]|1[0-2])$");

    @Inject
    DashboardPersistenceDAO dashboardDao;
    @Inject
    DefaultHibernateDao eclipseDBDao;
    @Inject
    WorkingGroupsService wgService;
    @Inject
    ImageStoreService images;

    @RestClient
    @Inject
    SysAPI sysAPI;

    @Inject
    CommitterProjectActivityMapper mapper;
    @Inject
    OrganizationProductMapper productMapper;

    @GET
    public Response getAll(@QueryParam("working_group") String workingGroup, @QueryParam("level") String level) {
        WorkingGroup wg = wgService.getByName(workingGroup);
        if (wg != null) {
            List<String> docids = new ArrayList<>();
            if (wg.getResources().getParticipationAgreements().getIndividual() != null) {
                docids.add(wg.getResources().getParticipationAgreements().getIndividual().getDocumentId());
            }
            if (wg.getResources().getParticipationAgreements().getOrganization() != null) {
                docids.add(wg.getResources().getParticipationAgreements().getOrganization().getDocumentId());
            }
            // if there are no docids, we can never match an org
            if (docids.isEmpty()) {
                return Response.ok(Collections.emptyList()).build();
            }
            wrap.setParam(FoundationDBParameterNames.DOCUMENT_IDS.getName(), docids);
        } else if (wg == null && StringUtils.isNotBlank(workingGroup)) {
            // return empty response if wg was set but no results
            return Response.ok(Collections.emptyList()).build();
        }
        List<MemberOrganization> orgs = orgService.get(wrap);
        if (orgs.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // if we have level and working group set, filter the output
        if (StringUtils.isNotEmpty(workingGroup) && StringUtils.isNotEmpty(level)) {
            orgs = orgs.stream().filter(
                    o -> o.getWgpas().stream().anyMatch(wgpa -> wgpa.getWorkingGroup().equalsIgnoreCase(workingGroup)
                            && wgpa.getLevel().equalsIgnoreCase(level)))
                    .collect(Collectors.toList());
        }
        return Response.ok(new ArrayList<>(orgs)).build();
    }

    @GET
    @Path("{orgID:\\d+}")
    public Response get(@PathParam("orgID") String organizationID) {
        Optional<MemberOrganization> org = orgService.getByID(organizationID);
        if (org.isEmpty()) {
            return Response.status(404).build();
        }
        return Response.ok(org.get()).build();
    }

    @POST
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{orgID:\\d+}")
    public Response update(@PathParam("orgID") String organizationID, OrganizationInfoUpdateRequest updateRequest) {
        if (updateRequest.getDescription().length() > 700) {
            return new org.eclipsefoundation.core.model.Error(400,
                    "Organization description should not be over 700 characters").asResponse();
        }
        // get ref and update the object
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), organizationID);
        List<OrganizationInformation> infoRefs = eclipseDBDao
                .get(new RDBMSQuery<>(wrap, filters.get(OrganizationInformation.class), params));
        // if ref doesn't exist, create one
        OrganizationInformation infoRef;
        if (infoRefs.isEmpty()) {
            infoRef = new OrganizationInformation();
            infoRef.setOrganizationID(Integer.valueOf(organizationID));
        } else {
            infoRef = infoRefs.get(0);
        }
        infoRef.setCompanyUrl(updateRequest.getCompanyUrl());
        infoRef.setLongDescription(updateRequest.getDescription());

        // update the org info
        List<OrganizationInformation> updatedOrg = eclipseDBDao.add(
                new RDBMSQuery<>(wrap, filters.get(OrganizationInformation.class), params), Arrays.asList(infoRef));
        if (updatedOrg.isEmpty()) {
            return Response.status(503).build();
        }
        // clear the caches and return a fresh copy
        cache.fuzzyRemove(organizationID, OrganizationInformation.class);
        cache.fuzzyRemove(organizationID, MemberOrganization.class);
        Optional<MemberOrganization> org = orgService.getByID(organizationID);
        if (org.isEmpty()) {
            return Response.status(503).build();
        }
        modLogHelper.postUpdateModLog(ident, OrganizationInformation.TABLE, organizationID, null);
        return Response.ok(org.get()).build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ EMPLY, CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/activity")
    public Response getPeriodActivity(@PathParam("orgID") String organizationID, @QueryParam("period") String period,
            @QueryParam("from_period") String fromPeriod) {
        List<CommitterProjectActivity> orgCountryActivity = getCommitterActivity(organizationID, period, fromPeriod);
        // if there is no data returned, return a 404
        if (orgCountryActivity.isEmpty()) {
            return Response.status(404).build();
        }
        return Response.ok(orgCountryActivity.stream().map(mapper::toModel).collect(Collectors.toList())).build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ EMPLY, CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/activity/overview")
    public Response getPeriodActivityOverview(@PathParam("orgID") String organizationID,
            @QueryParam("period") String period, @QueryParam("from_period") String fromPeriod) {
        List<CommitterProjectActivity> orgCountryActivity = getCommitterActivity(organizationID, period, fromPeriod);

        // reduce count using sum, and build immutable
        return Response.ok(OrganizationActivity.builder()
                .setCount(orgCountryActivity.stream().reduce(0, (sub, el) -> sub + el.getCount(), Integer::sum))
                .build()).build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ EMPLY, CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/activity/yearly")
    public Response getPeriodActivityYearly(@PathParam("orgID") String organizationID) {
        LocalDate d = LocalDate.now();
        // get the previous year of results
        List<CommitterProjectActivity> orgCountryActivity = getCommitterActivity(organizationID, null,
                getPeriodStringForDate(d.minusYears(1)));

        // group results by the activity period, then reduce the count into a single int result, and transform into an
        // output.
        List<OrganizationActivity> activityList = orgCountryActivity.stream()
                .collect(Collectors.groupingBy((CommitterProjectActivity cpa) -> cpa.getCompositeID().getPeriod()))
                .entrySet().stream()
                .map(e -> OrganizationActivity.builder().setPeriod(e.getKey())
                        .setCount(e.getValue().stream().reduce(0, (sub, el) -> sub + el.getCount(), Integer::sum))
                        .build())
                .collect(Collectors.toList());
        // backfill activity log for 1 year
        activityList = backfillHistoricActivityData(activityList, d);
        // sort the period, sorting from greatest (most recent) to smallest (least recent)
        activityList.sort((o1, o2) -> o2.getPeriod().compareTo(o1.getPeriod()));

        return Response.ok(OrganizationYearlyActivity.builder().setActivity(activityList).build()).build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/committers")
    public Response getCommitters(@PathParam("orgID") String organizationID) {
        Optional<List<PeopleData>> committers = orgService.getCommittersForOrganization(organizationID);
        if (committers.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // get the working groups for each WGPA the org has
        return Response.ok(committers.get()).build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ EMPLY, CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/committers/activity")
    public Response getCommitterActivityOverview(@PathParam("orgID") String organizationID,
            @QueryParam("period") String period, @QueryParam("from_period") String fromPeriod) {
        List<CommitterProjectActivity> orgCountryActivity = getCommitterActivity(organizationID, period, fromPeriod);

        // reduce count using sum, and build immutable
        return Response.ok(OrganizationActivity.builder().setCount(orgCountryActivity.stream()
                .collect(Collectors.groupingBy(
                        (CommitterProjectActivity cpa) -> cpa.getCompositeID().getCommitter().getCommitter().getId()))
                .keySet().size())
                .setPeriod(determinePeriodParameterName(period, fromPeriod).equals(
                        DashboardParameterNames.FROM_PERIOD.getName()) ? fromPeriod : checkPeriodOrDefault(period))
                .build()).build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ EMPLY, CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/committers/activity/yearly")
    public Response getCommitterActivityYearly(@PathParam("orgID") String organizationID) {
        LocalDate d = LocalDate.now();
        // get the previous year of results
        List<CommitterProjectActivity> orgCountryActivity = getCommitterActivity(organizationID, null,
                getPeriodStringForDate(d.minusYears(1)));

        // group results by the activity period, then reduce the count into a single int result, and transform into an
        // output.
        List<OrganizationActivity> activityList = orgCountryActivity.stream()
                .collect(Collectors.groupingBy((CommitterProjectActivity cpa) -> cpa.getCompositeID().getPeriod()))
                .entrySet().stream()
                .map(e -> OrganizationActivity.builder().setPeriod(e.getKey())
                        .setCount(e.getValue().stream()
                                .collect(Collectors.groupingBy((CommitterProjectActivity cpa) -> cpa.getCompositeID()
                                        .getCommitter().getCommitter().getId()))
                                .keySet().size())
                        .build())
                .collect(Collectors.toList());
        // backfill activity log for 1 year
        activityList = backfillHistoricActivityData(activityList, d);
        // sort the period, sorting from greatest (most recent) to smallest (least recent)
        activityList.sort((o1, o2) -> o2.getPeriod().compareTo(o1.getPeriod()));

        return Response.ok(OrganizationYearlyActivity.builder().setActivity(activityList).build()).build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/contributors")
    public Response getContributors(@PathParam("orgID") String organizationID) {
        // get users, where we only care about if they are committers and employees
        List<EnhancedPersonData> people = getEnhancedPeopleDetails(organizationID,
                Arrays.asList(OrganizationalUserType.CM, OrganizationalUserType.EMPLY));
        if (people.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // filter out the people who have the CM role
        return Response
                .ok(people.stream().filter(p -> !p.getRelations().contains(OrganizationalUserType.CM.name())).map(p -> {
                    PeopleData.Builder out = PeopleData.builder().setPersonID(p.getId()).setFname(p.getFirstName())
                            .setLname(p.getLastName()).setIssuesPending(false).setMember(true).setUnixAcctCreated(true)
                            .setType("XX");
                    if (doesUserHaveElevatedAccess(organizationID)) {
                        out.setEmail(p.getEmail());
                    } else {
                        out.setEmail("");
                    }
                    return out.build();
                }).collect(Collectors.toList())).build();
    }

    @GET
    @Path("{orgID:\\d+}/projects")
    public Response getProjects(@PathParam("orgID") String organizationID) {
        Optional<List<ProjectData>> memberOrg = orgService.getProjectsForOrganization(organizationID);
        if (memberOrg.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // get the working groups for each WGPA the org has
        return Response.ok(memberOrg.get()).build();
    }

    @GET
    @Path("{orgID:\\d+}/products")
    public Response getProducts(@PathParam("orgID") String organizationID) {
        // limit results to given org
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(EclipseDBParameterNames.ORGANIZATION_ID.getName(), organizationID);

        List<OrganizationProducts> orgProducts = eclipseDBDao
                .get(new RDBMSQuery<>(wrap, filters.get(OrganizationProducts.class), params));
        if (orgProducts.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        return Response.ok(orgProducts.stream().map(productMapper::toModel).collect(Collectors.toList())).build();
    }

    @POST
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/products")
    public Response createProduct(@PathParam("orgID") String organizationID, OrganizationProductData newProduct) {
        List<OrganizationProducts> orgProducts = eclipseDBDao.add(
                new RDBMSQuery<>(wrap, filters.get(OrganizationProducts.class), new MultivaluedMapImpl<>()),
                Arrays.asList(productMapper.toDTO(newProduct, eclipseDBDao)));
        if (orgProducts.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // track creation/update of product
        Integer pId = orgProducts.get(0).getCompositeId().getProductId();
        modLogHelper.postInsertModLog(ident, OrganizationProducts.TABLE, organizationID,
                pId != null ? Integer.toString(pId) : null);
        return Response.noContent().build();
    }

    @GET
    @Path("{orgID:\\d+}/products/{productID:\\d+}")
    public Response getProduct(@PathParam("orgID") Integer organizationID, @PathParam("productID") Integer productID) {
        // limit results to given org
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(EclipseDBParameterNames.ORGANIZATION_ID.getName(), Integer.toString(organizationID));
        params.add(EclipseDBParameterNames.PRODUCT_ID.getName(), Integer.toString(productID));

        List<OrganizationProducts> orgProducts = eclipseDBDao
                .get(new RDBMSQuery<>(wrap, filters.get(OrganizationProducts.class), params));
        if (orgProducts.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        return Response.ok(productMapper.toModel(orgProducts.get(0))).build();
    }

    @PUT
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/products/{productID:\\d+}")
    public Response updateProducts(@PathParam("orgID") Integer organizationID, OrganizationProductData newProduct,
            @PathParam("productID") Integer productID) {
        List<OrganizationProducts> orgProducts = eclipseDBDao.add(
                new RDBMSQuery<>(wrap, filters.get(OrganizationProducts.class), new MultivaluedMapImpl<>()),
                Arrays.asList(productMapper.toDTO(newProduct, eclipseDBDao)));
        if (orgProducts.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // track creation/update of product
        modLogHelper.postUpdateModLog(ident, OrganizationProducts.TABLE, Integer.toString(organizationID),
                Integer.toString(productID));
        return Response.ok(productMapper.toModel(orgProducts.get(0))).build();
    }

    @DELETE
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{orgID:\\d+}/products/{productID}")
    public Response deleteProduct(@PathParam("orgID") String organizationID, @PathParam("productID") String productID) {
        // limit results to given org + product
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(EclipseDBParameterNames.ORGANIZATION_ID.getName(), organizationID);
        params.add(EclipseDBParameterNames.PRODUCT_ID.getName(), productID);
        eclipseDBDao.delete(new RDBMSQuery<>(wrap, filters.get(OrganizationProducts.class), params));
        modLogHelper.postDeleteModLog(ident, OrganizationProducts.TABLE, organizationID, productID);
        return Response.ok().build();
    }

    @GET
    @Path("{orgID:\\d+}/representatives")
    public Response getRepresentatives(@PathParam("orgID") String organizationID) {
        // don't cache return as results are sensitive to user state.
        List<EnhancedPersonData> people = getEnhancedPeopleDetails(organizationID, Arrays.asList(CR, CRA, MA, DE));
        if (people == null) {
            return Response.status(500).build();
        }
        return Response.ok(people).build();
    }

    @GET
    @Path("{orgID:\\d+}/working_groups")
    public Response getWorkingGroups(@PathParam("orgID") String organizationID) {
        Optional<MemberOrganization> memberOrg = orgService.getByID(organizationID);
        if (memberOrg.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // get the working groups for each WGPA the org has
        return Response.ok(memberOrg.get().getWgpas().stream().map(wgpa -> wgService.getByName(wgpa.getWorkingGroup()))
                .filter(Objects::nonNull).collect(Collectors.toList())).build();
    }

    @POST
    @Authenticated
    @RolesAllowed({ CR, CRA, DE, MA })
    @Path("{orgID:\\d+}/logos")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response postOrganizationLogoUpdate(@PathParam("orgID") String organizationID,
            @MultipartForm OrganizationLogoUpdateRequest request) throws FileTooLargeException {
        // handle writing and checking image data
        ImageStoreFormat format = ImageStoreFormats.getFormat(request.imageFormat);
        String extension = ImageFileHelper.convertMimeType(request.imageMIME);
        if (extension != null && format != null) {
            LOGGER.debug("{} image: {} bytes", format, request.image.length);
            // business logic - only print should use eps
            if (!format.equals(ImageStoreFormats.PRINT) && extension.equalsIgnoreCase("eps")) {
                throw new BadRequestException("EPS format should not be used for non-print logos");
            }
            images.writeImage(() -> request.image, organizationID, request.imageMIME, Optional.of(format));
        }
        // clear the item from the cache
        cache.fuzzyRemove(organizationID, MemberOrganization.class);
        // return the org
        Optional<MemberOrganization> org = orgService.getByID(organizationID);
        if (org.isEmpty()) {
            return Response.status(404).build();
        }
        modLogHelper.postUpdateModLog(ident, OrganizationInformation.TABLE, organizationID, format.getName());
        return Response.ok(org.get()).build();
    }

    /**
     * Retrieves all commit activity count entries for an organization, either for the given period, or from the second
     * passed period.
     * 
     * @param organizationID organization to retrieve entries for
     * @param period the singular period to retrieve entries for (optional)
     * @param fromPeriod the starting period to retrieve entries from (optional)
     * @return list of committer activity entries.
     */
    private List<CommitterProjectActivity> getCommitterActivity(String organizationID, String period,
            String fromPeriod) {
        // set up params for current call
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        // get the period parameter in use (period and from_period are not compatible with each other)
        String paramName = determinePeriodParameterName(period, fromPeriod);
        if (paramName.equals(DashboardParameterNames.FROM_PERIOD.getName())) {
            params.add(paramName, checkPeriodOrDefault(fromPeriod));
        } else {
            params.add(paramName, checkPeriodOrDefault(period));
        }
        params.add(DashboardParameterNames.ORGANIZATION_ID.getName(), organizationID);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Retrieving organization activity with params {}", params);
        }
        RDBMSQuery<CommitterProjectActivity> q = new RDBMSQuery<>(wrap, filters.get(CommitterProjectActivity.class),
                params);
        q.setUseLimit(false);
        // get cached activity as there is no need to refresh it too often.
        return dashboardDao.get(q);
    }

    /**
     * Determine the parameter name based on validity of period values, using the below priority:
     * 
     * period > from period > default (single period, this month)
     * 
     * @param period the proposed period value
     * @param fromPeriod the proposed from period value
     * @return either the {@linkplain DashboardParameterNames.PERIOD} or
     * {@linkplain DashboardParameterNames.FROM_PERIOD} parameter names.
     */
    private String determinePeriodParameterName(String period, String fromPeriod) {
        if (isPeriodFormatValid(period)) {
            return DashboardParameterNames.PERIOD.getName();
        } else if (isPeriodFormatValid(fromPeriod)) {
            return DashboardParameterNames.FROM_PERIOD.getName();
        } else {
            return DashboardParameterNames.PERIOD.getName();
        }
    }

    /**
     * Provides a checked period or the default, which is the current month
     * 
     * @param period the proposed time that should be used. Accurate to the month, in the format YYYYMM
     * @return the passed parameter if valid, otherwise returns default value.
     */
    private String checkPeriodOrDefault(String period) {
        return isPeriodFormatValid(period) ? period : getPeriodStringForDate(LocalDate.now());
    }

    private String getPeriodStringForDate(LocalDate d) {
        // check that month is 2 characters
        String month = Integer.toString(d.getMonthValue());
        if (month.length() == 1) {
            month = "0" + month;
        }
        return Integer.toString(d.getYear()) + month;
    }

    private boolean isPeriodFormatValid(String proposedPeriod) {
        return StringUtils.isNotBlank(proposedPeriod) && PERIOD_DATE_FORMAT.matcher(proposedPeriod).matches();
    }

    private List<OrganizationActivity> backfillHistoricActivityData(List<OrganizationActivity> base, LocalDate from) {
        List<OrganizationActivity> activityList = new ArrayList<>(base);
        LocalDate d = from;
        for (int i = 0; i < 12; i++) {
            // get current period string and update date string
            String currentPeriodString = getPeriodStringForDate(d);
            d = d.minus(1, ChronoUnit.MONTHS);

            // check if we need to back fill this period
            if (activityList.stream().noneMatch(oa -> oa.getPeriod().equals(currentPeriodString))) {
                activityList.add(OrganizationActivity.builder().setCount(0).setPeriod(currentPeriodString).build());
            }
        }
        return activityList;
    }

}
