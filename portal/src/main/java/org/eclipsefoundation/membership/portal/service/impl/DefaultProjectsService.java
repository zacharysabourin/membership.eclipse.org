package org.eclipsefoundation.membership.portal.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.api.OrganizationAPI;
import org.eclipsefoundation.api.ProjectAPI;
import org.eclipsefoundation.api.model.ProjectData;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.dashboard.dao.DashboardPersistenceDAO;
import org.eclipsefoundation.dashboard.dto.ProjectCompanyActivity;
import org.eclipsefoundation.dashboard.namespaces.DashboardParameterNames;
import org.eclipsefoundation.membership.portal.model.MemberOrganization;
import org.eclipsefoundation.membership.portal.service.OrganizationsService;
import org.eclipsefoundation.membership.portal.service.ProjectsService;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class DefaultProjectsService implements ProjectsService {
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultProjectsService.class);

    @Inject
    DashboardPersistenceDAO dashDao;
    @Inject
    CachingService cache;
    @Inject
    FilterService filters;
    @Inject
    OrganizationsService orgService;

    @Inject
    APIMiddleware middleware;
    @RestClient
    @Inject
    OrganizationAPI orgAPI;
    @RestClient
    @Inject
    ProjectAPI projectsAPI;

    @Override
    public List<MemberOrganization> getOrganizationsForProject(String projectId, RequestWrapper wrap) {
        LOGGER.trace(">> getOrganizationsForProject({}, wrap)", projectId);
        if (doesProjectExist(projectId) == null) {
            LOGGER.debug("Project with ID '{}' not found in FoundationDB", projectId);
            LOGGER.trace("<< getOrganizationsForProject({}, wrap)", projectId);
            return null;
        }
        // retrieve activity for a given project within last 3 months
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DashboardParameterNames.PROJECT.getName(), projectId);
        params.add(DashboardParameterNames.HAS_ORGANIZATION.getName(), "true");

        // Use cache to fetch batched results
        LOGGER.debug("Looking up organizations for project '{}'", projectId);
        List<MemberOrganization> out = cache.get(projectId, params, MemberOrganization.class, () -> {
            LOGGER.debug("Fetching fresh organization data for project {}", projectId);
            RDBMSQuery<ProjectCompanyActivity> q = new RDBMSQuery<>(wrap, filters.get(ProjectCompanyActivity.class),
                    params);
            q.setUseLimit(false);
            // retrieve activity for a given project
            List<ProjectCompanyActivity> activity = dashDao.get(q);
            if (activity.isEmpty()) {
                LOGGER.debug("Found no activity associated with project '{}'", projectId);
                LOGGER.trace("<< getOrganizationsForProject({}, wrap)", projectId);
                return new ArrayList<MemberOrganization>();
            }
            // convert the stream activity to the contributing org, dropping any contributions w/o org and deduplicating
            List<String> organizations = activity.stream().map(a -> Integer.toString(a.getCompositeId().getOrgId()))
                    .distinct().collect(Collectors.toList());

            LOGGER.debug("Found {} organizations associated with project {}, fetching actual org data",
                    organizations.size(), projectId);
            // fetch member orgs, and shuffle results for output
            List<MemberOrganization> memberOrgs = new ArrayList<>(orgService.getByIDs(organizations));
            Collections.shuffle(memberOrgs);
            return memberOrgs;
        }).orElse(Collections.emptyList());
        LOGGER.trace("<< getOrganizationsForProject({}, wrap)", projectId);
        return out;
    }

    @Override
    public List<ProjectData> getProjectsForOrganization(int organizationId, RequestWrapper wrap) {
        LOGGER.trace("-- getProjectsForOrganization({}, wrap)", organizationId);
        return cache
                .get(Integer.toString(organizationId), new MultivaluedMapImpl<>(), ProjectData.class, () -> middleware
                        .getAll(i -> projectsAPI.getProjects(i, Integer.toString(organizationId)), ProjectData.class))
                .orElse(Collections.emptyList());
    }

    @Override
    public ProjectData doesProjectExist(String projectId) {
        LOGGER.trace("-- doesProjectExist({})", projectId);
        Optional<ProjectData> project = cache.get(projectId, new MultivaluedMapImpl<>(), ProjectData.class,
                () -> projectsAPI.getProject(BaseAPIParameters.builder().build(), projectId));
        return project.orElse(null);
    }
}
