package org.eclipsefoundation.membership.portal.request.model;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_OrganizationInfoUpdateRequest.Builder.class)
public abstract class OrganizationInfoUpdateRequest {

    public abstract String getDescription();

    @Nullable
    public abstract String getCompanyUrl();

    public static Builder builder() {
        return new AutoValue_OrganizationInfoUpdateRequest.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setDescription(String description);

        public abstract Builder setCompanyUrl(@Nullable String companyUrl);

        public abstract OrganizationInfoUpdateRequest build();
    }

}
