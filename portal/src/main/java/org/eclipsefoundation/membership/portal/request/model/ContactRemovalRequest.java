package org.eclipsefoundation.membership.portal.request.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ContactRemovalRequest.Builder.class)
public abstract class ContactRemovalRequest {

    public abstract String getUsername();

    public abstract Integer getOrganizationId();

    public abstract String getReason();

    public static Builder builder() {
        return new AutoValue_ContactRemovalRequest.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setUsername(String username);

        public abstract Builder setOrganizationId(Integer organizationID);

        public abstract Builder setReason(String reason);

        public abstract ContactRemovalRequest build();
    }
}
