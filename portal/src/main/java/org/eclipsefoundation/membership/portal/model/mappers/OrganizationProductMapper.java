package org.eclipsefoundation.membership.portal.model.mappers;

import org.eclipsefoundation.eclipsedb.dto.OrganizationProducts;
import org.eclipsefoundation.membership.portal.model.OrganizationProductData;
import org.eclipsefoundation.membership.portal.model.mappers.BaseEntityMapper.QuarkusMappingConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = QuarkusMappingConfig.class)
public interface OrganizationProductMapper extends BaseEntityMapper<OrganizationProducts, OrganizationProductData> {

    @Mapping(source = "compositeId.productId", target = "productId")
    @Mapping(source = "compositeId.organizationId", target = "organizationId")
    OrganizationProductData toModel(OrganizationProducts dtoEntity);
}
