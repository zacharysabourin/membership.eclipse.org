package org.eclipsefoundation.membership.portal.request.model;

import javax.annotation.Nonnull;
import javax.ws.rs.FormParam;

public class OrganizationLogoUpdateRequest {

    @FormParam("image")
    @Nonnull
    public byte[] image;
    @FormParam("image_mime")
    @Nonnull
    public String imageMIME;
    @FormParam("image_format")
    @Nonnull
    public String imageFormat;
}
