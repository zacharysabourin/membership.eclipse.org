/**
 * Copyright (c) 2021 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.resources;

import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CR;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CRA;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.DE;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.MA;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.api.ComplexAPI;
import org.eclipsefoundation.api.PeopleAPI;
import org.eclipsefoundation.api.model.EnhancedOrganizationContactData;
import org.eclipsefoundation.core.helper.CSRFHelper;
import org.eclipsefoundation.core.helper.ResponseHelper;
import org.eclipsefoundation.core.model.AdditionalUserData;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.membership.portal.helper.ModLogHelper;
import org.eclipsefoundation.membership.portal.model.EnhancedPersonData;
import org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType;
import org.eclipsefoundation.membership.portal.service.OrganizationsService;
import org.eclipsefoundation.persistence.service.FilterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.identity.SecurityIdentity;

/**
 * Provides access to commonly required services and containers for REST request serving.
 *
 * @author Martin Lowe
 */
public abstract class AbstractRESTResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(AbstractRESTResource.class);
    public static final String ALL_CACHE_PLACEHOLDER = "all";

    @Context
    HttpServletRequest request;

    @Inject
    FilterService filters;
    @Inject
    CachingService cache;

    @Inject
    RequestWrapper wrap;
    @Inject
    ResponseHelper responseBuider;

    @Inject
    CSRFHelper csrfHelper;
    @Inject
    ModLogHelper modLogHelper;
    @Inject
    AdditionalUserData aud;
    @Inject
    SecurityIdentity ident;

    @Inject
    OrganizationsService orgService;

    @RestClient
    @Inject
    PeopleAPI peopleAPI;
    @RestClient
    @Inject
    ComplexAPI complexAPI;
    @Inject
    APIMiddleware middle;

    /**
     * 
     * @param organizationID
     * @param relations
     * @return
     */
    protected List<EnhancedPersonData> getEnhancedPeopleDetails(String organizationID,
            List<OrganizationalUserType> relations) {
        // get the cached contacts data
        Optional<List<EnhancedOrganizationContactData>> ocs = cache.get(organizationID, null,
                EnhancedOrganizationContactData.class,
                () -> middle.paginationPassThrough(p -> complexAPI.getContacts(p, organizationID, null), wrap,
                        EnhancedOrganizationContactData.class));
        if (ocs.isEmpty()) {
            return Collections.emptyList();
        }
        boolean elevatedAccess = doesUserHaveElevatedAccess(organizationID);
        // convert the contact data to a more usable format
        return ocs.get().stream().map(p -> {
            // only expose email if user has elevated permissions
            return EnhancedPersonData.builder().setEmail(elevatedAccess ? p.getPerson().getEmail() : null)
                    .setFirstName(p.getPerson().getFname()).setLastName(p.getPerson().getLname())
                    .setId(p.getPerson().getPersonID())
                    .setRelations(p.getRelations().stream().map(r -> r.getRelation())
                            .filter(r -> relations.contains(OrganizationalUserType.valueOfChecked(r)))
                            .collect(Collectors.toList()))
                    .build();
        }).collect(Collectors.toList());
    }

    protected boolean doesUserHaveElevatedAccess(String organizationID) {
        if (ident.isAnonymous()) {
            return false;
        }
        List<OrganizationalUserType> roles = orgService.getUserAccessRoles(organizationID,
                ident.getPrincipal().getName());
        return roles.contains(CR) || roles.contains(CRA) || roles.contains(DE) || roles.contains(MA);
    }
}
