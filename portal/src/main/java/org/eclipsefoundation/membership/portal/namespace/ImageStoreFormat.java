package org.eclipsefoundation.membership.portal.namespace;

import java.util.stream.Stream;

/**
 * Centralized formats for image store.
 * 
 * @author Martin Lowe
 *
 */
public interface ImageStoreFormat {

    String getName();

    public enum ImageStoreFormats implements ImageStoreFormat {
        SMALL, LARGE, PRINT, WEB, WEB_SRC;

        @Override
        public String getName() {
            return this.name().toLowerCase();
        }

        public static ImageStoreFormat getFormat(String name) {
            if (name == null) {
                return null;
            }
            return Stream.of(values()).filter(v -> name.equalsIgnoreCase(v.getName())).findFirst().orElse(null);
        }
    }
}
