/**
 * Copyright (c) 2021 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.resources;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.api.model.OrganizationContactData;
import org.eclipsefoundation.core.namespace.RequestHeaderNames;
import org.eclipsefoundation.eclipsedb.dto.SysEventLog;
import org.eclipsefoundation.membership.portal.service.OrganizationsService;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.Authenticated;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;

/**
 * Handles OIDC routing for the request.
 *
 * @author Martin Lowe
 */
@Path("")
public class OIDCResource extends AbstractRESTResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(OIDCResource.class);

    @ConfigProperty(name = "eclipse.app.base-url", defaultValue = "/")
    String baseUrl;
    @ConfigProperty(name = "eclipse.security.log.table", defaultValue = "sessions")
    String loggingTable;
    @ConfigProperty(name = "eclipse.security.log.table", defaultValue = "sessions")
    String hostName;

    @Inject
    OrganizationsService orgService;
    @Inject
    DefaultHibernateDao dao;

    @GET
    @Authenticated
    @Path("/login")
    public Response routeLogin(@Context HttpServletRequest request) throws URISyntaxException {
        logLoginRequest(request);
        return redirect(baseUrl);
    }

    /**
     * While OIDC plugin takes care of actual logout, a route is needed to properly reroute anon user to home page.
     *
     * @throws URISyntaxException
     */
    @GET
    @Path("/logout")
    public Response routeLogout() throws URISyntaxException {
        return redirect(baseUrl);
    }

    @GET
    @Path("userinfo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserInfo(@HeaderParam(value = RequestHeaderNames.CSRF_TOKEN) String csrf) {
        // require CSRF to protect user info (could contain PII)
        // csrfHelper.compareCSRF(aud, csrf);
        if (!ident.isAnonymous()) {
            // cast the principal to a JWT token (which is the type for OIDC)
            DefaultJWTCallerPrincipal defaultPrin = (DefaultJWTCallerPrincipal) ident.getPrincipal();
            // create wrapper around data for output
            InfoWrapper uiw = new InfoWrapper();
            uiw.name = defaultPrin.getName();
            uiw.givenName = defaultPrin.getClaim("given_name");
            uiw.familyName = defaultPrin.getClaim("family_name");
            uiw.email = defaultPrin.getClaim("email");
            // update a list of OrganizationContactData objects to be organizationID -> [relations...]
            orgService.getOrganizationContacts(defaultPrin.getName())
                    .ifPresent(l -> uiw.organizationalRoles = l.stream()
                            .collect(Collectors.groupingBy(OrganizationContactData::getOrganizationID)).entrySet()
                            .stream()
                            .collect(Collectors.toMap(Entry<Integer, List<OrganizationContactData>>::getKey,
                                    entry -> entry.getValue().stream().map(OrganizationContactData::getRelation)
                                            .collect(Collectors.toList()))));

            return Response.ok(uiw).build();
        } else {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("csrf")
    public Response generateCSRF() {
        return Response.ok().build();
    }

    /**
     * Logs incoming successful login request to the system event table, using the users first found organization and
     * the server name as primary keys, and the username as the uid.
     * 
     * @param request the current http request object
     */
    private void logLoginRequest(HttpServletRequest request) {
        // retrieve the list of orgs for current user
        Principal p = ident.getPrincipal();
        Optional<List<OrganizationContactData>> cs = orgService.getOrganizationContacts(p.getName());
        // generate the entry for the login event
        SysEventLog log = new SysEventLog();
        log.setLogTable(loggingTable);
        log.setUid(p.getName());
        // set the org to the first available org if present
        if (cs.isPresent() && !cs.get().isEmpty()) {
            log.setPk1(Integer.toString(cs.get().get(0).getOrganizationID()));
        }
        log.setPk2(request.getServerName());
        log.setEvtDateTime(LocalDateTime.now());
        log.setLogAction("INSERT");
        dao.add(new RDBMSQuery<>(wrap, filters.get(SysEventLog.class)), Arrays.asList(log));
    }

    private Response redirect(String location) throws URISyntaxException {
        return Response.temporaryRedirect(new URI(location)).build();
    }

    public static class InfoWrapper {
        String name;
        String givenName;
        String familyName;
        String email;
        Map<Integer, List<String>> organizationalRoles = Collections.emptyMap();

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the givenName
         */
        public String getGivenName() {
            return givenName;
        }

        /**
         * @param givenName the givenName to set
         */
        public void setGivenName(String givenName) {
            this.givenName = givenName;
        }

        /**
         * @return the familyName
         */
        public String getFamilyName() {
            return familyName;
        }

        /**
         * @param familyName the familyName to set
         */
        public void setFamilyName(String familyName) {
            this.familyName = familyName;
        }

        /**
         * @return the email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email the email to set
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return the organizationalRoles
         */
        public Map<Integer, List<String>> getOrganizationalRoles() {
            return new HashMap<>(organizationalRoles);
        }

        /**
         * @param organizationalRoles the organizationalRoles to set
         */
        public void setOrganizationalRoles(Map<Integer, List<String>> organizationalRoles) {
            this.organizationalRoles = new HashMap<>(organizationalRoles);
        }
    }
}
