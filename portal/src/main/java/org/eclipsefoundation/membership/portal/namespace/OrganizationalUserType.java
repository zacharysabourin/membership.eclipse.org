package org.eclipsefoundation.membership.portal.namespace;

public enum OrganizationalUserType {
    CR("Company Representative"), CRA("Company representative - alternate"), MA("Marketing Representative"),
    CM("Committer"), DE("Delegate"), EMPLY("Employee"), PE("Page Editor"), NA("Not found");

    private final String label;

    private OrganizationalUserType(String label) {
        this.label = label;
    }

    /**
     * Gets the label for the user type.
     * 
     * @return
     */
    public String getLabel() {
        return this.label;
    }

    public static OrganizationalUserType valueOfChecked(String name) {
        try {
            return valueOf(name);
        } catch (IllegalArgumentException e) {
            // return na when no relation is found
            return NA;
        }
    }

}
