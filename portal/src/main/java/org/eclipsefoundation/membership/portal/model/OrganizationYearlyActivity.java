package org.eclipsefoundation.membership.portal.model;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_OrganizationYearlyActivity.Builder.class)
public abstract class OrganizationYearlyActivity {
    public abstract List<OrganizationActivity> getActivity();

    public static Builder builder() {
        return new AutoValue_OrganizationYearlyActivity.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setActivity(List<OrganizationActivity> activity);

        public abstract OrganizationYearlyActivity build();
    }

}
