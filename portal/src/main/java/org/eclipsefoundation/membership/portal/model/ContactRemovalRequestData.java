package org.eclipsefoundation.membership.portal.model;

import org.eclipsefoundation.api.model.PeopleData;
import org.eclipsefoundation.membership.portal.request.model.ContactRemovalRequest;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ContactRemovalRequestData.Builder.class)
public abstract class ContactRemovalRequestData {

    public abstract PeopleData getPerson();
    public abstract MemberOrganization getOrganization();
    public abstract String getReason();
    public abstract ContactRemovalRequest getRawRequest();
    
    public static Builder builder() {
        return new AutoValue_ContactRemovalRequestData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public static abstract class Builder {
        public abstract Builder setPerson(PeopleData person);
        public abstract Builder setOrganization(MemberOrganization organization);
        public abstract Builder setReason(String reason);
        public abstract Builder setRawRequest(ContactRemovalRequest rawRequest);
        public abstract ContactRemovalRequestData build();
    }
}
