package org.eclipsefoundation.membership.portal.resources;

import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CR;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.CRA;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.DE;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.EMPLY;
import static org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType.MA;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.api.PeopleAPI.PeopleRequestParams;
import org.eclipsefoundation.api.SysAPI;
import org.eclipsefoundation.api.model.OrganizationContactData;
import org.eclipsefoundation.api.model.PeopleData;
import org.eclipsefoundation.api.model.SysRelationData;
import org.eclipsefoundation.core.namespace.RequestHeaderNames;
import org.eclipsefoundation.membership.portal.model.ContactRemovalRequestData;
import org.eclipsefoundation.membership.portal.model.EnhancedPersonData;
import org.eclipsefoundation.membership.portal.model.MemberOrganization;
import org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType;
import org.eclipsefoundation.membership.portal.request.RolesAllowed;
import org.eclipsefoundation.membership.portal.request.model.ContactRemovalRequest;
import org.eclipsefoundation.membership.portal.service.MailerService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;

import io.quarkus.security.Authenticated;

@Path("organizations/{orgID:\\d+}/contacts")
@Produces(MediaType.APPLICATION_JSON)
public class OrganizationContactsResource extends AbstractRESTResource {

    @Inject
    MailerService mailer;

    @RestClient
    @Inject
    SysAPI sysAPI;

    @GET
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    public Response getContactsWithSearch(@PathParam("orgID") String organizationID,
            @HeaderParam(value = RequestHeaderNames.CSRF_TOKEN) String csrf) {
        // ensure csrf
        csrfHelper.compareCSRF(csrfHelper.getSessionCSRFToken(aud, request), csrf);
        Optional<List<SysRelationData>> relations = cache.get(ALL_CACHE_PLACEHOLDER, new MultivaluedMapImpl<>(),
                SysRelationData.class,
                () -> middle.getAll(i -> sysAPI.getSysRelations(i, "CO"), SysRelationData.class));
        // get all relations for organizations and convert to organizational user types
        List<OrganizationalUserType> actual = relations.get().stream()
                .map(srd -> OrganizationalUserType.valueOfChecked(srd.getRelation()))
                .filter(out -> !out.equals(OrganizationalUserType.NA)).collect(Collectors.toList());
        // add committers to the list (outside of CO group, and we need it specifically)
        actual.add(OrganizationalUserType.CM);
        List<EnhancedPersonData> contacts = getEnhancedPeopleDetails(organizationID, actual);
        if (contacts.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        return Response.ok(contacts).build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{user}")
    public Response getContactsForUser(@PathParam("orgID") String organizationID, @PathParam("user") String username,
            @HeaderParam(value = RequestHeaderNames.CSRF_TOKEN) String csrf) {
        // ensure csrf
        csrfHelper.compareCSRF(csrfHelper.getSessionCSRFToken(aud, request), csrf);
        Optional<List<OrganizationContactData>> contacts = orgService.getOrganizationContacts(organizationID, username);
        if (contacts.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        return Response.ok(contacts.get()).build();
    }

    @DELETE
    @Authenticated
    @RolesAllowed({ CR, DE, CRA })
    @Path("{user}")
    public Response deleteUserFromOrganizationRequest(@PathParam("orgID") Integer organizationID,
            @PathParam("user") String username, ContactRemovalRequest request) {
        if (!request.getOrganizationId().equals(organizationID)) {
            throw new BadRequestException(
                    "The organization that is being updated should match the organization in the URL");
        }
        ContactRemovalRequestData data = buildRemovalRequestData(request);
        // fire an email request to remove user from organization
        mailer.sendContactRemovalRequest(request);
        mailer.sendContactRemovalNotification(data);
        return Response.ok().build();
    }

    @GET
    @Authenticated
    @RolesAllowed({ CR, DE, CRA, MA })
    @Path("{user}/{relation}")
    public Response checkUserRelation(@PathParam("orgID") String organizationID, @PathParam("user") String username,
            @PathParam("relation") String relation, @HeaderParam(value = RequestHeaderNames.CSRF_TOKEN) String csrf) {
        // ensure csrf
        csrfHelper.compareCSRF(csrfHelper.getSessionCSRFToken(aud, request), csrf);
        Optional<List<OrganizationContactData>> contacts = orgService.getOrganizationContacts(organizationID, username);
        if (contacts.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        return Response.ok(contacts.get()).build();
    }

    @POST
    @Authenticated
    @RolesAllowed({ CR, DE, CRA })
    @Path("{user}/{relation}")
    public Response addUserRelation(@PathParam("orgID") String organizationID, @PathParam("user") String username,
            @PathParam("relation") String relation) {
        // check that the relation is real before trying to post it
        if (!checkRelationExists(relation)) {
            throw new BadRequestException("The following relation does not exist in service: " + relation);
        } else if (CR.equals(OrganizationalUserType.valueOf(relation))) {
            throw new BadRequestException("Adding the CR relation is not yet supported by the API.");
        } else if (EMPLY.equals(OrganizationalUserType.valueOf(relation))) {
            throw new BadRequestException("Adding the EMPLY relation is not supported by the API.");
        } else if (orgService.getUserAccessRoles(organizationID, username).isEmpty()) {
            throw new BadRequestException("Cannot add a relation to a user that isn't part of the organization");
        }

        // get current users rels (multiple allowed)
        List<OrganizationalUserType> currentUserRel = orgService.getUserAccessRoles(organizationID,
                ident.getPrincipal().getName());
        if (!currentUserRel.contains(CR) && CRA.equals(OrganizationalUserType.valueOf(relation))) {
            throw new BadRequestException(
                    "Only individuals with the primary Company Representative role (CR) may add company representative alternates");
        } else if ((!currentUserRel.contains(CR) && !currentUserRel.contains(CRA))
                && DE.equals(OrganizationalUserType.valueOf(relation))) {
            throw new BadRequestException(
                    "Only individuals with the Company Representative roles (CR/CRA) may add delegates");
        }

        // marshal the contact information into an entity to push to the service
        OrganizationContactData contact = OrganizationContactData.builder()
                .setComments("Relation created through the Membership Portal by " + ident.getPrincipal().getName())
                .setOrganizationID(Integer.valueOf(organizationID)).setPersonID(username.toLowerCase())
                .setRelation(relation).setRelation(relation.toUpperCase()).build();

        // update the entity and return
        orgService.updateOrganizationContact(organizationID, contact);
        modLogHelper.postUpdateModLog(ident, "OrganizationContact", username.toLowerCase(), organizationID + "|" + relation);
        return Response.ok().build();
    }

    @DELETE
    @Authenticated
    @RolesAllowed({ CR, DE, CRA })
    @Path("{user}/{relation}")
    public Response deleteUserRelation(@PathParam("orgID") String organizationID, @PathParam("user") String username,
            @PathParam("relation") String relation) {
        if (!checkRelationExists(relation)) {
            throw new BadRequestException("The following relation does not exist in service: " + relation);
        } else if (CR.equals(OrganizationalUserType.valueOf(relation))) {
            throw new BadRequestException("Removing the CR relation is not yet supported by the API.");
        } else if (EMPLY.equals(OrganizationalUserType.valueOf(relation))) {
            throw new BadRequestException("Removing the EMPLY relation is not supported by the API.");
        }

        // get current users rels (multiple allowed)
        List<OrganizationalUserType> currentUserRel = orgService.getUserAccessRoles(organizationID,
                ident.getPrincipal().getName());
        if (!currentUserRel.contains(CR) && CRA.equals(OrganizationalUserType.valueOf(relation))) {
            throw new BadRequestException(
                    "Only individuals with the primary Company Representative role (CR) may remove company representative alternates");
        } else if ((!currentUserRel.contains(CR) && !currentUserRel.contains(CRA))
                && DE.equals(OrganizationalUserType.valueOf(relation))) {
            throw new BadRequestException(
                    "Only individuals with the Company Representative roles (CR/CRA) may remove delegates");
        }

        orgService.removeOrganizationContact(organizationID, username, relation);
        modLogHelper.postDeleteModLog(ident, "OrganizationContact", username.toLowerCase(), organizationID + "|" + relation);
        return Response.ok().build();
    }

    /**
     * Check if the passed relation matches an existing relation from the sys API.
     * 
     * @param relation the relation to check
     * @return true if the relation exists, false otherwise
     */
    private boolean checkRelationExists(String relation) {
        Optional<List<SysRelationData>> relations = cache.get(ALL_CACHE_PLACEHOLDER, new MultivaluedMapImpl<>(),
                SysRelationData.class,
                () -> middle.getAll(i -> sysAPI.getSysRelations(i, "CO"), SysRelationData.class));
        if (relations.isEmpty()) {
            throw new IllegalStateException("Could not retrieve any relations from upstream, service may be down.");
        }
        return relations.get().stream().anyMatch(r -> r.getRelation().equalsIgnoreCase(relation));
    }

    /**
     * Supplements a removal request with more data about the organization and person that should be acted on for the
     * notifications that are sent out by the system for a removal request.
     * 
     * @param r the removal request to retrieve more contextual information for
     * @return a processed removal request with more contextual data on the org and person involved in the transaction.
     * @throws BadRequestException if either the organization ID or username do not translate to a real entity, then
     * this is thrown as the state cannot be recovered.
     */
    private ContactRemovalRequestData buildRemovalRequestData(ContactRemovalRequest r) {
        // should be cached by service, should always have a result
        Optional<MemberOrganization> org = orgService.getByID(Integer.toString(r.getOrganizationId()));
        if (org.isEmpty()) {
            throw new BadRequestException("The passed username is not a valid username");
        }
        // get cached people. Should only return one entry as usernames should be unique keys
        Optional<List<PeopleData>> people = cache.get(r.getUsername(), new MultivaluedMapImpl<>(), PeopleData.class,
                () -> middle.getAll(
                        p -> peopleAPI.getPeople(p, PeopleRequestParams.builder().setUsername(r.getUsername()).build()),
                        PeopleData.class));
        if (people.isEmpty()) {
            throw new BadRequestException("The passed username is not a valid username");
        }
        // create the output object, including the passed reason.
        return ContactRemovalRequestData.builder().setOrganization(org.get()).setPerson(people.get().get(0))
                .setReason(r.getReason()).setRawRequest(r).build();
    }
}
