package org.eclipsefoundation.membership.portal.service;

import java.util.List;

import org.eclipsefoundation.api.model.ProjectData;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.membership.portal.model.MemberOrganization;

/**
 * Service for retrieving committer stats from the dashboard DB.
 * 
 * @author Martin Lowe
 *
 */
public interface ProjectsService {

    /**
     * Retrieve a list of organizations for a given project.
     * 
     * @param projectId the project to retrieve contributing organizations for.
     * @param wrap TODO
     * @return list of organizations that contributed to the project.
     */
    List<MemberOrganization> getOrganizationsForProject(String projectId, RequestWrapper wrap);

    /**
     * Retrieve a list of projects that an organization has contributed to.
     * 
     * @param organizationId the organization to retrieve contributed to projects for.
     * @return a list of projects that the organization has contributed to, or an empty list.
     */
    List<ProjectData> getProjectsForOrganization(int organizationId, RequestWrapper wrap);

    /**
     * Checks whether a project with the given ID exists.
     * 
     * @param projectId the project ID to check if it exists.
     * @return the project if it exists, or null
     */
    ProjectData doesProjectExist(String projectId);
}
