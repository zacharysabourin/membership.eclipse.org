package org.eclipsefoundation.membership.portal.model;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_OrganizationProductData.Builder.class)
public abstract class OrganizationProductData {
    @Nullable 
    public abstract Integer getProductId();

    public abstract Integer getOrganizationId();

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getDescription();

    public abstract String getProductUrl();

    public static Builder builder() {
        return new AutoValue_OrganizationProductData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setProductId(@Nullable Integer productId);

        public abstract Builder setOrganizationId(Integer organizationId);

        public abstract Builder setName(@Nullable String name);

        public abstract Builder setDescription(@Nullable String description);

        public abstract Builder setProductUrl(String productUrl);

        public abstract OrganizationProductData build();
    }
}
