/**
 * Copyright (c) 2021 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.portal.service;

import java.util.List;
import java.util.Optional;

import org.eclipsefoundation.api.model.OrganizationContactData;
import org.eclipsefoundation.api.model.PeopleData;
import org.eclipsefoundation.api.model.ProjectData;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.membership.portal.model.MemberOrganization;
import org.eclipsefoundation.membership.portal.namespace.OrganizationalUserType;

/**
 * Service for interacting with external organization entities.
 * 
 * @author Martin Lowe
 *
 */
public interface OrganizationsService {
    /**
     * Retrieve a list of all member organizations available to the API.
     * 
     * @return a list of all member organizations.
     */
    List<MemberOrganization> get(RequestWrapper wrap);

    /**
     * Retrieve a single member organization by its organization ID.
     * 
     * @param id the organization ID for the entry to retrieve
     * @return the member organization if it exists, otherwise an empty optional.
     */
    Optional<MemberOrganization> getByID(String id);

    /**
     * Retrieve a list of member organization by their organization ID.
     * 
     * @param id the organization ID for the entry to retrieve
     * @return the member organization if it exists, otherwise an empty optional.
     */
    List<MemberOrganization> getByIDs(List<String> id);

    /**
     * Retrieve a list of all projects for an organization.
     * 
     * @deprecated deprecated for new service call,
     * {@link ProjectsService#getProjectsForOrganization(int, RequestWrapper)
     * 
     * @return a list of all member organization projects.
     */
    @Deprecated(forRemoval = true, since = "v1.6.5")
    Optional<List<ProjectData>> getProjectsForOrganization(String id);

    /**
     * Retrieve a list of all committers for an organization.
     * 
     * @return a list of all committer members of an organization.
     */
    Optional<List<PeopleData>> getCommittersForOrganization(String id);

    Optional<List<PeopleData>> getContributorsForOrganization(String id);

    /**
     * Retrieve a list of organization contacts that match the optional serach parameters below.
     * 
     * @param orgID the ID of the organization to search.
     * @param mail the email address to use as a search parameter
     * @param role the role to limit resulting users to.
     * @param fName the first name of the user (no fuzzy).
     * @param lName the last name of the user (no fuzzy).
     * @return a list of organization contacts that match the given parameters.
     */
    Optional<List<OrganizationContactData>> getOrganizationContacts(String orgID, Optional<String> mail,
            Optional<String> role, Optional<String> fName, Optional<String> lName);

    /**
     * Retrieves all user relations for a given organzation for a user.
     * 
     * @param orgID the ID of the organization to search.
     * @param userName the username of the contact to retrieve entries for.
     * @return list of user relations for the given user in organization if they exist
     */
    Optional<List<OrganizationContactData>> getOrganizationContacts(String orgID, String userName);

    /**
     * Retrieves all user relations of organzations for a user.
     * 
     * @param userName the username of the contact to retrieve entries for.
     * @return list of user relations for the given user in organizations if they exist
     */
    Optional<List<OrganizationContactData>> getOrganizationContacts(String userName);

    /**
     * Updates the organization contact with the given model.
     * 
     * @param orgID the ID of the organization to search.
     * @param orgContact the updated organization contact to set
     * @return the updated entity from the external service.
     */
    OrganizationContactData updateOrganizationContact(String orgID, OrganizationContactData orgContact);

    /**
     * Checks the external org service for a given user relation.
     * 
     * @param orgID the ID of the organization to search.
     * @param userName the username of the contact to update.
     * @param role the role of the user to check for.
     * @return true if the user has the role, false otherwise.
     */
    boolean organizationContactHasRole(String orgID, String userName, String role);

    /**
     * Triggers a deletion call in the external organization service for a contact.
     * 
     * @param orgID the ID of the organization to search.
     * @param userName the username of the contact to update.
     * @param role the role of the user to remove.
     */
    void removeOrganizationContact(String orgID, String userName, String role);

    /**
     * Gets the user roles for the given organization.
     * 
     * @param orgID the organization user access is being retrieved from
     * @param userName the user whos access is being requested
     * @return list of user access roles, or empty list if there are none.
     */
    List<OrganizationalUserType> getUserAccessRoles(String orgID, String userName);
}
