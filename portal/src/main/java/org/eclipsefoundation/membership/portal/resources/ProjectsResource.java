package org.eclipsefoundation.membership.portal.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipsefoundation.membership.portal.model.MemberOrganization;
import org.eclipsefoundation.membership.portal.service.ProjectsService;

@Path("projects")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProjectsResource extends AbstractRESTResource {

    @Inject
    ProjectsService committerService;

    @GET
    @Path("{projectId}/organizations")
    public Response getOrganizationsForProject(@PathParam("projectId") String projectId) {
        List<MemberOrganization> orgs = committerService.getOrganizationsForProject(projectId, wrap);
        if (orgs == null) {
            return Response.status(404).build();
        }
        return Response.ok(orgs).build();
    }
}
