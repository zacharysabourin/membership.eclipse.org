package org.eclipsefoundation.membership.portal.config;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

import org.eclipsefoundation.application.dao.REMPersistenceDAO;
import org.eclipsefoundation.core.model.CSRFGenerator;
import org.eclipsefoundation.persistence.dto.DistributedCSRFToken.DistributedCSRFTokenFilter;
import org.eclipsefoundation.persistence.model.DistributedCSRFGenerator;

@Dependent
public class DistributedCSRFProvider {

    @Produces
    @ApplicationScoped
    public CSRFGenerator distributedGenerator(REMPersistenceDAO dao, DistributedCSRFTokenFilter filter) {
        return new DistributedCSRFGenerator(dao, filter);
    }
}
