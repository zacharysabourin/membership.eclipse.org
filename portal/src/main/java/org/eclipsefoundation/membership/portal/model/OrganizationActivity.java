package org.eclipsefoundation.membership.portal.model;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_OrganizationActivity.Builder.class)
public abstract class OrganizationActivity {

    public abstract Integer getCount();

    @Nullable
    public abstract String getPeriod();

    public static Builder builder() {
        return new AutoValue_OrganizationActivity.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setCount(Integer count);

        public abstract Builder setPeriod(@Nullable String period);

        public abstract OrganizationActivity build();
    }

}
