package org.eclipsefoundation.membership.portal.model;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_EnhancedPersonData.Builder.class)
public abstract class EnhancedPersonData {

    @Nullable
    public abstract String getId();

    public abstract String getFirstName();

    public abstract String getLastName();

    @Nullable
    public abstract String getEmail();

    public abstract List<String> getRelations();

    public static Builder builder() {
        return new AutoValue_EnhancedPersonData.Builder().setRelations(Collections.emptyList());
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setId(@Nullable String id);

        public abstract Builder setFirstName(String firstName);

        public abstract Builder setLastName(String lastName);

        public abstract Builder setEmail(@Nullable String email);

        public abstract Builder setRelations(List<String> relations);

        public abstract EnhancedPersonData build();
    }
}
