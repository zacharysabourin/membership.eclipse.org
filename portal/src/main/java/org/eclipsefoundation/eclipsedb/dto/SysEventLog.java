package org.eclipsefoundation.eclipsedb.dto;

import java.time.LocalDateTime;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.eclipsedb.namespaces.EclipseDBParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

@Entity
@Table(name = "SYS_EvtLog")
public class SysEventLog extends BareNode {
    public static final DtoTable TABLE = new DtoTable(SysEventLog.class, "sel");

    @Id
    private int logId;
    private String logTable;
    @Column(name = "PK1")
    private String pk1;
    @Column(name = "PK2")
    private String pk2;
    private String logAction;
    private String uid;
    private LocalDateTime evtDateTime;

    @Override
    public Object getId() {
        return getLogId();
    }

    /**
     * @return the logId
     */
    public int getLogId() {
        return logId;
    }

    /**
     * @param logId the logId to set
     */
    public void setLogId(int logId) {
        this.logId = logId;
    }

    /**
     * @return the logTable
     */
    public String getLogTable() {
        return logTable;
    }

    /**
     * @param logTable the logTable to set
     */
    public void setLogTable(String logTable) {
        this.logTable = logTable;
    }

    /**
     * @return the pk1
     */
    public String getPk1() {
        return pk1;
    }

    /**
     * @param pk1 the pk1 to set
     */
    public void setPk1(String pk1) {
        this.pk1 = pk1;
    }

    /**
     * @return the pk2
     */
    public String getPk2() {
        return pk2;
    }

    /**
     * @param pk2 the pk2 to set
     */
    public void setPk2(String pk2) {
        this.pk2 = pk2;
    }

    /**
     * @return the logAction
     */
    public String getLogAction() {
        return logAction;
    }

    /**
     * @param logAction the logAction to set
     */
    public void setLogAction(String logAction) {
        this.logAction = logAction;
    }

    /**
     * @return the uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid the uid to set
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * @return the evtDateTime
     */
    public LocalDateTime getEvtDateTime() {
        return evtDateTime;
    }

    /**
     * @param evtDateTime the evtDateTime to set
     */
    public void setEvtDateTime(LocalDateTime evtDateTime) {
        this.evtDateTime = evtDateTime;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SysEventLog [logId=");
        builder.append(logId);
        builder.append(", logTable=");
        builder.append(logTable);
        builder.append(", pk1=");
        builder.append(pk1);
        builder.append(", pk2=");
        builder.append(pk2);
        builder.append(", logAction=");
        builder.append(logAction);
        builder.append(", uid=");
        builder.append(uid);
        builder.append(", evtDateTime=");
        builder.append(evtDateTime);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class SysEventLogFilter implements DtoFilter<SysEventLog> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // log ID check
                String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
                if (id != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".logId = ?",
                            new Object[] { Integer.valueOf(id) }));
                }
                // username check
                String username = params.getFirst(EclipseDBParameterNames.USERNAME.getName());
                if (username != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".uid = ?",
                            new Object[] { username }));
                }
                // username check
                String orgId = params.getFirst(EclipseDBParameterNames.ORGANIZATION_ID.getName());
                if (orgId != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".pk1 = ?",
                            new Object[] { orgId }));
                }
            }

            return stmt;
        }

        @Override
        public Class<SysEventLog> getType() {
            return SysEventLog.class;
        }
    }
}
