package org.eclipsefoundation.eclipsedb.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.eclipsedb.namespaces.EclipseDBParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

@Table
@Entity
public class OrganizationProducts extends BareNode {
    public static final DtoTable TABLE = new DtoTable(OrganizationProducts.class, "op");

    @EmbeddedId
    private OrganizationProductsId compositeId;
    private String name;
    @Column(length = 700)
    private String description;
    @Column(name = "product_url")
    private String productUrl;

    @Override
    public Object getId() {
        return getCompositeId();
    }

    /**
     * @return the compositeId
     */
    public OrganizationProductsId getCompositeId() {
        return compositeId;
    }

    /**
     * @param compositeId the compositeId to set
     */
    public void setCompositeId(OrganizationProductsId compositeId) {
        this.compositeId = compositeId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the productUrl
     */
    public String getProductUrl() {
        return productUrl;
    }

    /**
     * @param productUrl the productUrl to set
     */
    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    @Embeddable
    public static class OrganizationProductsId implements Serializable {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer productId;
        private Integer organizationId;

        /**
         * @return the productId
         */
        public Integer getProductId() {
            return productId;
        }

        /**
         * @param productId the productId to set
         */
        public void setProductId(Integer productId) {
            this.productId = productId;
        }

        /**
         * @return the organizationId
         */
        public Integer getOrganizationId() {
            return organizationId;
        }

        /**
         * @param organizationId the organizationId to set
         */
        public void setOrganizationId(Integer organizationId) {
            this.organizationId = organizationId;
        }

        @Override
        public int hashCode() {
            return Objects.hash(organizationId, productId);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            OrganizationProductsId other = (OrganizationProductsId) obj;
            return Objects.equals(organizationId, other.organizationId) && Objects.equals(productId, other.productId);
        }

    }
    
    @Singleton
    public static class OrganizationInformationFilter implements DtoFilter<OrganizationProducts> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement stmt = builder.build(TABLE);
            if (isRoot) {
                // organization ID check
                String orgId = params.getFirst(EclipseDBParameterNames.ORGANIZATION_ID.getName());
                if (orgId != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeId.organizationId = ?",
                            new Object[] { Integer.valueOf(orgId) }));
                }
                // product ID check
                String productId = params.getFirst(EclipseDBParameterNames.PRODUCT_ID.getName());
                if (productId != null) {
                    stmt.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".compositeId.productId = ?",
                            new Object[] { Integer.valueOf(productId) }));
                }
            }

            return stmt;
        }

        @Override
        public Class<OrganizationProducts> getType() {
            return OrganizationProducts.class;
        }
    }
}
