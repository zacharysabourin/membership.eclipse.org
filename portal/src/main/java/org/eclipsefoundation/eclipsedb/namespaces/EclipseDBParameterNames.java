package org.eclipsefoundation.eclipsedb.namespaces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Singleton;

import org.eclipsefoundation.core.namespace.UrlParameterNamespace;

@Singleton
public class EclipseDBParameterNames implements UrlParameterNamespace {

    public static final UrlParameter PRODUCT_ID = new UrlParameter("product_id");
    public static final UrlParameter ORGANIZATION_ID = new UrlParameter("organization_id");
    public static final UrlParameter USERNAME = new UrlParameter("username");

    private static final List<UrlParameter> params = Collections
            .unmodifiableList(Arrays.asList(PRODUCT_ID, ORGANIZATION_ID, USERNAME));

    @Override
    public List<UrlParameter> getParameters() {
        return new ArrayList<>(params);
    }

}
