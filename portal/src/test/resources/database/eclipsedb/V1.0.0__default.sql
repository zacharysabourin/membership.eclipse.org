-- eclipse.OrganizationInformation definition

CREATE TABLE OrganizationInformation (
  OrganizationID int(11) NOT NULL,
  short_description varchar(512) DEFAULT NULL,
  long_description mediumtext DEFAULT NULL,
  company_url mediumtext NOT NULL,
  small_mime varchar(32) DEFAULT NULL,
  small_logo blob DEFAULT NULL,
  small_width smallint(5) unsigned NOT NULL,
  small_height smallint(5) unsigned NOT NULL,
  large_mime varchar(32) DEFAULT NULL,
  large_logo blob DEFAULT NULL,
  large_width smallint(5) unsigned NOT NULL,
  large_height smallint(5) unsigned NOT NULL
);
INSERT INTO OrganizationInformation(OrganizationID,short_description,long_description,company_url,small_mime,small_width,small_height,large_mime,large_width,large_height)
  VALUES(15, 'some description', 'some longer description', 'some company url', 'image/jpg',300,300,'image/png',500,500);
  
CREATE TABLE `OrganizationProducts` (
  `ProductID` int(11) NOT NULL AUTO_INCREMENT,
  `OrganizationID` int(11) DEFAULT NULL,
  `name` varchar(256) NOT NULL,
  `description` text DEFAULT NULL,
  `product_url` varchar(512) NOT NULL,
  PRIMARY KEY (`ProductID`)
);
INSERT INTO OrganizationProducts(ProductID,OrganizationID,name,description,product_url)
  VALUES(15,1,'name', 'description', 'url');