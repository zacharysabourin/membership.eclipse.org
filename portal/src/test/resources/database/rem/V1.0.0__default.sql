CREATE TABLE `DistributedCSRFToken` (
  `token` varchar(100) NOT NULL,
  `ipAddress` varchar(64) NOT NULL,
  `userAgent` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `user` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`token`)
);
