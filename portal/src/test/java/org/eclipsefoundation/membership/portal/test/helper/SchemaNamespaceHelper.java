package org.eclipsefoundation.membership.portal.test.helper;

/**
 * Indicates the file paths for the various schema files used to validate test objects and formats.
 * 
 * @author Martin Lowe
 *
 */
public class SchemaNamespaceHelper {
    public static final String BASE_SCHEMAS_PATH = "schemas/";
    public static final String BASE_SCHEMAS_PATH_SUFFIX = "-schema.json";

    public static final String MEMBERSHIP_FORM_SCHEMA_PATH = BASE_SCHEMAS_PATH + "membership-form"
            + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String MEMBERSHIP_FORMS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "membership-forms"
            + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String CONTACTS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "contacts" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String CONTACT_SCHEMA_PATH = BASE_SCHEMAS_PATH + "contact" + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String FORM_ORGANIZATIONS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "form-organizations"
            + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String FORM_ORGANIZATION_SCHEMA_PATH = BASE_SCHEMAS_PATH + "form-organization"
            + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String WORKING_GROUPS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "form-working-groups"
            + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String WORKING_GROUP_SCHEMA_PATH = BASE_SCHEMAS_PATH + "form-working-group"
            + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATIONS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organizations"
            + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATION_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organization" + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String ORGANIZATION_COMMITTER_ACTIVITY_SCHEMA_PATH = BASE_SCHEMAS_PATH
            + "organization-committer-activity-list" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATION_CONTACTS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organization-contacts"
            + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATION_CONTACT_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organization-contact"
            + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String ORGANIZATION_PRODUCTS_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organization-products"
            + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String ORGANIZATION_PRODUCT_SCHEMA_PATH = BASE_SCHEMAS_PATH + "organization-product"
            + BASE_SCHEMAS_PATH_SUFFIX;

    private SchemaNamespaceHelper() {
    }
}
