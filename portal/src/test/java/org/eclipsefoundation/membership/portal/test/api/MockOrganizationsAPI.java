package org.eclipsefoundation.membership.portal.test.api;

import java.util.Arrays;
import java.util.Date;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.ws.rs.BeanParam;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.api.OrganizationAPI;
import org.eclipsefoundation.api.model.OrganizationContactData;
import org.eclipsefoundation.api.model.OrganizationData;
import org.eclipsefoundation.api.model.OrganizationDocumentData;
import org.eclipsefoundation.api.model.OrganizationMembershipData;
import org.eclipsefoundation.api.model.PeopleData;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;

@Alternative()
@Priority(1)
@ApplicationScoped
@RestClient
public class MockOrganizationsAPI implements OrganizationAPI {

    public static final String ORGANIZATION_CONTACT_USER_NAME = "opearson";
    public static final String ORGANIZATION_SAMPLE_ID = "15";

    PeopleData people = PeopleData.builder().setBlog("blog url").setComments("comments").setEmail("some_email@dummy.co")
            .setFax("fax").setFname("First name").setIssuesPending(false).setLatitude("lat").setLname("Last name")
            .setLongitude("lon").setMember(true).setMemberPassword("password").setMemberSince(new Date())
            .setMobile("mobile phone").setProvisioning("provisioning").setScrmGuid("scrm_guid").setType("type")
            .setUnixAcctCreated(true).setPhone("phone number").setPersonID(ORGANIZATION_CONTACT_USER_NAME).build();

    OrganizationData org = OrganizationData.build().setComments("").setFax("").setLatLong("").setMemberSince(new Date())
            .setName1("").setName2("").setOrganizationID(Integer.valueOf(ORGANIZATION_SAMPLE_ID)).setPhone("")
            .setScrmGUID("").setTimestamp(new Date()).build();

    OrganizationContactData contact = OrganizationContactData.builder().setComments("")
            .setOrganizationID(Integer.valueOf(ORGANIZATION_SAMPLE_ID)).setPersonID(people.getPersonID())
            .setRelation("CR").setTitle("sample").build();

    OrganizationMembershipData om = OrganizationMembershipData.builder().setComments("sample").setDuesTier(1)
            .setEntryDate(new Date()).setExpiryDate(null).setInvoiceMonth(1).setIssuesPending(false)
            .setOrganizationID(Integer.valueOf(ORGANIZATION_SAMPLE_ID)).setRelation("AP").setRenewalProb("Probably")
            .build();

    OrganizationDocumentData d = OrganizationDocumentData.builder().setDocumentID("32c5a745df264aa2a809")
            .setOrganizationID(Integer.valueOf(ORGANIZATION_SAMPLE_ID)).setComments("comments")
            .setEffectiveDate(new Date()).setExpirationDate(new Date()).setReceivedDate(new Date())
            .setScannedDocumentBLOB(null).setScannedDocumentBytes(0).setScannedDocumentFileName(null)
            .setScannedDocumentMime(null).setVersion(1).build();

    @Override
    public Response getOrganizations(@BeanParam BaseAPIParameters baseParams, @BeanParam OrganizationRequestParams params) {
        return Response.ok(Arrays.asList(org)).build();
    }

    @Override
    public OrganizationData getOrganization(String id) {
        return org;
    }

    @Override
    public Response getOrganizationMembership(String id,  BaseAPIParameters baseParams,  OrganizationRequestParams params) {
        return Response.ok(Arrays.asList(om)).build();
    }

    @Override
    public OrganizationContactData updateOrganizationContacts(String id, OrganizationContactData contact) {
        return contact;
    }

    @Override
    public Response getOrganizationEmploymentHistory(String id, @BeanParam BaseAPIParameters baseParams) {
        return Response.ok(Arrays.asList(contact)).build();
    }

    @Override
    public Response getOrganizationContacts(@BeanParam BaseAPIParameters baseParams, OrganizationRequestParams params) {
        return Response.ok(Arrays.asList(contact)).build();
    }

    @Override
    public Response getOrganizationContactsWithSearch(String id, @BeanParam BaseAPIParameters baseParams, OrganizationRequestParams params) {
        // TODO Auto-generated method stub
        return Response.ok(Arrays.asList(contact)).build();
    }

    @Override
    public Response getOrganizationContact(String id, String personID, @BeanParam BaseAPIParameters baseParams, OrganizationRequestParams params) {
        // TODO Auto-generated method stub
        return Response.ok(Arrays.asList(contact)).build();
    }

    @Override
    public Response removeOrganizationContacts(String id, String personID, String relation) {
        // TODO Auto-generated method stub
        return Response.ok(Arrays.asList(contact)).build();
    }

    @Override
    public Response getOrganizationDocuments(String id, @BeanParam BaseAPIParameters baseParams, OrganizationRequestParams params) {
        // TODO Auto-generated method stub
        return Response.ok(Arrays.asList(d)).build();
    }


}
