package org.eclipsefoundation.membership.portal.test.resources;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Used for enhanced CORS filter testing.
 * 
 * @author Martin Lowe
 *
 */
@Path("test")
@Produces(MediaType.APPLICATION_JSON)
public class TestResources {

    /**
     * Covered by test1 CORS config
     * 
     * @return ok response
     */
    @GET
    @Path("1")
    public Response get() {
        return Response.ok().build();
    }

    /**
     * Covered by test1 CORS config
     * 
     * @return ok response
     */
    @GET
    @Path("1/sub")
    public Response getFirstSubresource() {
        return Response.ok().build();
    }

    /**
     * Covered by test1 CORS config
     * 
     * @return ok response
     */
    @PUT
    @Path("1/sub")
    public Response putFirstSubresource() {
        return Response.ok().build();
    }

    /**
     * Covered by test1 CORS config
     * 
     * @return ok response
     */
    @GET
    @Path("1/other")
    public Response getFirstOtherResource() {
        return Response.ok().build();
    }

    /**
     * Covered by no CORS config (wrong method, get not covered)
     * 
     * @return ok response
     */
    @GET
    @Path("2")
    public Response otherGet() {
        return Response.ok().build();
    }

    /**
     * Covered by test2 CORS config
     * 
     * @return ok response
     */
    @POST
    @Path("2")
    public Response post() {
        return Response.ok().build();
    }

    /**
     * Covered by test2 CORS config
     * 
     * @return ok response
     */
    @POST
    @Path("2/sub")
    public Response postSecondSubresource() {
        return Response.ok().build();
    }

    /**
     * Covered by no CORS config (not wildcard/regex matched endpoints)
     * 
     * @return ok response
     */
    @POST
    @Path("2/other")
    public Response postSecondOtherResource() {
        return Response.ok().build();
    }
}
