package org.eclipsefoundation.membership.portal.resources;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import javax.inject.Inject;

import org.eclipsefoundation.core.namespace.RequestHeaderNames;
import org.eclipsefoundation.membership.portal.model.OrganizationProductData;
import org.eclipsefoundation.membership.portal.test.helper.AuthHelper;
import org.eclipsefoundation.membership.portal.test.helper.SchemaNamespaceHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.ConfigMetadata;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.restassured.filter.session.SessionFilter;
import io.restassured.http.ContentType;

@QuarkusTest
public class OrganizationsResourceTest {

    public static final String ORGANIZATION_CONTACT_USER_NAME = "opearson";
    public static final String ORGANIZATION_SAMPLE_ID = "15";
    public static final String ORGANIZATION_PRODUCT_ID = "1";

    public static final String ORGANIZATIONS_BASE_URL = "/organizations";
    public static final String ORGANIZATION_BY_ID_URL = ORGANIZATIONS_BASE_URL + "/{organizationID}";
    public static final String ORGANIZATION_ACTIVITY_URL = ORGANIZATION_BY_ID_URL + "/activity";
    public static final String ORGANIZATION_CONTACTS_URL = ORGANIZATION_BY_ID_URL + "/contacts";
    public static final String ORGANIZATION_CONTACT_BY_ID_URL = ORGANIZATION_CONTACTS_URL + "/{contactID}";
    public static final String ORGANIZATION_COUNTRY_ACTIVITY_URL = ORGANIZATION_BY_ID_URL + "/country";
    public static final String ORGANIZATION_PRODUCTS_URL = ORGANIZATION_BY_ID_URL + "/products";
    public static final String ORGANIZATION_PRODUCT_BY_ID_URL = ORGANIZATION_PRODUCTS_URL + "/{productID}";

    @Inject
    ObjectMapper mapper;

    //
    // GET /organizations
    //
    // @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getOrganizations_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(ORGANIZATIONS_BASE_URL).then().statusCode(200);
    }

    // @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getOrganizations_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(ORGANIZATIONS_BASE_URL).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.ORGANIZATIONS_SCHEMA_PATH));
    }

    //
    // GET /organizations/{organizationID}
    //
    // @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getOrganization_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(ORGANIZATION_BY_ID_URL, ORGANIZATION_SAMPLE_ID).then().statusCode(200);
    }

    // @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getOrganization_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(ORGANIZATION_BY_ID_URL, ORGANIZATION_SAMPLE_ID).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.ORGANIZATION_SCHEMA_PATH));
    }

    //
    // GET /organizations/{organizationID}/activity
    //
    // @Test
    void getOrganizationCommitterActivity_success() {
        given().when().get(ORGANIZATION_ACTIVITY_URL, ORGANIZATION_SAMPLE_ID).then().statusCode(200);
    }

    // @Test
    void getOrganizationCommitterActivity_success_format() {
        given().get(ORGANIZATION_ACTIVITY_URL, ORGANIZATION_SAMPLE_ID).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.ORGANIZATION_COMMITTER_ACTIVITY_SCHEMA_PATH));
    }

    //
    // GET /organizations/{organizationID}/contacts
    //
    @Test
    void getOrganizationContacts_requireAuth() {
        // auth is triggered before CSRF (as the request is dumped before it gets
        // processed)
        given().auth().none().when().get(ORGANIZATION_CONTACTS_URL, ORGANIZATION_SAMPLE_ID).then().statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.PORTAL_ADMIN_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getOrganizationContacts_csrfGuard() {
        // happens after auth, once the request is processed
        given().when().get(ORGANIZATION_CONTACTS_URL, ORGANIZATION_SAMPLE_ID).then().statusCode(403);
    }

    // @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.PORTAL_ADMIN_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getOrganizationContacts_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(ORGANIZATION_CONTACTS_URL, ORGANIZATION_SAMPLE_ID).then().statusCode(200);
    }

    // @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.PORTAL_ADMIN_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getOrganizationContacts_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(ORGANIZATION_CONTACTS_URL, ORGANIZATION_SAMPLE_ID).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.ORGANIZATION_CONTACTS_SCHEMA_PATH));
    }

    //
    // GET /organizations/{organizationID}/contacts
    //
    @Test
    void getOrganizationContact_requireAuth() {
        // auth is triggered before CSRF (as the request is dumped before it gets
        // processed)
        given().auth().none().when()
                .get(ORGANIZATION_CONTACT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME).then()
                .statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.PORTAL_ADMIN_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getOrganizationContact_csrfGuard() {
        // happens after auth, once the request is processed
        given().when().get(ORGANIZATION_CONTACT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME)
                .then().statusCode(403);
    }

    // @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.PORTAL_ADMIN_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getOrganizationContact_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(ORGANIZATION_CONTACT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME)
                .then().statusCode(200);
    }

    // @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.PORTAL_ADMIN_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getOrganizationContact_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(ORGANIZATION_CONTACT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_CONTACT_USER_NAME)
                .then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.ORGANIZATION_CONTACTS_SCHEMA_PATH));
    }

    //
    // GET /organizations/{organizationID}/products
    //
    @Test
    void getOrganizationProducts_success() {
        given().when().get(ORGANIZATION_PRODUCTS_URL, ORGANIZATION_SAMPLE_ID).then().statusCode(200);
    }

    @Test
    void getOrganizationProducts_success_format() {
        given().when().get(ORGANIZATION_PRODUCTS_URL, ORGANIZATION_SAMPLE_ID).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.ORGANIZATION_PRODUCTS_SCHEMA_PATH));
    }

    //
    // GET /organizations/{organizationID}/products/{productID}
    //
    @Test
    void getOrganizationProduct_success() {
        given().when().get(ORGANIZATION_PRODUCT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID).then()
                .statusCode(200);
    }

    @Test
    void getOrganizationProduct_success_format() {
        given().when().get(ORGANIZATION_PRODUCT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID).then()
                .assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.ORGANIZATION_PRODUCT_SCHEMA_PATH));
    }

    //
    // POST /form/{id}/organizations
    //
    @Test
    void postOrganizationProduct_requireAuth() {
        // auth is triggered after CSRF for non GET requests
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .auth().none().contentType(ContentType.JSON).body(generateSample()).when()
                .post(ORGANIZATION_PRODUCTS_URL, ORGANIZATION_SAMPLE_ID).then().statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postOrganizationProduct_csrfGuard() {
        // happens after auth, once the request is processed
        given().contentType(ContentType.JSON).body(generateSample()).when()
                .post(ORGANIZATION_PRODUCTS_URL, ORGANIZATION_SAMPLE_ID).then().statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postOrganizationProduct_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when()
                .post(ORGANIZATION_PRODUCTS_URL, ORGANIZATION_SAMPLE_ID).then().statusCode(204);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postOrganizationProduct_success_pushFormat() {
        // Check that the input matches what is specified in spec
        String json = generateSample();
        Assertions.assertTrue(
                matchesJsonSchemaInClasspath(SchemaNamespaceHelper.ORGANIZATION_PRODUCT_SCHEMA_PATH).matches(json));

        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(json).when().post(ORGANIZATION_PRODUCTS_URL, ORGANIZATION_SAMPLE_ID)
                .then().statusCode(204);

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(json).when().post(ORGANIZATION_PRODUCTS_URL, ORGANIZATION_SAMPLE_ID).then().assertThat()
                .statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.TEXT).body(json).when().post(ORGANIZATION_PRODUCTS_URL, ORGANIZATION_SAMPLE_ID)
                .then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.XML).body(json).when().post(ORGANIZATION_PRODUCTS_URL, ORGANIZATION_SAMPLE_ID)
                .then().statusCode(500);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postOrganizationProduct_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when()
                .post(ORGANIZATION_PRODUCTS_URL, ORGANIZATION_SAMPLE_ID).then().statusCode(204);

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(generateSample()).when().post(ORGANIZATION_PRODUCTS_URL, ORGANIZATION_SAMPLE_ID).then()
                .statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.TEXT).body(generateSample()).when()
                .post(ORGANIZATION_PRODUCTS_URL, ORGANIZATION_SAMPLE_ID).then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.XML).body(generateSample()).when()
                .post(ORGANIZATION_PRODUCTS_URL, ORGANIZATION_SAMPLE_ID).then().statusCode(500);
    }

    //
    // PUT /form/{id}/organizations/{organizationId}
    //
    @Test
    void putOrganizationProductByID_requireAuth() {
        // auth is triggered after CSRF for non GET requests
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .auth().none().contentType(ContentType.JSON).body(generateSample()).when()
                .put(ORGANIZATION_PRODUCT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID).then()
                .statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putOrganizationProductByID_csrfGuard() {
        // happens after auth, once the request is processed
        given().contentType(ContentType.JSON).body(generateSample()).when()
                .put(ORGANIZATION_PRODUCT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID).then()
                .statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putOrganizationProductByID_empty() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).when()
                .put(ORGANIZATION_PRODUCT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID).then()
                .statusCode(500);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putOrganizationProductByID_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when()
                .put(ORGANIZATION_PRODUCT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID).then()
                .statusCode(200);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putOrganizationProductByID_success_pushFormat() {
        SessionFilter sessionFilter = new SessionFilter();
        // Check that the input matches what is specified in spec
        String json = generateSample();
        Assertions.assertTrue(
                matchesJsonSchemaInClasspath(SchemaNamespaceHelper.ORGANIZATION_PRODUCT_SCHEMA_PATH).matches(json));

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(json).when()
                .put(ORGANIZATION_PRODUCT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID).then()
                .statusCode(200);

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(json).when().put(ORGANIZATION_PRODUCT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID)
                .then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.TEXT).body(json).when()
                .put(ORGANIZATION_PRODUCT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID).then()
                .statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.XML).body(json).when()
                .put(ORGANIZATION_PRODUCT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID).then()
                .statusCode(500);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putOrganizationProductByID_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when()
                .put(ORGANIZATION_PRODUCT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID).then()
                .assertThat().body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.ORGANIZATION_PRODUCT_SCHEMA_PATH))
                .statusCode(200);
        // asserts content type of output for integrity
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).accept(ContentType.TEXT).body(generateSample()).when()
                .put(ORGANIZATION_PRODUCT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID).then()
                .statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).accept(ContentType.XML).body(generateSample()).when()
                .put(ORGANIZATION_PRODUCT_BY_ID_URL, ORGANIZATION_SAMPLE_ID, ORGANIZATION_PRODUCT_ID).then()
                .statusCode(500);
    }

    private String generateSample() {
        try {
            return mapper.writeValueAsString(OrganizationProductData.builder().setDescription("description")
                    .setName("name").setOrganizationId(15).setProductId(1).setProductUrl("url").build());
        } catch (JsonProcessingException e) {
        }
        return null;
    }
}
