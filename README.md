# react-eclipsefdn-members

[![Netlify Status](https://api.netlify.com/api/v1/badges/b0087dce-17ae-46f6-bbea-b3813d35be3f/deploy-status)](https://app.netlify.com/sites/eclipsefdn-react-members/deploys)

Supported by our member organizations, the Eclipse Foundation provides our community with Intellectual Property, Mentorship, Marketing, Event and IT Services.


<!-- TOC -->
- [Getting Started](#getting-started)
- [CSRF and API Security](#csrf-and-api-security)
- [Running the project in included web server](#running-the-project-in-included-web-server)
  - [Dependencies to run](#dependencies-to-run)
  - [Setup](#setup)
    - [Local Running instances](#local-running-instances)
    - [Environment and properties](#environment-and-properties)
  - [Running](#running)
  - [Docker](#docker)
    - [Generate Certs for HTTPS](#generate-certs-for-https)
    - [Update your Host file](#update-your-host-file)
  - [MariaDB setup](#mariadb-setup)
  - [KeyCloak Setup](#keycloak-setup)
    - [Create realms](#create-realms)
    - [Eclipse Foundation as an Identity Provider](#eclipse-foundation-as-an-identity-provider)
    - [Client Configuration](#client-configuration)
- [Contributing](#contributing)
  - [Declared Project Licenses](#declared-project-licenses)
- [Bugs and feature requests](#bugs-and-feature-requests)
- [Authors](#authors)
- [Trademarks](#trademarks)
- [Copyright and license](#copyright-and-license)
<!-- /TOC -->

## Getting Started

Before you start, please make sure you have [yarn](https://classic.yarnpkg.com/en/docs/install/) installed.

Once that's done, you can install dependencies, build assets and start a dev server:

```bash
yarn --cwd src/main/www
yarn --cwd src/main/www build
yarn --cwd src/main/www start
yarn --cwd src/main/www start-spec
```

The web app will run in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.


## CSRF and API Security
Currently, the endpoints that can contain personal data of users have been secured by OIDC and CSRF. What this means for development in the front end is all requests will need to be performed with a legitimate Eclipse Foundation login and account for the CSRF header.

Pertaining to data posted to the API, there is no current automatic deletion policy enforced, and no current way in the UI to send a call to delete data. If you wish to delete this data, you will need to craft javascript within the site to take advantage of the session and CSRF headers, and manually make the call. More information on the form deletion endpoint can be seen in the OpenAPI spec under `/spec/openapi.yml`.

Additionally, when requesting any PII/form data, a CSRF token will need to be passed unless disabled on a development server. This token will live under the `x-csrf-token` header that is supplied on every request the user makes to the server, including the unprotected `/csrf/` endpoint that is available. The token should be posted back to the server using the same header. This value will remain the same for the duration of the browser session.

[^ Top](#react-eclipsefdn-members)
## Running the project in included web server

### Dependencies to run

- Docker-compose
- Maven
- Java version 11
- Make

[^ Top](#react-eclipsefdn-members)
### Setup

#### Environment and properties

The following setup assumes for development that a user with universal write permissions is used for database access. If a different setup exists in your local dev environment, then some tweaks to the data post-setup will need to be done. To start with, `make pre-setup` should be done to initialize the local environment file `.env`. Once run, check the file generated to see the required environment variables for setup. If empty or incorrect, please fill out the variables according to your local environment

The setup for this project assumes that MariaDB and Keycloak are running in your local environment and are ready to interact with, including databases and realms. If this is not the case, please follow the steps outlined in the [Keycloak setup](#keycloak-setup) and [MariaDB setup](#mariadb-setup) sections. Once the Keycloak setup is done, the client ID and credentials should be set into the `.env` file under the properties that match below.

- community: rem_api -> MEMBERSHIP_OIDC_CLIENT_*
- foundation: rem_client -> PORTAL_OIDCCLIENT_CLIENT_*
- foundation: fdb_api -> FOUNDATION_OIDC_CLIENT_*

For the APPLICATION_OIDCCLIENT_CLIENT_* fields, this requires a special API token with elevated read permissions to properly query the Accounts API. This value can be passed on from another team member internally. In the future we will migrate this to use Keycloak as well to simplify access and make it more friendly to outside running of the project.

Once this is done, `make setup` can be run to generate the `secret.properties` files used for the various facets of the application. While there are a few values to plugin, these values are very variable and are unique to the machine rather than easily configurable. If you are using settings outside the default, `quarkus.oidc.auth-server-url` and `quarkus.oidc-client.auth-server-url` properties will need to be overridden in the secret.properties files generated under the foundationdb, portal, and application folders in the config folder. The URLs should be updated to point at the base of the realm for the external realm in question for Quarkus to properly discover the endpoints.

With these properties updated, the server should be able to start and authenticate properly. The command `make compile-start` can be used to then fully build the project and start it in docker, tethered to the current terminal session. 

As a side note, the insertion of data into the database can be enabled/disabled for development environments by setting the following fields within `src/main/resources/application.properties`:

1. Setting `%dev.eclipse.dataloader.enabled` to true/false. This property is what enables the Data bootstrap to load in mock data.

[^ Top](#react-eclipsefdn-members)
### Running

To run the server as a local instance as a stack, you will need to compile the application first, which can be done through `make compile-start`. This takes care of all of the steps needed to cleanly build and rebuild the application from scratch. This will also run the stack with the packaged application.

[^ Top](#react-eclipsefdn-members)
### Docker

We include a `docker-compose.infra.yml` file with this project to help you get started. This includes:

* [mariadb:latest](https://hub.docker.com/_/mariadb)
* [jboss/keycloak:11.0.1](https://hub.docker.com/r/jboss/keycloak/)

#### Generate Certs for HTTPS

, You will need to create a certificate in order to serve the Application on https. Make sure that the Common Name (e.g. server FQDN or YOUR name) is set to `www.rem.docker`.

```sh
make generate-cert
```

#### Update your Host file

We use [jwilder/nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy) as automated Nginx reverse proxy for our docker containers. So instead of having to lookup the port of a new service, you can simply remember it's internal dev hostname.

Different operating system, different file paths!

Windows: C:\Windows\System32\drivers\etc\hosts
Linux / MacOS: /etc/hosts

```
# rem services

127.0.0.1 keycloak
127.0.0.1 api.rem.docker
127.0.0.1 www.rem.docker
127.0.0.1 nginx.rem.docker
```

[^ Top](#react-eclipsefdn-members)
### MariaDB setup

The base definitions of the tables as required for the connection of this API are defined under `./application/src/main/resources/sql/rem_ddl.sql`, `./portal/src/main/resources/sql/eclipsedb_ddl.sql`, `./portal/src/main/resources/sql/dashboard_ddl.sql`, and a final file from the FoundationDB API (TODO). These should all be added present in the MariaDB instance to support this API. 

### Keycloak Setup

#### Create realms

Realm is a concept in Keycloak that refers to an object managing a set of users along with their credentials, roles and groups. To create a `realm`, visit [Keycloak Admin Console](http://localhost:8080/auth/admin), mouse hover where it says `master` and click on `Add Realm`, set the name to `community` and click `create`. Repeat this and additionally create a `foundation` realm.

#### Eclipse Foundation as an Identity Provider

It's possible to delegate authentication to third party identity providers with Keycloak. With this App, we want to leverage [Eclipse Foundation OpenID Connect](https://wiki.eclipse.org/OpenID) since we want our users to login with our standard login page. To do so, you will need a client_id/secret from us.

Assuming you have access to that already, please follow these steps to add the Eclipse Foundation as an `Identity Provider`.

1. Click on `Identity Providers` in the left menu then click on `Add provider...`. Select `OpenID Connect v1.0` from the dropdown menu.

2. Populate the form with the following information:

```
Alias : eclipsefdn
Display Name: Eclipse Foundation
Sync Mode : Force (To make sure the user is updated each time they login)
Authorization URL: https://accounts.eclipse.org/oauth2/authorize
Token URL: https://accounts.eclipse.org/oauth2/token
Logout URL: https://accounts.eclipse.org/oauth2/revoke
User Info URL: https://accounts.eclipse.org/oauth2/UserInfo
Client Authentication: Client secret sent as post
Client ID: <CLIENT_ID>
Client Secret: <CLIENT_SECRET>
Default Scopes: openid profile email offline_access
```

1. Finally, we want to configure Eclipse Foundation has the only authentication option. Click on `Authentication` in the left menu. Set `Identity Provider Redirector` to `required` and `Forms` to `disabled`. Finally, click on Actions and set `eclipsefdn` has the `Default Identity Provider`.


#### Client Configuration

The `clients` tab allows you to manage the list of allowed applications.

To create a client, click on `Clients` in the left menu. You can set the client_id to `rem_app` and the `Root URL` to `http://localhost:3000`. Make sure that the `Client Protocol` is set to `openid-connect` and the `Access Type` is set to `confidential`.

- Foundation realm
  - rem_api client
    - Root URL = ""
    - Client protocol = ""
    - Access type = ""
  - fdn_api client
    - Root URL = ""
    - Client protocol = ""
    - Access type = ""
- Community realm
  - rem_app client
    - Root URL = "http://localhost:3000"
    - Client protocol = "openid-connect"
    - Access type = "confidential"

An additional client will be required for the FoundationDB API access. Information on setting up this client should be defined under the FoundationDB API README file. Once the client is acquired, it will need to have its client ID and secret set in the secret.properties file. They will be respectively set under the properties `quarkus.oidc-client.client-id` and `quarkus.oidc-client.credentials.secret`. The URL of the client within the FoundationDB API realm will need to also be set within the secret.properties under the `quarkus.oidc-client.auth-server-url` property.

To enable connections to the FoundationDB API, 2 clients will need to be created in the same realm as was created for the FoundationDB API service. This second client will be set up similarly to the first, but have service accounts enabled. Once enabled, roles will need to be set within the service account giving all org related roles, as well as sys read access. This should properly restrict service access to the API.

[^ Top](#react-eclipsefdn-members)
## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [membership.eclipse.org](https://gitlab.eclipse.org/eclipsefdn/it/websites/membership.eclipse.org) repository
2. Clone repository: `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/membership.eclipse.org.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

[^ Top](#react-eclipsefdn-members)
### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

[^ Top](#react-eclipsefdn-members)
## Bugs and feature requests

Have a bug or a feature request? Please search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.eclipse.org/eclipsefdn/it/websites/membership.eclipse.org/-/issues).

[^ Top](#react-eclipsefdn-members)
## Authors

**Christopher Guindon (Eclipse Foundation)**

- <https://twitter.com/chrisguindon>
- <https://github.com/chrisguindon>

**Martin Lowe (Eclipse Foundation)**

- <https://github.com/autumnfound>

**Zhou Fang (Eclipse Foundation)**

- <https://github.com/linkfang>

[^ Top](#react-eclipsefdn-members)
## Trademarks

* Eclipse® is a Trademark of the Eclipse Foundation, Inc.
* Eclipse Foundation is a Trademark of the Eclipse Foundation, Inc.

[^ Top](#react-eclipsefdn-members)
## Copyright and license

Copyright 2018-2022 the [Eclipse Foundation, Inc.](https://www.eclipse.org) and the [membership.eclipse.org authors](https://gitlab.eclipse.org/eclipsefdn/it/websites/membership.eclipse.org/-/graphs/master). Code released under the [Eclipse Public License Version 2.0 (EPL-2.0)](https://gitlab.eclipse.org/eclipsefdn/it/websites/membership.eclipse.org/-/raw/master/README.md).
