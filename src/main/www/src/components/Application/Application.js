import { useCallback, useContext, useEffect, useState } from 'react';
import { Switch, Route, Redirect, useHistory } from 'react-router-dom';
import { useFormik } from 'formik';
import SignIn from './SignIn/SignIn';
import {
  application_api_prefix,
  CONTACT_TYPE,
  END_POINT,
  FETCH_HEADER,
  getCurrentMode,
  HAS_TOKEN_EXPIRED,
  LOGIN_EXPIRED_MSG,
  MODE_REACT_ONLY,
  PAGE_STEP,
  ROUTE_COMPANY,
  ROUTE_MEMBERSHIP,
  ROUTE_REVIEW,
  ROUTE_SIGNING,
  ROUTE_SUBMITTED,
  ROUTE_WGS,
} from '../../Constants/Constants';
import { initialValues } from '../UIComponents/FormComponents/formFieldModel';
import CompanyInformation from './CompanyInformation/CompanyInformation';
import MembershipLevel from './MembershipLevel/MembershipLevel';
import WorkingGroupsWrapper from './WorkingGroups/WorkingGroupsWrapper';
import SigningAuthority from './SigningAuthority/SigningAuthority';
import Review from './Review/Review';
import Step from '../UIComponents/Steppers/Step';
import SignInIntroduction from './SignIn/SignInIntroduction';
import SubmitSuccess from './SubmitSuccess/SubmitSuccess';
import { VALIDATION_SCHEMA_FOR_ENROLMENT_FORM } from '../UIComponents/FormComponents/ValidationSchema';
import {
  fetchWrapperForm,
  checkValidityWithoutSubmitting,
  isProd,
  matchCompanyFieldsToBackend,
  matchContactFieldsToBackend,
  matchMembershipLevelFieldsToBackend,
  matchWGFieldsToBackend,
} from '../../Utils/formFunctionHelpers';
import MembershipContext from '../../Context/MembershipContext';
import TopSlideMsg from '../UIComponents/Notifications/TopSlideMsg';

export default function Application() {
  const history = useHistory();
  const { currentUserForm, currentFormId, furthestPage, setCurrentUserForm } = useContext(MembershipContext);

  const [updatedFormValues, setUpdatedFormValues] = useState(initialValues);
  const [isStartNewForm, setIsStartNewForm] = useState(true);
  const [isLoginExpired, setIsLoginExpired] = useState(false);
  const [isTermChecked, setIsTermChecked] = useState(false);
  const [fullWorkingGroupList, setFullWorkingGroupList] = useState([]);
  const [workingGroupsUserJoined, setWorkingGroupsUserJoined] = useState([]);

  const submitCompanyInfo = () => {
    const { values, setFieldValue } = formik;
    !isProd && console.log('updated company info: ', values);

    const onSuccessOrg = (data) => {
      // Add id to local data after a successful POST
      if (values.organization.address.id) {
        return;
      }
      setFieldValue('organization.address.id', data.address?.id || data[0]?.address?.id || '');
    };
    fetchWrapperForm(
      currentFormId,
      END_POINT.organizations,
      matchCompanyFieldsToBackend(values.organization, currentFormId),
      onSuccessOrg
    );

    const onSuccessMemberRep = (data) => {
      // Add id to local data after a successful POST
      if (values.representative.member.id) {
        return;
      }
      setFieldValue('representative.member.id', data.id || data[0]?.id || '');
    };
    fetchWrapperForm(
      currentFormId,
      END_POINT.contacts,
      matchContactFieldsToBackend(values.representative.member, CONTACT_TYPE.COMPANY, currentFormId),
      onSuccessMemberRep
    );

    const onSuccessMarketingRep = (data) => {
      if (values.representative.marketing.id) {
        return;
      }
      setFieldValue('representative.marketing.id', data.id || data[0]?.id || '');
    };
    fetchWrapperForm(
      currentFormId,
      END_POINT.contacts,
      matchContactFieldsToBackend(values.representative.marketing, CONTACT_TYPE.MARKETING, currentFormId),
      onSuccessMarketingRep
    );

    const onSuccessAccountingRep = (data) => {
      if (values.representative.accounting.id) {
        return;
      }
      setFieldValue('representative.accounting.id', data.id || data[0]?.id || '');
    };
    fetchWrapperForm(
      currentFormId,
      END_POINT.contacts,
      matchContactFieldsToBackend(values.representative.accounting, CONTACT_TYPE.ACCOUNTING, currentFormId),
      onSuccessAccountingRep
    );

    fetchWrapperForm(
      currentFormId,
      '',
      matchMembershipLevelFieldsToBackend(values, currentFormId, currentUserForm.name)
    );

    let isWGRepSameAsCompany = false;
    values.workingGroups.map(
      (wg) => (isWGRepSameAsCompany = wg.workingGroupRepresentative?.sameAsCompany || isWGRepSameAsCompany)
    );
    // only do this API call when there is at least 1 WG rep is same as company rep
    isWGRepSameAsCompany &&
      values.workingGroups.forEach((item) => {
        fetchWrapperForm(currentFormId, END_POINT.working_groups, matchWGFieldsToBackend(item, currentFormId));
      });

    // Only make the API call when signingAuthority Representative has an id and sameAsCompany is true
    // If not, it means there is nothing in the db, so no need to update.
    if (values.signingAuthorityRepresentative.id && values.signingAuthorityRepresentative.sameAsCompany) {
      fetchWrapperForm(
        currentFormId,
        END_POINT.contacts,
        matchContactFieldsToBackend(values.signingAuthorityRepresentative, CONTACT_TYPE.SIGNING, currentFormId)
      );
    }
  };

  const submitMembershipLevel = () => {
    const { values } = formik;
    !isProd && console.log('updated membership level: ', values);
    fetchWrapperForm(
      currentFormId,
      '',
      matchMembershipLevelFieldsToBackend(values, currentFormId, currentUserForm.name)
    );
  };

  const submitWorkingGroups = () => {
    const { values, setFieldValue } = formik;
    !isProd && console.log('updated working groups: ', values);

    if (!values.skipJoiningWG) {
      // If the user is joining at least 1 wg, then make related API call
      const onSuccessWG = (data, item, index) => {
        if (item.id) {
          return;
        }
        const newWGArray = values.workingGroups.map((wg, i) =>
          i === index ? { ...wg, id: data.id || data[0]?.id || '' } : wg
        );
        setFieldValue('workingGroups', newWGArray);
      };
      values.workingGroups.forEach((item, index) => {
        fetchWrapperForm(currentFormId, END_POINT.working_groups, matchWGFieldsToBackend(item, currentFormId), (data) =>
          onSuccessWG(data, item, index)
        );
      });
    }
  };

  const submitSigningAuthority = () => {
    const { values, setFieldValue } = formik;
    !isProd && console.log('updated SigningAuthority: ', values);

    const onSuccessSigningAuthority = (data) => {
      if (values.signingAuthorityRepresentative.id) {
        return;
      }
      setFieldValue('signingAuthorityRepresentative.id', data.id || data[0].id || '');
    };
    fetchWrapperForm(
      currentFormId,
      END_POINT.contacts,
      matchContactFieldsToBackend(values.signingAuthorityRepresentative, CONTACT_TYPE.SIGNING, currentFormId),
      onSuccessSigningAuthority
    );
  };

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: VALIDATION_SCHEMA_FOR_ENROLMENT_FORM,
  });

  const handleLoginExpired = useCallback(() => {
    if (sessionStorage.getItem(HAS_TOKEN_EXPIRED)) {
      sessionStorage.setItem(HAS_TOKEN_EXPIRED, '');
      // using setTimeout here is to make the pop up message more noticeable
      setTimeout(() => {
        setIsLoginExpired(true);
      }, 200);
    }
  }, []);

  useEffect(() => {
    handleLoginExpired();
  }, [handleLoginExpired]);

  useEffect(() => {
    if (getCurrentMode() === MODE_REACT_ONLY) {
      setCurrentUserForm({
        name: 'Anonymous',
      });
      return;
    }

    const getUserInfo = () => {
      fetch(application_api_prefix() + `/${END_POINT.userinfo}`, { headers: FETCH_HEADER })
        .then((res) => res.json())
        .then((data) => {
          !isProd && console.log('user info: ', data); // {family_name: "User1", given_name: "User1", name: "user1"}
          setCurrentUserForm({
            ...data,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    };
    getUserInfo();
  }, [setCurrentUserForm]);

  // generate the step options above the form
  const renderStepper = () => (
    <div className="stepper">
      {PAGE_STEP.map((pageStep, index) => {
        return (
          <Step
            key={index}
            title={pageStep.label}
            index={index}
            pathName={pageStep.pathName}
            updatedFormValues={updatedFormValues}
            formik={{
              ...formik,
              submitCompanyInfo: submitCompanyInfo,
              submitMembershipLevel: submitMembershipLevel,
              submitWorkingGroups: submitWorkingGroups,
              submitSigningAuthority: submitSigningAuthority,
            }}
          />
        );
      })}
    </div>
  );

  return (
    <>
      <Switch>
        <Route exact path="/">
          <Redirect to="/sign-in" />
        </Route>

        <Route exact path="/sign-in">
          <SignInIntroduction />
          {renderStepper()}
          <SignIn
            history={history}
            setIsStartNewForm={setIsStartNewForm}
            resetForm={formik.resetForm}
            updatedFormValues={updatedFormValues}
            setUpdatedFormValues={setUpdatedFormValues}
          />
        </Route>

        <Route path={ROUTE_COMPANY}>
          <form onSubmit={checkValidityWithoutSubmitting}>
            {renderStepper()}
            {
              // stop users visiting steps/pages that are not able to edit yet
              furthestPage.index >= 1 ? (
                <CompanyInformation
                  formik={formik}
                  isStartNewForm={isStartNewForm}
                  fullWorkingGroupList={fullWorkingGroupList}
                  setFullWorkingGroupList={setFullWorkingGroupList}
                  setWorkingGroupsUserJoined={setWorkingGroupsUserJoined}
                  updatedFormValues={updatedFormValues}
                  setUpdatedFormValues={setUpdatedFormValues}
                  submitForm={submitCompanyInfo}
                />
              ) : (
                // if uses are not allowed to visit this page,
                // then will be brought back to the furthest they can visit
                <Redirect to={furthestPage.pathName} />
              )
            }
          </form>
        </Route>

        <Route path={ROUTE_MEMBERSHIP}>
          <form onSubmit={checkValidityWithoutSubmitting}>
            {renderStepper()}
            {furthestPage.index >= 2 ? (
              <MembershipLevel
                formik={formik}
                updatedFormValues={updatedFormValues}
                setUpdatedFormValues={setUpdatedFormValues}
                submitForm={submitMembershipLevel}
              />
            ) : (
              <Redirect to={furthestPage.pathName} />
            )}
          </form>
        </Route>

        <Route path={ROUTE_WGS}>
          <form onSubmit={checkValidityWithoutSubmitting}>
            {renderStepper()}
            {furthestPage.index >= 3 ? (
              <WorkingGroupsWrapper
                formik={formik}
                isStartNewForm={isStartNewForm}
                fullWorkingGroupList={fullWorkingGroupList}
                workingGroupsUserJoined={workingGroupsUserJoined}
                updatedFormValues={updatedFormValues}
                setUpdatedFormValues={setUpdatedFormValues}
                submitForm={submitWorkingGroups}
              />
            ) : (
              <Redirect to={furthestPage.pathName} />
            )}
          </form>
        </Route>

        <Route path={ROUTE_SIGNING}>
          <form onSubmit={checkValidityWithoutSubmitting}>
            {renderStepper()}
            {furthestPage.index >= 4 ? (
              <SigningAuthority
                formik={formik}
                updatedFormValues={updatedFormValues}
                submitForm={submitSigningAuthority}
                setUpdatedFormValues={setUpdatedFormValues}
              />
            ) : (
              <Redirect to={furthestPage.pathName} />
            )}
          </form>
        </Route>

        <Route path={ROUTE_REVIEW}>
          {renderStepper()}
          {furthestPage.index >= 5 ? (
            <Review values={formik.values} isTermChecked={isTermChecked} setIsTermChecked={setIsTermChecked} />
          ) : (
            <Redirect to={furthestPage.pathName} />
          )}
        </Route>

        <Route path={ROUTE_SUBMITTED}>
          {furthestPage.index >= 6 ? <SubmitSuccess /> : <Redirect to={furthestPage.pathName} />}
        </Route>

        <Redirect to="/" />
      </Switch>

      <TopSlideMsg
        shouldShowUp={isLoginExpired}
        msgContent={LOGIN_EXPIRED_MSG}
        setShouldShowUp={setIsLoginExpired}
        isError={true}
      />
    </>
  );
}
