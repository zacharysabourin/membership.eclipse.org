export default function NoAccess() {
  return (
    <div style={{ height: 'calc(100vh - 215px)' }}>
      <h1 className="margin-bottom-40">No Access</h1>
      <p>
        It looks like you do not have access to this page. If you believe this is in error, please reach out to{' '}
        <a href="mailto:membership.coordination@eclipse-foundation.org">
          membership.coordination@eclipse-foundation.org.
        </a>
      </p>
    </div>
  );
}
