import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import { useEffect } from 'react';
import { errorRed, successGreen } from '../../../Constants/Constants';

export default function TopSlideMsg({ shouldShowUp, setShouldShowUp, isError, msgContent, displayPeriod }) {
  useEffect(() => {
    if (shouldShowUp) {
      setTimeout(() => {
        setShouldShowUp(false);
      }, displayPeriod || 4000);
    }
  }, [shouldShowUp, setShouldShowUp, displayPeriod]);
  return (
    <div className={shouldShowUp ? 'msg-popup msg-popup-show' : 'msg-popup'}>
      {isError ? (
        <ErrorOutlineIcon style={{ color: errorRed, fontSize: 30 }} />
      ) : (
        <CheckCircleOutlineIcon style={{ color: successGreen, fontSize: 30 }} />
      )}
      <span>{msgContent}</span>
    </div>
  );
}
