import { GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';
import { CustomToolbarProps } from '../../../Interfaces/portal_interface';

export default function CustomToolbar({ fileName }: CustomToolbarProps) {
  return (
    <GridToolbarContainer>
      <GridToolbarExport csvOptions={{ fileName: fileName }} />
    </GridToolbarContainer>
  );
}
