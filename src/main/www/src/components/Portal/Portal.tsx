import 'eclipsefdn-solstice-assets/js/ga';
import 'eclipsefdn-solstice-assets/js/privacy';
import { Switch, Route, Redirect } from 'react-router-dom';
import { CircularProgress, Theme } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import LeftNavBar from './NavBar/LeftNavBar';
import {
  api_prefix,
  END_POINT,
  FETCH_HEADER,
  getCurrentMode,
  LOGIN_FROM_KEY,
  mainContentBGColor,
  MODE_REACT_ONLY,
  ORIGINAL_PATH_KEY,
} from '../../Constants/Constants';
import Dashboard from './Dashboard/Dashboard';
import AppTopBar from './NavBar/AppTopBar';
import OrgProfile from './OrgProfile/OrgProfiles';
import ContactManagement from './ContactManagement/ContactManagement';
import { useEffect, useContext, useState } from 'react';
import PortalFooter from './PortalFooter';
import PortalLogin from './Login/PortalLogin';
import YourProjects from './YourProjects/YourProjects';
import PortalContext from '../../Context/PortalContext';
import { isProd } from '../../Utils/formFunctionHelpers';
import GlobalContext from '../../Context/GlobalContext';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    content: {
      position: 'relative',
      minHeight: 'calc(100vh - 65px)',
      flexGrow: 1,
      padding: theme.spacing(2.5, 1.5, 13.5),
      backgroundColor: mainContentBGColor,
      [theme.breakpoints.up('sm')]: {
        padding: theme.spacing(2.5, 2.5, 8.5),
      },
      [theme.breakpoints.up('md')]: {
        marginLeft: theme.spacing(28),
        padding: theme.spacing(6.5, 6.5, 11.5),
        marginTop: theme.spacing(6.5),
      },
    },
    loadingContainer: {
      backgroundColor: '#fff',
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    loginFooter: {
      '& div, & div a': {
        color: 'white',
        textShadow: '1px 1px #000',
      },
    },
  })
);

const originalPath = window.location.pathname !== '/portal/login' ? window.location.pathname : '/portal/dashboard';

export default function Portal({
  isFetchingUser,
  setIsFetchingUser,
}: {
  isFetchingUser: boolean;
  setIsFetchingUser: (param: boolean) => void;
}) {
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = useState(false);
  const { gotCSRF } = useContext(GlobalContext);
  const { setOrgId, currentUserPortal, setCurrentUserPortal } = useContext(PortalContext);
  // The loggedIn will only be true when the user is logged in and he/she is at least linked to 1 org
  const loggedIn = currentUserPortal !== null && currentUserPortal.relation.length > 0;
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  useEffect(() => {
    if (getCurrentMode() === MODE_REACT_ONLY) {
      setOrgId(731);
      setCurrentUserPortal({
        name: 'Anonymous',
        relation: ['CR', 'EMPLY'],
      });
      return;
    }

    const getUserInfo = () => {
      fetch(api_prefix() + `/${END_POINT.userinfo}`, { headers: FETCH_HEADER })
        .then((res) => res.json())
        .then((data) => {
          !isProd && console.log('user info: ', data); // {family_name: "User1", given_name: "User1", name: "user1"}

          const orgRoles = data.organizational_roles;
          // For now, 1 user will only belong to 1 org, so just get the 1st/only id in it
          const orgId = parseInt(Object.keys(orgRoles)?.[0]) || 0;
          setOrgId(orgId);
          setCurrentUserPortal({
            ...data,
            relation: orgId ? orgRoles[orgId] : [],
          });
          setIsFetchingUser(false);
        })
        .catch((err) => {
          setIsFetchingUser(false);
          console.log(err);
        });
    };
    getUserInfo();
  }, [setCurrentUserPortal, setIsFetchingUser, setOrgId]);

  useEffect(() => {
    !loggedIn
      ? sessionStorage.setItem(ORIGINAL_PATH_KEY, originalPath)
      : sessionStorage.setItem(ORIGINAL_PATH_KEY, '/portal');
  }, [loggedIn]);

  useEffect(() => {
    getCurrentMode() === MODE_REACT_ONLY && setIsFetchingUser(false);
  }, [setIsFetchingUser]);

  useEffect(() => {
    localStorage.setItem(LOGIN_FROM_KEY, 'Form');
  }, []);

  return (
    <>
      {loggedIn && (
        <>
          <AppTopBar handleDrawerToggle={handleDrawerToggle} />
          <LeftNavBar mobileOpen={mobileOpen} handleDrawerToggle={handleDrawerToggle} />
        </>
      )}
      {/* When current user has not logged in, we need to show login page, so the main content part should be hidden */}
      <main className={loggedIn ? classes.content : ''}>
        {gotCSRF && !isFetchingUser ? (
          <>
            <Switch>
              <Route path="/portal/login">
                {loggedIn && <Redirect to={sessionStorage.getItem(ORIGINAL_PATH_KEY)} />}
                <PortalLogin isFetchingUser={isFetchingUser} setIsFetchingUser={setIsFetchingUser} />
              </Route>
              <Route path="/portal/dashboard">
                {!loggedIn && <Redirect to="/portal/login" />}
                <Dashboard />
              </Route>
              <Route exact path="/portal/org-profile">
                {!loggedIn && <Redirect to="/portal/login" />}
                <OrgProfile />
              </Route>
              <Route exact path="/portal/contact-management">
                {!loggedIn && <Redirect to="/portal/login" />}
                <ContactManagement />
              </Route>
              <Route exact path="/portal/your-projects">
                {!loggedIn && <Redirect to="/portal/login" />}
                <YourProjects />
              </Route>
              <Route path="/portal">
                <Redirect to="/portal/dashboard" />
              </Route>
            </Switch>
            <div className={loggedIn ? '' : classes.loginFooter}>
              <PortalFooter />
            </div>
          </>
        ) : (
          <div className={classes.loadingContainer}>
            <CircularProgress />
          </div>
        )}
      </main>
    </>
  );
}
