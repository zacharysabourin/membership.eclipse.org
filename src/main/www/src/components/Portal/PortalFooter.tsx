import { Theme } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { iconGray } from '../../Constants/Constants';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    footer: {
      position: 'absolute',
      fontSize: 12,
      color: iconGray,
      bottom: 10,
      left: 15,
      right: 15,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      [theme.breakpoints.up('sm')]: {
        flexDirection: 'row',
        alignItems: 'stretch',
        justifyContent: 'space-between',
        left: 25,
        right: 25,
      },
      [theme.breakpoints.up('md')]: {
        left: 65,
        right: 65,
      },
    },
    footerLinkContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      marginLeft: theme.spacing(0),
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(2),
      },
      width: 280,
      '& a': {
        color: iconGray,
      },
    },
  })
);

export default function PortalFooter() {
  const classes = useStyles();
  return (
    <div className={classes.footer}>
      <p>Copyright &#169; Eclipse Foundation. All Rights Reserved.</p>
      <div className={classes.footerLinkContainer}>
        <a target="_blank" rel="noreferrer" href="https://www.eclipse.org/legal/privacy.php">
          Privacy Policy
        </a>
        <a target="_blank" rel="noreferrer" href="http://www.eclipse.org/legal/termsofuse.php">
          Terms of Use
        </a>
        <a target="_blank" rel="noreferrer" href="http://www.eclipse.org/legal/copyright.php">
          Copyright Agent
        </a>
      </div>
    </div>
  );
}
