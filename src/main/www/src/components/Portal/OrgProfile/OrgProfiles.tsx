import { createStyles, makeStyles, Typography, Theme } from '@material-ui/core';
import BusinessIcon from '@material-ui/icons/Business';
import { brightOrange, iconGray, PERMISSIONS_BASED_ON_ROLES } from '../../../Constants/Constants';
import { checkPermission } from '../../../Utils/portalFunctionHelpers';
import OrgProfilesBasicInfo from './OrgProfilesBasicInfo';
import OrgProfilesLinks from './OrgProfilesLinks';
import { useContext } from 'react';
import NoAccess from '../../ErrorPages/NoAccess';
import PortalContext from '../../../Context/PortalContext';

const useStyle = makeStyles((theme: Theme) =>
  createStyles({
    headerIcon: {
      fontSize: 80,
      color: iconGray,
      borderBottom: `6px ${brightOrange} solid`,
    },
    pageHeader: {
      margin: theme.spacing(0.5, 0, 4),
    },
  })
);

export default function OrgProfile() {
  const classes = useStyle();
  const { currentUserPortal } = useContext(PortalContext);

  return !checkPermission(PERMISSIONS_BASED_ON_ROLES.accessOrgProfile, currentUserPortal?.relation) ? (
    <NoAccess />
  ) : (
    <>
      <BusinessIcon className={classes.headerIcon} />
      <Typography variant="h4" component="h1" className={classes.pageHeader}>
        Your Organization Profile
      </Typography>

      <OrgProfilesBasicInfo />
      <OrgProfilesLinks />
    </>
  );
}
