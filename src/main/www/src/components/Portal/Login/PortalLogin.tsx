import { Button, CircularProgress } from '@material-ui/core';
import efGRYLogo from '../../../assets/logos/ef-gry.svg';
import { LOGIN_FROM_KEY } from '../../../Constants/Constants';
import { useContext } from 'react';
import PortalContext from '../../../Context/PortalContext';

interface PortalLoginProps {
  isFetchingUser: boolean;
  setIsFetchingUser: (data: boolean) => void;
}

export default function PortalLogin({ isFetchingUser, setIsFetchingUser }: PortalLoginProps) {
  const { currentUserPortal } = useContext(PortalContext);
  const handleSignIn = () => {
    setIsFetchingUser(true);
    localStorage.setItem(LOGIN_FROM_KEY, 'Portal');
    window.location.assign('/api/login');
  };
  return (
    <div className="portal-login-page">
      <div className="portal-login-image"></div>
      <div className="portal-login-container">
        <img src={efGRYLogo} alt="Eclipse Foundation logo" className="margin-top-20" />
        {isFetchingUser ? (
          <div className="margin-top-40">
            <CircularProgress />
          </div>
        ) : currentUserPortal?.name ? (
          <div className="text-container padding-left-30 padding-right-30">
            <p>
              Your account is not linked to a current Eclipse Foundation member. If your organization is a member,
              please reach out to{' '}
              <a href="mailto:membership.coordination@eclipse-foundation.org">
                membership.coordination@eclipse-foundation.org
              </a>{' '}
              to have your account connected to the organization. If you recently connected your account to your
              organization, it may take up to 24 hours to see the changes.
            </p>
            <p className="margin-bottom-30">
              For more information on membership, including how to become a member, visit{' '}
              <a href="https://eclipse.org/membership">eclipse.org/membership</a>.
            </p>
          </div>
        ) : (
          <>
            <p className="h4 text-center margin-20">Get started by logging in with your Eclipse Foundation account:</p>
            <div className="portal-login-btn-container margin-bottom-20">
              <Button variant="contained" color="primary" size="large" onClick={handleSignIn}>
                Log in
              </Button>
              <Button
                variant="contained"
                color="secondary"
                size="large"
                onClick={() => window.location.assign('https://accounts.eclipse.org/')}
              >
                Create an account
              </Button>
            </div>
          </>
        )}
      </div>
    </div>
  );
}
