import { useState } from 'react';
import SectionCtn from '../../../UIComponents/CustomContainer/SectionCtn';
import ModalWindow from '../../../UIComponents/Notifications/ModalWindow';
import { ListItem, ListItemText } from '@material-ui/core';
import DashboardProjects from './DashboardProjects';
import DashboardWGs from './DashboardWGs';
import { SelectedItemArray } from '../../../../Interfaces/portal_interface';

export default function DashboardProjectsAndWG() {
  const [selectedItemArray, setSelectedItemArray] = useState<SelectedItemArray>({
    title: '',
    data: [{ name: '', url: '' }],
  });
  const [open, setOpen] = useState(false);

  const renderAllItems = (itemArray: undefined | Array<any>) =>
    itemArray?.map((item) =>
      item.url ? (
        <ListItem key={item.name} button component="a" href={item.url} target="_blank" rel="noopener">
          {item.name}
        </ListItem>
      ) : (
        <ListItemText
          key={item.name}
          style={{
            padding: '5px 15px',
            minHeight: 30,
          }}
          primary={item.name}
        />
      )
    );

  return (
    <SectionCtn title="Projects and Working Groups" id="projects-wg">
      <DashboardWGs setSelectedItemArray={setSelectedItemArray} setOpen={setOpen} />
      <DashboardProjects />

      <ModalWindow
        title={selectedItemArray?.title}
        content=""
        customContent={() => renderAllItems(selectedItemArray?.data)}
        handleProceed={''}
        shouldOpen={open}
        setShouldOpen={setOpen}
        cancelText={'Close'}
        yesText={false}
      />
    </SectionCtn>
  );
}
