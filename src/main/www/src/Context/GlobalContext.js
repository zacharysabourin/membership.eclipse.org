import React from 'react';

const GlobalContext = React.createContext({
  gotCSRF: false,
  setGotCSRF: () => {},
  succeededToExecute: (msg) => {},
  failedToExecute: (msg, displayPeriod) => {},
});

export default GlobalContext;
