package org.eclipsefoundation.membership.application.resources;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.namespace.RequestHeaderNames;
import org.eclipsefoundation.membership.application.dto.FormWorkingGroup;
import org.eclipsefoundation.membership.application.dto.MembershipForm;
import org.eclipsefoundation.membership.application.model.FormWorkingGroupData;
import org.eclipsefoundation.membership.application.model.mappers.FormWorkingGroupMapper;
import org.eclipsefoundation.membership.application.test.dao.MockHibernateDao;
import org.eclipsefoundation.membership.application.test.helper.AuthHelper;
import org.eclipsefoundation.membership.application.test.helper.DtoHelper;
import org.eclipsefoundation.membership.application.test.helper.SchemaNamespaceHelper;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.ConfigMetadata;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.restassured.filter.session.SessionFilter;
import io.restassured.http.ContentType;

@TestInstance(Lifecycle.PER_CLASS)
@QuarkusTest
class FormWorkingGroupsResourceTest {
    public static final String FORM_WORKING_GROUPS_BASE_URL = MembershipFormResourceTest.FORMS_BY_ID_URL
            + "/working_groups";
    public static final String FORM_WORKING_GROUP_BY_ID_URL = FORM_WORKING_GROUPS_BASE_URL + "/{workingGroupId}";

    @Inject
    ObjectMapper json;
    @Inject
    MockHibernateDao mockDao;
    @Inject
    FilterService filters;
    @Inject
    FormWorkingGroupMapper wgMapper;

    //
    // GET /form/{id}/working_groups
    //
    @Test
    void getFormWorkingGroups_requireAuth() {
        // auth is triggered before CSRF (as the request is dumped before it gets
        // processed)
        given().auth().none().when().get(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID()).then().statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormWorkingGroups_csrfGuard() {
        // happens after auth, once the request is processed
        given().when().get(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID()).then().statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormWorkingGroups_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID()).then().statusCode(200);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormWorkingGroups_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID()).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH));
    }

    //
    // GET /form/{id}/working_groups/{contactId}
    //
    @Test
    void getFormWorkingGroupByID_requireAuth() {
        // auth is triggered before CSRF (as the request is dumped before it gets
        // processed)
        given().auth().none().when().get(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), mockDao.getFormWGID())
                .then().statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormWorkingGroupByID_csrfGuard() {
        // happens after auth, once the request is processed
        given().when().get(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), mockDao.getFormWGID()).then()
                .statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormWorkingGroupByID_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), mockDao.getFormWGID()).then()
                .statusCode(200);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormWorkingGroupByID_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), mockDao.getFormWGID()).then()
                .assertThat().body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.WORKING_GROUP_SCHEMA_PATH));
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .accept(ContentType.JSON).when()
                .get(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), mockDao.getFormWGID()).then().statusCode(200);

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .accept(ContentType.XML).when()
                .get(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), mockDao.getFormWGID()).then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .accept(ContentType.TEXT).when()
                .get(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), mockDao.getFormWGID()).then().statusCode(500);
    }

    //
    // POST /form/{id}/working_groups
    //
    @Test
    void postFormWorkingGroup_requireAuth() {
        // auth is triggered after CSRF for non GET requests
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .auth().none().contentType(ContentType.JSON).body(generateSample()).when()
                .post(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID()).then().statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postFormWorkingGroup_csrfGuard() {
        // happens after auth, once the request is processed
        given().contentType(ContentType.JSON).body(generateSample()).when()
                .post(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID()).then().statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postFormWorkingGroup_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when()
                .post(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID()).then().statusCode(200);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postFormWorkingGroup_success_pushFormat() {
        // Check that the input matches what is specified in spec
        String json = generateSample();
        Assertions.assertTrue(
                matchesJsonSchemaInClasspath(SchemaNamespaceHelper.WORKING_GROUP_SCHEMA_PATH).matches(json));

        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(json).when().post(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID())
                .then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH));
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(json).when().post(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID())
                .then().statusCode(200);

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.TEXT).body(json).when().post(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID())
                .then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.XML).body(json).when().post(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID())
                .then().statusCode(500);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postFormWorkingGroup_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(generateSample()).contentType(ContentType.JSON).when()
                .post(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID()).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH));

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(generateSample()).when().post(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID()).then()
                .statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.TEXT).body(generateSample()).when()
                .post(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID()).then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.XML).body(generateSample()).when()
                .post(FORM_WORKING_GROUPS_BASE_URL, mockDao.getFormID()).then().statusCode(500);
    }

    //
    // PUT /form/{id}/working_groups/{contactId}
    //
    @Test
    void putFormWorkingGroupByID_requireAuth() {
        // create record to update
        FormWorkingGroupData wg = generateSampleRaw();
        List<FormWorkingGroup> wgs = mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(FormWorkingGroup.class), null), Arrays.asList(wgMapper.toDTO(wg, mockDao)));
        // auth is triggered after CSRF for non GET requests
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .auth().none().contentType(ContentType.JSON).body(generateSample()).when()
                .put(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), wgs.get(0).getId()).then().statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormWorkingGroupByID_csrfGuard() {
        // create record to update
        FormWorkingGroupData wg = generateSampleRaw();
        List<FormWorkingGroup> wgs = mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(FormWorkingGroup.class), null), Arrays.asList(wgMapper.toDTO(wg, mockDao)));
        // happens after auth, once the request is processed
        given().contentType(ContentType.JSON).body(generateSample()).when()
                .put(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), wgs.get(0).getId()).then().statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormWorkingGroupByID_empty() {
        // create record to update
        FormWorkingGroupData wg = generateSampleRaw();
        List<FormWorkingGroup> wgs = mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(FormWorkingGroup.class), null), Arrays.asList(wgMapper.toDTO(wg, mockDao)));
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).when()
                .put(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), wgs.get(0).getId()).then().statusCode(500);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormWorkingGroupByID_success() {
        // create record to update
        FormWorkingGroupData wg = generateSampleRaw();
        List<FormWorkingGroup> wgs = mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(FormWorkingGroup.class), null), Arrays.asList(wgMapper.toDTO(wg, mockDao)));
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when()
                .put(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), wgs.get(0).getId()).then().statusCode(200);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormWorkingGroupByID_success_pushFormat() {

        // create record to update
        FormWorkingGroupData wg = generateSampleRaw();
        List<FormWorkingGroup> wgs = mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(FormWorkingGroup.class), null), Arrays.asList(wgMapper.toDTO(wg, mockDao)));

        SessionFilter sessionFilter = new SessionFilter();
        // Check that the input matches what is specified in spec
        String json = generateSample();
        System.out.println(json);
        Assertions.assertTrue(
                matchesJsonSchemaInClasspath(SchemaNamespaceHelper.WORKING_GROUP_SCHEMA_PATH).matches(json));

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(json).contentType(ContentType.JSON).when()
                .put(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), wgs.get(0).getId()).then().statusCode(200);

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(json).when().put(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), wgs.get(0).getId()).then()
                .statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.TEXT).body(json).when()
                .put(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), wgs.get(0).getId()).then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.XML).body(json).when()
                .put(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), wgs.get(0).getId()).then().statusCode(500);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormWorkingGroupByID_success_format() {
        // create record to update
        FormWorkingGroupData wg = generateSampleRaw();
        List<FormWorkingGroup> wgs = mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(FormWorkingGroup.class), null), Arrays.asList(wgMapper.toDTO(wg, mockDao)));

        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when()
                .put(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), wgs.get(0).getId()).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.WORKING_GROUPS_SCHEMA_PATH));
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when()
                .put(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), wgs.get(0).getId()).then()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MEMBERSHIP_FORMS_SCHEMA_PATH)).statusCode(200);

        // asserts content type of output for integrity
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).accept(ContentType.TEXT).body(generateSample()).when()
                .put(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), wgs.get(0).getId()).then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).accept(ContentType.XML).body(generateSample()).when()
                .put(FORM_WORKING_GROUP_BY_ID_URL, mockDao.getFormID(), wgs.get(0).getId()).then().statusCode(500);
    }

    private FormWorkingGroupData generateSampleRaw() {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), mockDao.getFormID());
        MembershipForm mfs = mockDao.getReference(mockDao.getFormID(), MembershipForm.class);
        return wgMapper.toModel(DtoHelper.generateWorkingGroups(mfs).get(0));
    }

    private String generateSample() {
        try {
            return json.writeValueAsString(generateSampleRaw());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
