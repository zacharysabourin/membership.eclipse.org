package org.eclipsefoundation.membership.application.resources;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import javax.inject.Inject;

import org.eclipsefoundation.core.namespace.RequestHeaderNames;
import org.eclipsefoundation.membership.application.dto.FormOrganization;
import org.eclipsefoundation.membership.application.dto.MembershipForm;
import org.eclipsefoundation.membership.application.test.dao.MockHibernateDao;
import org.eclipsefoundation.membership.application.test.helper.AuthHelper;
import org.eclipsefoundation.membership.application.test.helper.DtoHelper;
import org.eclipsefoundation.membership.application.test.helper.SchemaNamespaceHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.ConfigMetadata;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.restassured.filter.session.SessionFilter;
import io.restassured.http.ContentType;

@TestInstance(Lifecycle.PER_CLASS)
@QuarkusTest
class FormOrganizationResourceTest {
    public static final String FORM_ORGANIZATION_BASE_URL = MembershipFormResourceTest.FORMS_BY_ID_URL
            + "/organizations";
    public static final String FORM_ORGANIZATION_BY_ID_URL = FORM_ORGANIZATION_BASE_URL + "/{organizationId}";

    @Inject
    ObjectMapper json;
    @Inject
    MockHibernateDao mockDao;

    //
    // GET /form/{id}/organizations
    //
    @Test
    void getFormOrganizations_requireAuth() {
        // auth is triggered before CSRF (as the request is dumped before it gets
        // processed)
        given().auth().none().when().get(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID()).then().statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormOrganizations_csrfGuard() {
        // happens after auth, once the request is processed
        given().when().get(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID()).then().statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormOrganizations_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID()).then().statusCode(200);
    }

    // @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormOrganizations_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID()).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.FORM_ORGANIZATIONS_SCHEMA_PATH));
    }

    //
    // GET /form/{id}/organizations/{organizationId}
    //
    @Test
    void getFormOrganizationByID_requireAuth() {
        // auth is triggered before CSRF (as the request is dumped before it gets
        // processed)
        given().auth().none().when().get(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID())
                .then().statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormOrganizationByID_csrfGuard() {
        // happens after auth, once the request is processed
        given().when().get(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then()
                .statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormOrganizationByID_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then()
                .statusCode(200);
    }

    // @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormOrganizationByID_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then()
                .assertThat().body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.FORM_ORGANIZATION_SCHEMA_PATH))
                .statusCode(200);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .accept(ContentType.JSON).when()
                .get(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.FORM_ORGANIZATION_SCHEMA_PATH))
                .statusCode(200);

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .accept(ContentType.XML).when()
                .get(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .accept(ContentType.TEXT).when()
                .get(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then().statusCode(500);
    }

    //
    // POST /form/{id}/organizations
    //
    @Test
    void postFormOrganization_requireAuth() {
        // auth is triggered after CSRF for non GET requests
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .auth().none().contentType(ContentType.JSON).body(generateSample()).when()
                .post(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID()).then().statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postFormOrganization_csrfGuard() {
        // happens after auth, once the request is processed
        given().contentType(ContentType.JSON).body(generateSample()).when()
                .post(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID()).then().statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postFormOrganization_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when()
                .post(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID()).then().statusCode(200);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postFormOrganization_success_pushFormat() {
        // Check that the input matches what is specified in spec
        String json = generateSample();
        Assertions.assertTrue(
                matchesJsonSchemaInClasspath(SchemaNamespaceHelper.FORM_ORGANIZATION_SCHEMA_PATH).matches(json));

        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(json).when().post(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID())
                .then().body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.FORM_ORGANIZATION_SCHEMA_PATH))
                .statusCode(200);

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(json).when().post(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID()).then().assertThat()
                .statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.TEXT).body(json).when().post(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID())
                .then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.XML).body(json).when().post(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID())
                .then().statusCode(500);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postFormOrganization_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when()
                .post(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID()).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.FORM_ORGANIZATION_SCHEMA_PATH));

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(generateSample()).when().post(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID()).then()
                .statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.TEXT).body(generateSample()).when()
                .post(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID()).then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.XML).body(generateSample()).when()
                .post(FORM_ORGANIZATION_BASE_URL, mockDao.getFormID()).then().statusCode(500);
    }

    //
    // PUT /form/{id}/organizations/{organizationId}
    //
    @Test
    void putFormOrganizationByID_requireAuth() {
        // auth is triggered after CSRF for non GET requests
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .auth().none().contentType(ContentType.JSON).body(generateSample()).when()
                .put(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then().statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormOrganizationByID_csrfGuard() {
        // happens after auth, once the request is processed
        given().contentType(ContentType.JSON).body(generateSample()).when()
                .put(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then().statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormOrganizationByID_empty() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).when()
                .put(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then().statusCode(500);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormOrganizationByID_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when()
                .put(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then().statusCode(200);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormOrganizationByID_success_pushFormat() {
        SessionFilter sessionFilter = new SessionFilter();
        // Check that the input matches what is specified in spec
        String json = generateSample();
        Assertions.assertTrue(
                matchesJsonSchemaInClasspath(SchemaNamespaceHelper.FORM_ORGANIZATION_SCHEMA_PATH).matches(json));

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(json).when()
                .put(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then().statusCode(200);

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(json).when().put(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then()
                .statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.TEXT).body(json).when()
                .put(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.XML).body(json).when()
                .put(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then().statusCode(500);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormOrganizationByID_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when()
                .put(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.FORM_ORGANIZATION_SCHEMA_PATH))
                .statusCode(200);
        // asserts content type of output for integrity
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).accept(ContentType.TEXT).body(generateSample()).when()
                .put(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).accept(ContentType.XML).body(generateSample()).when()
                .put(FORM_ORGANIZATION_BY_ID_URL, mockDao.getFormID(), mockDao.getFormOrgID()).then().statusCode(500);
    }

    private FormOrganization generateSampleRaw() {
        return DtoHelper.generateOrg(mockDao.getReference(mockDao.getFormID(), MembershipForm.class));
    }

    private String generateSample() {
        try {
            return json.writeValueAsString(generateSampleRaw());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
