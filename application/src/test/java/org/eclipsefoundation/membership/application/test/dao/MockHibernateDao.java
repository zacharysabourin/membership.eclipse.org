package org.eclipsefoundation.membership.application.test.dao;

import javax.enterprise.context.ApplicationScoped;

import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;

import io.quarkus.runtime.Startup;
import io.quarkus.test.Mock;

/**
 * To keep tests separate from datastore, set up a dummy endpoint that returns copies of static data.
 *
 * @author Martin Lowe
 */
@Mock
@Startup
@ApplicationScoped
public class MockHibernateDao extends DefaultHibernateDao {

    /*
     * Test helper methods for quick access to IDs
     */
    public String getFormID() {
        return "form-uuid";
    }

    public String getFormOrgID() {
        return "15";
    }

    public String getFormContactID() {
        return "opearson";
    }

    public String getFormWGID() {
        return "working_group";
    }
}
