package org.eclipsefoundation.membership.application.resources;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.eclipsefoundation.core.model.FlatRequestWrapper;
import org.eclipsefoundation.core.namespace.RequestHeaderNames;
import org.eclipsefoundation.membership.application.dto.MembershipForm;
import org.eclipsefoundation.membership.application.test.dao.MockHibernateDao;
import org.eclipsefoundation.membership.application.test.helper.AuthHelper;
import org.eclipsefoundation.membership.application.test.helper.DtoHelper;
import org.eclipsefoundation.membership.application.test.helper.SchemaNamespaceHelper;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.oidc.Claim;
import io.quarkus.test.security.oidc.ConfigMetadata;
import io.quarkus.test.security.oidc.OidcSecurity;
import io.quarkus.test.security.oidc.UserInfo;
import io.restassured.filter.session.SessionFilter;
import io.restassured.http.ContentType;

@TestInstance(Lifecycle.PER_CLASS)
@QuarkusTest
class MembershipFormResourceTest {
    private static final String FORM_UUID = "form-uuid";
    public static final String FORMS_BASE_URL = "/form";
    public static final String FORMS_BY_ID_URL = FORMS_BASE_URL + "/{id}";

    @Inject
    ObjectMapper json;
    @Inject
    MockHibernateDao mockDao;
    @Inject
    FilterService filters;

    //
    // GET /form
    //
    @Test
    void getForms_requireAuth() {
        // auth is triggered before CSRF (as the request is dumped before it gets
        // processed)
        given().auth().none().when().get(FORMS_BASE_URL).then().statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getForms_csrfGuard() {
        // happens after auth, once the request is processed
        given().when().get(FORMS_BASE_URL).then().statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getForms_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(FORMS_BASE_URL).then().statusCode(200);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getForms_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(FORMS_BASE_URL).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MEMBERSHIP_FORMS_SCHEMA_PATH));
    }

    //
    // GET /form/{id}
    //
    @Test
    void getFormByID_requireAuth() {
        // auth is triggered before CSRF (as the request is dumped before it gets
        // processed)
        given().auth().none().when().get(FORMS_BY_ID_URL, FORM_UUID).then().statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = {
            @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE) }, userinfo = {
                    @UserInfo(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
                    @UserInfo(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, config = {
                            @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormByID_csrfGuard() {
        // happens after auth, once the request is processed
        given().when().get(FORMS_BY_ID_URL, FORM_UUID).then().statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormByID_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(FORMS_BY_ID_URL, FORM_UUID).then().statusCode(200);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = {
            @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE) }, userinfo = {
                    @UserInfo(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
                    @UserInfo(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, config = {
                            @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void getFormByID_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(FORMS_BY_ID_URL, FORM_UUID).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MEMBERSHIP_FORM_SCHEMA_PATH));

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .when().get(FORMS_BY_ID_URL, FORM_UUID).then().statusCode(200);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .accept(ContentType.JSON).when().get(FORMS_BY_ID_URL, FORM_UUID).then().statusCode(200);

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .accept(ContentType.XML).when().get(FORMS_BY_ID_URL, FORM_UUID).then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .accept(ContentType.TEXT).when().get(FORMS_BY_ID_URL, FORM_UUID).then().statusCode(500);
    }

    //
    // POST /form
    //
    @Test
    void postForm_requireAuth() {
        // auth is triggered after CSRF for non GET requests
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .auth().none().contentType(ContentType.JSON).body(generateSample()).when().post(FORMS_BASE_URL).then()
                .statusCode(401);
    }

    @Test
    void postForm_csrfGuard() {
        // happens after auth, once the request is processed
        given().contentType(ContentType.JSON).body(generateSample()).when().post(FORMS_BASE_URL).then().statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postForm_success() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when().post(FORMS_BASE_URL).then()
                .statusCode(200);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postForm_success_pushFormat() {
        // Check that the input matches what is specified in spec
        String json = generateSample();
        Assertions.assertTrue(
                matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MEMBERSHIP_FORM_SCHEMA_PATH).matches(json));
        // json format
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(json).contentType(ContentType.JSON).when().post(FORMS_BASE_URL).then().statusCode(200);

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(json).when().post(FORMS_BASE_URL).then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.TEXT).body(json).when().post(FORMS_BASE_URL).then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.XML).body(json).when().post(FORMS_BASE_URL).then().statusCode(500);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void postForm_success_format() {
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when().post(FORMS_BASE_URL).then().assertThat()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MEMBERSHIP_FORMS_SCHEMA_PATH));

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(generateSample()).when().post(FORMS_BASE_URL).then().assertThat().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.TEXT).body(generateSample()).when().post(FORMS_BASE_URL).then()
                .statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.XML).body(generateSample()).when().post(FORMS_BASE_URL).then().statusCode(500);
    }

    //
    // PUT /form/{id}
    //
    @Test
    void putFormByID_requireAuth() {
        List<MembershipForm> form = mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(MembershipForm.class), null), Arrays.asList(generateRawSample()));
        // auth is triggered after CSRF for non GET requests
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .auth().none().contentType(ContentType.JSON).accept(ContentType.JSON).body(generateSample()).when()
                .put(FORMS_BY_ID_URL, form.get(0).getId()).then().statusCode(401);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormByID_csrfGuard() {
        List<MembershipForm> form = mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(MembershipForm.class), null), Arrays.asList(generateRawSample()));
        // happens after auth, once the request is processed
        given().contentType(ContentType.JSON).accept(ContentType.JSON).body(generateSample()).when()
                .put(FORMS_BY_ID_URL, form.get(0).getId()).then().statusCode(403);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormByID_empty() {
        List<MembershipForm> form = mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(MembershipForm.class), null), Arrays.asList(generateRawSample()));
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).accept(ContentType.JSON).when().put(FORMS_BY_ID_URL, form.get(0).getId())
                .then().statusCode(500);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormByID_success() {
        List<MembershipForm> form = mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(MembershipForm.class), null), Arrays.asList(generateRawSample()));
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .body(generateSample()).contentType(ContentType.JSON).when().put(FORMS_BY_ID_URL, form.get(0).getId())
                .then().statusCode(200);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormByID_success_pushFormat() {
        List<MembershipForm> form = mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(MembershipForm.class), null), Arrays.asList(generateRawSample()));
        // Check that the input matches what is specified in spec
        String json = generateSample();
        Assertions.assertTrue(
                matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MEMBERSHIP_FORM_SCHEMA_PATH).matches(json));

        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).accept(ContentType.JSON).body(json).when()
                .put(FORMS_BY_ID_URL, form.get(0).getId()).then().statusCode(200);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(json).when().put(FORMS_BY_ID_URL, form.get(0).getId()).then()
                .statusCode(200);

        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.TEXT).body(json).when().put(FORMS_BY_ID_URL, form.get(0).getId()).then()
                .statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.XML).body(json).when().put(FORMS_BY_ID_URL, form.get(0).getId()).then()
                .statusCode(500);
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = AuthHelper.DEFAULT_ROLE)
    @OidcSecurity(claims = { @Claim(key = AuthHelper.EMAIL_CLAIM_KEY, value = AuthHelper.EMAIL_CLAIM_VALUE),
            @Claim(key = AuthHelper.GIVEN_NAME_CLAIM_KEY, value = AuthHelper.GIVEN_NAME_CLAIM_VALUE),
            @Claim(key = AuthHelper.FAMILY_NAME_CLAIM_KEY, value = AuthHelper.FAMILY_NAME_CLAIM_VALUE) }, userinfo = {}, config = {
                    @ConfigMetadata(key = AuthHelper.ISSUER_FIELD_KEY, value = AuthHelper.ISSUER_FIELD_VALUE) })
    void putFormByID_success_format() {
        List<MembershipForm> form = mockDao
                .add(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("https://membership.eclipse.org")),
                        filters.get(MembershipForm.class), null), Arrays.asList(generateRawSample()));

        // Check that the output matches what is specified in spec
        SessionFilter sessionFilter = new SessionFilter();
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).accept(ContentType.JSON).body(generateSample()).when()
                .put(FORMS_BY_ID_URL, form.get(0).getId()).then()
                .body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MEMBERSHIP_FORMS_SCHEMA_PATH)).statusCode(200);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).body(generateSample()).when().put(FORMS_BY_ID_URL, form.get(0).getId())
                .then().body(matchesJsonSchemaInClasspath(SchemaNamespaceHelper.MEMBERSHIP_FORMS_SCHEMA_PATH))
                .statusCode(200);
        // asserts content type of output for integrity
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).accept(ContentType.TEXT).body(generateSample()).when()
                .put(FORMS_BY_ID_URL, form.get(0).getId()).then().statusCode(500);
        given().filter(sessionFilter).header(RequestHeaderNames.CSRF_TOKEN, AuthHelper.getCSRFValue(sessionFilter))
                .contentType(ContentType.JSON).accept(ContentType.XML).body(generateSample()).when()
                .put(FORMS_BY_ID_URL, form.get(0).getId()).then().statusCode(500);
    }

    private String generateSample() {
        try {
            return json.writeValueAsString(generateRawSample());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private MembershipForm generateRawSample() {
        return DtoHelper.generateForm(Optional.of(AuthHelper.TEST_USER_NAME));
    }
}
