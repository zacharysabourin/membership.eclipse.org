CREATE TABLE MembershipForm (
  id varchar(255) NOT NULL,
  dateCreated bigint(20) DEFAULT NULL,
  dateUpdated bigint(20) DEFAULT NULL,
  dateSubmitted bigint(20) DEFAULT NULL,
  membershipLevel varchar(255) DEFAULT NULL,
  purchaseOrderRequired varchar(255) DEFAULT NULL,
  registrationCountry varchar(255) DEFAULT NULL,
  signingAuthority bit NOT NULL,
  userID varchar(255) DEFAULT NULL,
  vatNumber varchar(255) DEFAULT NULL,
  state int(10) DEFAULT NULL,
  applicationGroup varchar(255) DEFAULT NULL
);
INSERT INTO MembershipForm(id, dateCreated, dateUpdated, dateSubmitted, membershipLevel, purchaseOrderRequired, registrationCountry, signingAuthority, userID, vatNumber, state, applicationGroup)
  VALUES('form-uuid', 1634668281297, 1634668281297, null, 'Something','yes','CA',0,'opearson','0123456789', '1', 'eclipse');

CREATE TABLE Contact (
  id varchar(255) NOT NULL,
  email varchar(255) DEFAULT NULL,
  fName varchar(255) DEFAULT NULL,
  lName varchar(255) DEFAULT NULL,
  title varchar(255) DEFAULT NULL,
  type varchar(255) DEFAULT NULL,
  form_id varchar(255) DEFAULT NULL
);
INSERT INTO Contact(id, email, fName, lName, title, type, form_id)
  VALUES('opearson', 'test@test.co', 'opearson', 'mctesterson', 'Tester', 'SIGNING', 'form-uuid');

CREATE TABLE FormOrganization (
  id varchar(255) NOT NULL,
  legalName varchar(255) DEFAULT NULL,
  twitterHandle varchar(255) DEFAULT NULL,
  aggregateRevenue varchar(255) DEFAULT NULL,
  organizationType varchar(255) DEFAULT NULL,
  employeeCount varchar(255) DEFAULT NULL,
  form_id varchar(255) DEFAULT NULL
);
INSERT INTO FormOrganization(id, legalName, twitterHandle, aggregateRevenue, organizationType, employeeCount, form_id)
  VALUES('15', 'Sample org','@twitterHandle', 'A lot of money', 'NON_PROFIT_OPEN_SOURCE', '1000-10000', 'form-uuid');

CREATE TABLE FormWorkingGroup (
  id varchar(255) NOT NULL,
  effectiveDate TIMESTAMP(6) DEFAULT NULL,
  participationLevel varchar(255) DEFAULT NULL,
  workingGroupID varchar(255) DEFAULT NULL,
  contact_id varchar(255) DEFAULT NULL,
  form_id varchar(255) DEFAULT NULL
);
INSERT INTO FormWorkingGroup(id, effectiveDate, participationLevel, workingGroupID, contact_id, form_id)
  VALUES('working_group', CURRENT_TIMESTAMP(), 'Participant', 'working.group', 'opearson', 'form-uuid');

CREATE TABLE Address (
  id varchar(255) NOT NULL,
  locality varchar(255) DEFAULT NULL,
  country varchar(255) DEFAULT NULL,
  postalCode varchar(255) DEFAULT NULL,
  administrativeArea varchar(255) DEFAULT NULL,
  addressLine1 varchar(255) DEFAULT NULL,
  addressLine2 varchar(255) DEFAULT NULL,
  organization_id varchar(255) DEFAULT NULL
);
INSERT INTO Address(id, locality, country, postalCode, administrativeArea, addressLine1, addressLine2, organization_id)
  VALUES('address_id', 'Ottawa', 'Canada', 'k1a 1a1', 'ON', 'Street st.', 'Street st.', '15');

CREATE TABLE DistributedCSRFToken (
  `token` varchar(100) NOT NULL,
  `ipAddress` varchar(64) NOT NULL,
  `userAgent` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `user` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`token`)
);