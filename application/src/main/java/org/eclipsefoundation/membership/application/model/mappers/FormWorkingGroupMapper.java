package org.eclipsefoundation.membership.application.model.mappers;

import org.eclipsefoundation.membership.application.dto.FormWorkingGroup;
import org.eclipsefoundation.membership.application.model.FormWorkingGroupData;
import org.eclipsefoundation.membership.application.model.mappers.BaseEntityMapper.QuarkusMappingConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = QuarkusMappingConfig.class, uses = ContactMapper.class)
public interface FormWorkingGroupMapper extends BaseEntityMapper<FormWorkingGroup, FormWorkingGroupData> {

    @Mapping(source = "form.id", target = "formId")
    @Mapping(source = "workingGroupID", target = "workingGroup")
    FormWorkingGroupData toModel(FormWorkingGroup dtoEntity);
}
