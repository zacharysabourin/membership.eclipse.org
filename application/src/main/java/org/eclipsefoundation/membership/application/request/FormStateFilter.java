package org.eclipsefoundation.membership.application.request;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.core.model.RequestWrapper;
import org.eclipsefoundation.membership.application.dto.MembershipForm;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.service.FilterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.identity.SecurityIdentity;
import io.undertow.httpcore.HttpMethodNames;

/**
 * Stops requests to update form objects when the form has already been submitted. Currently reacts to all mutation
 * events under /form/{id}.
 * 
 * @author Martin Lowe
 *
 */
@Provider
public class FormStateFilter implements ContainerRequestFilter {
    public static final Logger LOGGER = LoggerFactory.getLogger(FormStateFilter.class);

    private static final Pattern SPECIFIC_FORM_URI_PATTERN = Pattern.compile("^\\/form\\/((?!state)[^\\/]+)\\/?.*");

    @ConfigProperty(name = "eclipse.api.allowed-admin-roles", defaultValue = "eclipsefdn_membership_portal_admin")
    Instance<List<String>> allowedAdminRoles;
    @ConfigProperty(name = "eclipse.api.application-group")
    Instance<String> applicationGroup;
    @ConfigProperty(name = "eclipse.api.form-group-filter.enabled", defaultValue = "true")
    Instance<Boolean> isEnabled;

    @Inject
    DefaultHibernateDao dao;
    @Inject
    FilterService filters;

    @Inject
    SecurityIdentity identity;
    @Inject
    RequestWrapper wrap;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // if current logged in user is an admin, do not stop request
        if (allowedAdminRoles.stream().flatMap(List<String>::stream).anyMatch(role -> identity.hasRole(role))) {
            return;
        }

        // check if path indicates a specific form
        Matcher m = SPECIFIC_FORM_URI_PATTERN.matcher(requestContext.getUriInfo().getPath());
        if (m.matches()) {
            // get form object to check if it has been submitted
            String formID = m.group(1);

            // get the forms matching ID and check if request should continue
                MembershipForm results = dao.getReference(formID, MembershipForm.class);
            if (Boolean.TRUE.equals(isEnabled.get()) && results != null
                    && !applicationGroup.get().equals(results.getApplicationGroup())) {
                LOGGER.error("Form with ID '{}' cannot be accessed as it belongs to another group. ('{}'){}", formID,
                        results.getApplicationGroup(), results);
                throw new BadRequestException("Form cannot be accessed as it belongs to another group");
            } else if (checkHttpMethod(requestContext) && results != null
                    && !FormState.INPROGRESS.equals(results.getState())) {
                LOGGER.debug("Form with ID '{}' was not updated as it is not in progress. ('{}')", formID,
                        results.getState());
                throw new BadRequestException("Form should not be updated if it is not inprogress");
            }
        }
    }

    private boolean checkHttpMethod(ContainerRequestContext requestContext) {
        String httpMethod = requestContext.getMethod();
        return httpMethod.equalsIgnoreCase(HttpMethodNames.POST) || httpMethod.equalsIgnoreCase(HttpMethodNames.PUT)
                || httpMethod.equalsIgnoreCase(HttpMethodNames.DELETE);
    }
}
