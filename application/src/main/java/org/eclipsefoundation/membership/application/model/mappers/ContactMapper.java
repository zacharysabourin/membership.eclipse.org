package org.eclipsefoundation.membership.application.model.mappers;

import org.eclipsefoundation.membership.application.dto.Contact;
import org.eclipsefoundation.membership.application.model.ContactData;
import org.eclipsefoundation.membership.application.model.mappers.BaseEntityMapper.QuarkusMappingConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = QuarkusMappingConfig.class)
public interface ContactMapper extends BaseEntityMapper<Contact, ContactData> {

    @Mapping(source = "form.id", target = "formId")
    @Mapping(source = "lName", target = "lastName")
    @Mapping(source = "fName", target = "firstName")
    @Mapping(source = "title", target = "jobTitle")
    ContactData toModel(Contact dtoEntity);
}
