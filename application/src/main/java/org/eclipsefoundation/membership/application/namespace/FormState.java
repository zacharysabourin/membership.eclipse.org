package org.eclipsefoundation.membership.application.namespace;

/**
 * Defined membership form states
 * 
 * @author Martin Lowe
 *
 */
public enum FormState {
    SUBMITTED, INPROGRESS, COMPLETE;
}
