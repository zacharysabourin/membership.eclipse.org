/**
 * Copyright (c) 2021 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.resources;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.namespace.RequestHeaderNames;
import org.eclipsefoundation.membership.application.dto.Contact;
import org.eclipsefoundation.membership.application.dto.FormWorkingGroup;
import org.eclipsefoundation.membership.application.dto.MembershipForm;
import org.eclipsefoundation.membership.application.model.FormWorkingGroupData;
import org.eclipsefoundation.membership.application.model.mappers.ContactMapper;
import org.eclipsefoundation.membership.application.model.mappers.FormWorkingGroupMapper;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.Authenticated;

/**
 * Handles organization CRUD requests.
 *
 * @author Martin Lowe
 */
@Authenticated
@Path("form/{id}/working_groups")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class WorkingGroupsResource extends AbstractRESTResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(WorkingGroupsResource.class);

    @Inject
    FormWorkingGroupMapper wgMapper;
    @Inject
    ContactMapper contactMapper;

    @GET
    public Response getWorkingGroups(@PathParam("id") String formID,
            @HeaderParam(value = RequestHeaderNames.CSRF_TOKEN) String csrf) {
        // ensure csrf
        csrfHelper.compareCSRF(csrfHelper.getSessionCSRFToken(aud, request), csrf);
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        // create parameter map
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.FORM_ID.getName(), formID);
        // retrieve the possible object
        List<FormWorkingGroup> results = dao.get(new RDBMSQuery<>(wrap, filters.get(FormWorkingGroup.class), params));
        if (results == null) {
            return Response.serverError().build();
        }
        results.forEach(wg -> LOGGER.debug("Found a form working group entry with id {} for form {}", wg.getId(),
                wg.getForm().getId()));
        // return the results as a response
        return Response.ok(results.stream().map(wgMapper::toModel)).build();
    }

    @POST
    public Response createWorkingGroup(@PathParam("id") String formID, FormWorkingGroupData wgData) {
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        FormWorkingGroup wg = wgMapper.toDTO(wgData, dao);
        MembershipForm form = dao.getReference(formID, MembershipForm.class);
        if (form == null) {
            return Response.status(404).build();
        }
        wg.setForm(form);
        // update the nested contact
        if (wg.getContact() != null) {
            if (wg.getContact().getId() != null) {
                // update the contact object to get entity wg if set
                Contact c = dao.getReference(wg.getContact().getId(), Contact.class);
                wg.setContact(wg.getContact().cloneTo(c));
            }
            // set the form back for wgerences
            wg.getContact().setForm(form);
        }
        return Response.ok(dao.add(new RDBMSQuery<>(wrap, filters.get(FormWorkingGroup.class)), Arrays.asList(wg))
                .stream().map(wgMapper::toModel)).build();
    }

    @GET
    @Path("{wgID}")
    public Response getWorkingGroup(@PathParam("id") String formID, @PathParam("wgID") String wgID,
            @HeaderParam(value = RequestHeaderNames.CSRF_TOKEN) String csrf) {
        // ensure csrf
        csrfHelper.compareCSRF(csrfHelper.getSessionCSRFToken(aud, request), csrf);
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        // create parameter map
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), wgID);
        params.add(MembershipFormAPIParameterNames.FORM_ID.getName(), formID);
        // retrieve the possible object
        List<FormWorkingGroup> results = dao.get(new RDBMSQuery<>(wrap, filters.get(FormWorkingGroup.class), params));
        if (results == null) {
            return Response.serverError().build();
        } else if (results.isEmpty()) {
            LOGGER.debug("Could not find a form working group entry with id {} for form {}", wgID, formID);
            return Response.status(404).build();
        }
        // return the results as a response
        return Response.ok(wgMapper.toModel(results.get(0))).build();
    }

    @PUT
    @Path("{wgID}")
    public Response updateWorkingGroup(@PathParam("id") String formID, FormWorkingGroupData wg,
            @PathParam("wgID") String wgID) {
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        // need to fetch ref to use attached entity
        FormWorkingGroup ref = wgMapper.toDTO(wg, dao).cloneTo(dao.getReference(wgID, FormWorkingGroup.class));
        MembershipForm form = dao.getReference(formID, MembershipForm.class);
        if (form == null) {
            return Response.status(404).build();
        }
        ref.setForm(form);
        // update the nested contact
        if (ref.getContact() != null && wg.getContact() != null) {
            contactMapper.toDTO(wg.getContact(), dao).cloneTo(ref.getContact());
        } else {
            ref.setContact(contactMapper.toDTO(wg.getContact(), dao));
            // set the form back for references
            if (ref.getContact() != null) {
                ref.getContact().setForm(ref.getForm());
            }
        }
        return Response.ok(dao.add(new RDBMSQuery<>(wrap, filters.get(FormWorkingGroup.class)), Arrays.asList(ref))
                .stream().map(wgMapper::toModel)).build();
    }

    @DELETE
    @Path("{wgID}")
    public Response deleteWorkingGroup(@PathParam("id") String formID, @PathParam("wgID") String id) {
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), id);
        params.add(MembershipFormAPIParameterNames.USER_ID.getName(), ident.getPrincipal().getName());

        dao.delete(new RDBMSQuery<>(wrap, filters.get(FormWorkingGroup.class), params));
        return Response.ok().build();
    }
}
