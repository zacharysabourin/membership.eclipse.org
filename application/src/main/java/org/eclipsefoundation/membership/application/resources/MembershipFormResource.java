/**
 * Copyright (c) 2021 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.membership.application.resources;

import java.io.IOException;
import java.io.StringWriter;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.core.namespace.RequestHeaderNames;
import org.eclipsefoundation.membership.application.api.AccountsAPI;
import org.eclipsefoundation.membership.application.api.AccountsAPI.EclipseUser;
import org.eclipsefoundation.membership.application.dto.Contact;
import org.eclipsefoundation.membership.application.dto.FormOrganization;
import org.eclipsefoundation.membership.application.dto.FormStatusReport;
import org.eclipsefoundation.membership.application.dto.FormWorkingGroup;
import org.eclipsefoundation.membership.application.dto.MembershipForm;
import org.eclipsefoundation.membership.application.dto.ValidationGroups.Completion;
import org.eclipsefoundation.membership.application.helper.TimeHelper;
import org.eclipsefoundation.membership.application.model.ConstraintViolationWrapFactory;
import org.eclipsefoundation.membership.application.model.ConstraintViolationWrapFactory.ConstraintViolationWrap;
import org.eclipsefoundation.membership.application.model.FormMailerData;
import org.eclipsefoundation.membership.application.model.FormStatusReportItem;
import org.eclipsefoundation.membership.application.namespace.FormState;
import org.eclipsefoundation.membership.application.namespace.MembershipFormAPIParameterNames;
import org.eclipsefoundation.membership.application.service.MailerService;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import io.quarkus.security.Authenticated;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;

/**
 * Handles membership form CRUD requests.
 *
 * @author Martin Lowe
 */
@Authenticated
@Path("form")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MembershipFormResource extends AbstractRESTResource {
    public static final Logger LOGGER = LoggerFactory.getLogger(MembershipFormResource.class);

    @ConfigProperty(name = "eclipse.api.application-group")
    String applicationGroup;
    @ConfigProperty(name = "eclipse.api.allow-list.csv-view")
    List<String> allowListCsvView;

    @Inject
    Validator validator;
    @Inject
    MailerService mailer;

    @RestClient
    @Inject
    AccountsAPI accounts;

    final CsvMapper CSV_MAPPER = (CsvMapper) new CsvMapper().registerModule(new JavaTimeModule())
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    @GET
    public Response getAll(@HeaderParam(value = RequestHeaderNames.CSRF_TOKEN) String csrf) {
        // ensure csrf
        csrfHelper.compareCSRF(csrfHelper.getSessionCSRFToken(aud, request), csrf);

        // create parameter map
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.USER_ID.getName(), ident.getPrincipal().getName());
        // retrieve the possible cached object
        List<MembershipForm> results = dao.get(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class), params));
        if (results == null) {
            return Response.serverError().build();
        }
        // return the results as a response
        return Response.ok(results).build();
    }

    @GET
    @Path("{id}")
    public Response get(@PathParam("id") String formID,
            @HeaderParam(value = RequestHeaderNames.CSRF_TOKEN) String csrf) {
        // ensure csrf
        csrfHelper.compareCSRF(csrfHelper.getSessionCSRFToken(aud, request), csrf);
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        // create parameter map
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), formID);

        // retrieve the possible cached object
        List<MembershipForm> results = dao.get(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class), params));
        if (results == null) {
            return Response.serverError().build();
        } else if (results.isEmpty()) {
            return Response.status(404).build();
        }
        // return the results as a response
        return Response.ok(results.get(0)).build();
    }

    @POST
    public List<MembershipForm> create(MembershipForm mem) {
        mem.setUserID(ident.getPrincipal().getName());
        mem.setDateCreated(TimeHelper.getMillis());
        mem.setDateUpdated(mem.getDateCreated());
        mem.setApplicationGroup(applicationGroup);
        return dao.add(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class)), Arrays.asList(mem));
    }

    @PUT
    @Path("{id}")
    public Response update(@PathParam("id") String formID, MembershipForm mem) {
        // make sure we have something to put
        if (mem == null) {
            return Response.status(500).build();
        }
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        mem.setUserID(ident.getPrincipal().getName());
        mem.setDateUpdated(TimeHelper.getMillis());
        mem.setApplicationGroup(applicationGroup);
        // need to fetch ref to use attached entity
        MembershipForm ref = mem.cloneTo(dao.getReference(formID, MembershipForm.class));
        return Response.ok(dao.add(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class)), Arrays.asList(ref)))
                .build();
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") String formID) {
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        // standard form params
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), formID);

        // FK dependents params
        MultivaluedMap<String, String> depParams = new MultivaluedMapImpl<>();
        depParams.add(MembershipFormAPIParameterNames.FORM_ID.getName(), formID);
        dao.delete(new RDBMSQuery<>(wrap, filters.get(FormWorkingGroup.class), depParams));
        dao.delete(new RDBMSQuery<>(wrap, filters.get(Contact.class), depParams));
        dao.delete(new RDBMSQuery<>(wrap, filters.get(FormOrganization.class), depParams));
        dao.delete(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class), params));
        return Response.ok().build();
    }

    @POST
    @Path("{id}/complete")
    public Response completeForm(@PathParam("id") String formID, @QueryParam("force") boolean force) {
        // check if user is allowed to modify these resources
        Response r = checkAccess(formID);
        if (r != null) {
            return r;
        }
        // create parameter map
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(DefaultUrlParameterNames.ID.getName(), formID);

        // retrieve the membership form for the current post
        List<MembershipForm> results = dao.get(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class), params));
        if (results == null) {
            return Response.serverError().build();
        } else if (results.isEmpty()) {
            return Response.status(404).build();
        } else if (!force && (FormState.SUBMITTED.equals(results.get(0).getState())
                || FormState.COMPLETE.equals(results.get(0).getState()))) {
            // dont send email if force param is not true and form already submitted
            return Response.status(204).build();
        }
        MembershipForm mf = results.get(0);

        // retrieve all of the info needed to post the form email
        MultivaluedMap<String, String> extraparams = new MultivaluedMapImpl<>();
        extraparams.add(MembershipFormAPIParameterNames.FORM_ID.getName(), formID);
        List<FormOrganization> orgs = dao.get(new RDBMSQuery<>(wrap, filters.get(FormOrganization.class), extraparams));
        List<FormWorkingGroup> wgs = dao.get(new RDBMSQuery<>(wrap, filters.get(FormWorkingGroup.class), extraparams));
        List<Contact> contacts = dao.get(new RDBMSQuery<>(wrap, filters.get(Contact.class), extraparams));

        // validate form elements
        Set<ConstraintViolationWrap> violations = new LinkedHashSet<>();
        violations.addAll(recordViolations(results));
        violations.addAll(recordViolations(orgs));
        violations.addAll(recordViolations(wgs));
        violations.addAll(recordViolations(contacts));
        // check that there are no validation violations for form elements
        if (!violations.isEmpty()) {
            return Response.status(Status.BAD_REQUEST.getStatusCode()).entity(violations).build();
        }

        // send the forms to the mailing service
        FormMailerData data = new FormMailerData(generateName((DefaultJWTCallerPrincipal) ident.getPrincipal()), mf,
                !orgs.isEmpty() ? orgs.get(0) : null, wgs, contacts);
        mailer.sendToFormAuthor(data);
        mailer.sendToMembershipTeam(data);

        // update the state and push the update
        mf.setDateSubmitted(TimeHelper.getMillis());
        mf.setDateUpdated(mf.getDateSubmitted());
        mf.setState(FormState.SUBMITTED);
        return Response.ok(dao.add(new RDBMSQuery<>(wrap, filters.get(MembershipForm.class)), Arrays.asList(mf)))
                .build();
    }

    @GET
    @Path("state/{state}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getActive(@PathParam("state") FormState state) throws IOException {
        // check if allow listed user is requesting data
        if (!allowListCsvView.contains(ident.getPrincipal().getName())) {
            return Response.status(403).build();
        }
        // create parameter map for our form state
        MultivaluedMap<String, String> params = new MultivaluedMapImpl<>();
        params.add(MembershipFormAPIParameterNames.FORM_STATE.getName(), state.name());
        // generate CSV output
        try (StringWriter sw = new StringWriter()) {
            // create the CSV writer for membership forms
            SequenceWriter writer = CSV_MAPPER.writer(CSV_MAPPER.schemaFor(FormStatusReportItem.class).withHeader())
                    .writeValues(sw);
            // create query to get all forms (no limit)
            RDBMSQuery<FormStatusReport> formQuery = new RDBMSQuery<>(wrap, filters.get(FormStatusReport.class),
                    params);
            formQuery.setUseLimit(false);
            // retrieve the membership form for the current state and write to output
            writer.writeAll(generateReportItems(dao.get(formQuery)));
            return Response.ok(sw.toString()).build();
        }
    }

    private List<FormStatusReportItem> generateReportItems(List<FormStatusReport> formItems) {
        return formItems.stream().map(f -> {
            // get the user from cache for current form
            Optional<EclipseUser> user = cache.get(f.getUserId(), new MultivaluedMapImpl<>(), EclipseUser.class,
                    () -> accounts.getUsers(f.getUserId()));
            return FormStatusReportItem.builder().setWorkingGroupCount(f.getWorkingGroupCount())
                    .setUserEmail(user.isPresent() ? user.get().getMail() : "")
                    .setFullName(user.isPresent() ? user.get().getFullName() : "")
                    .setDateCreated(
                            ZonedDateTime.ofInstant(Instant.ofEpochMilli(f.getDateCreated()), ZoneId.systemDefault()))
                    .setDateUpdated(f.getDateUpdated() != null
                            ? ZonedDateTime.ofInstant(Instant.ofEpochMilli(f.getDateUpdated()), ZoneId.systemDefault())
                            : null)
                    .setDateSubmitted(
                            f.getDateSubmitted() != null
                                    ? ZonedDateTime.ofInstant(Instant.ofEpochMilli(f.getDateSubmitted()),
                                            ZoneId.systemDefault())
                                    : null)
                    .setFormId(f.getFormId()).setUserId(f.getUserId()).setMembershipLevel(f.getMembershipLevel())
                    .setRegistrationCountry(f.getRegistrationCountry()).setState(f.getFormState())
                    .setOrganizationName(f.getOrganizationName()).setFormCompletionStage(f.getFormCompletionStage())
                    .build();
        }).collect(Collectors.toList());
    }

    private <T extends BareNode> Set<ConstraintViolationWrap> recordViolations(List<T> items) {
        ConstraintViolationWrapFactory factory = new ConstraintViolationWrapFactory();
        return items.stream().flatMap(item -> factory.build(validator.validate(item, Completion.class)).stream())
                .collect(Collectors.toSet());
    }

    private String generateName(DefaultJWTCallerPrincipal defaultPrin) {
        return new StringBuilder().append((String) defaultPrin.getClaim("given_name")).append(" ")
                .append((String) defaultPrin.getClaim("family_name")).toString();
    }
}
