package org.eclipsefoundation.membership.application.api;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.membership.application.model.WorkingGroupMap.WorkingGroup;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

@Path("working_groups")
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "membership-api")
public interface MembershipAPI {

    Response getWorkingGroups(@QueryParam("page") int page);

    @Path("{name}")
    WorkingGroup getWorkingGroup(@PathParam("name") String name);

}
