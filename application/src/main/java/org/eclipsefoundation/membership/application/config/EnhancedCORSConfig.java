package org.eclipsefoundation.membership.application.config;

import java.util.List;
import java.util.Map;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

@ConfigMapping(prefix = "eclipse.cors")
public interface EnhancedCORSConfig {
    @WithDefault("false")
    boolean enabled();

    Map<String, EnhancedCORSSet> config();

    public static interface EnhancedCORSSet {
        List<String> endpointPatterns();

        List<String> origins();

        List<String> methods();
    }
}
