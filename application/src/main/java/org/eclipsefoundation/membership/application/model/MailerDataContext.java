package org.eclipsefoundation.membership.application.model;

import java.time.LocalDateTime;

import io.quarkus.qute.TemplateData;

@TemplateData
public class MailerDataContext {
    public final String group;
    public final LocalDateTime now;
    public final boolean isPDF;
    public final boolean includePreamble;

    public MailerDataContext(String group, LocalDateTime now, boolean isPDF, boolean includePreamble) {
        this.group = group;
        this.now = now;
        this.isPDF = isPDF;
        this.includePreamble = includePreamble;
    }
}
