package org.eclipsefoundation.membership.application.model;

import java.time.OffsetDateTime;

import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_FormWorkingGroupData.Builder.class)
public abstract class FormWorkingGroupData {
    @Nullable
    public abstract String getId();
    @Nullable
    public abstract String getFormId();
    public abstract String getParticipationLevel();
    public abstract OffsetDateTime getEffectiveDate();
    public abstract String getWorkingGroup();
    public abstract ContactData getContact();
    
    public static Builder builder() {
        return new AutoValue_FormWorkingGroupData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setId(@Nullable String id);
        public abstract Builder setFormId(@Nullable String formId);
        public abstract Builder setParticipationLevel(String participationLevel);
        public abstract Builder setEffectiveDate(OffsetDateTime effectiveDate);
        public abstract Builder setWorkingGroup(String workingGroup);
        public abstract Builder setContact(ContactData contact);
        public abstract FormWorkingGroupData build();
    }
}
