package org.eclipsefoundation.membership.application.model;

import javax.annotation.Nullable;

import org.eclipsefoundation.membership.application.namespace.ContactTypes;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ContactData.Builder.class)
public abstract class ContactData {
    @Nullable
    public abstract String getId();
    @Nullable
    public abstract String getFormId();
    public abstract String getFirstName();
    public abstract String getLastName();
    public abstract String getJobTitle();
    public abstract String getEmail();
    public abstract ContactTypes getType();
    
    public static Builder builder() {
        return new AutoValue_ContactData.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setId(@Nullable String id);
        public abstract Builder setFormId(@Nullable String formId);
        public abstract Builder setFirstName(String firstName);
        public abstract Builder setLastName(String lastName);
        public abstract Builder setJobTitle(String jobTitle);
        public abstract Builder setEmail(String email);
        public abstract Builder setType(ContactTypes type);
        public abstract ContactData build();
    }
}
