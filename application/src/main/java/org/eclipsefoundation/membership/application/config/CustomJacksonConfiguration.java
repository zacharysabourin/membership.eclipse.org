package org.eclipsefoundation.membership.application.config;

import java.text.SimpleDateFormat;

import javax.inject.Singleton;

import org.eclipsefoundation.core.helper.DateTimeHelper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;

import io.quarkus.jackson.ObjectMapperCustomizer;

/**
 * Sets up Jackson with default API formatting standards.
 * 
 * @author Martin Lowe
 *
 */
@Singleton
public class CustomJacksonConfiguration implements ObjectMapperCustomizer {

    @Override
    public void customize(ObjectMapper objectMapper) {
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategies.SNAKE_CASE)
                .setDateFormat(new SimpleDateFormat(DateTimeHelper.RAW_RFC_3339_FORMAT));
    }

}
