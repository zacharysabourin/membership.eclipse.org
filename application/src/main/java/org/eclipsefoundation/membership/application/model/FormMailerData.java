package org.eclipsefoundation.membership.application.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipsefoundation.membership.application.dto.Contact;
import org.eclipsefoundation.membership.application.dto.FormOrganization;
import org.eclipsefoundation.membership.application.dto.FormWorkingGroup;
import org.eclipsefoundation.membership.application.dto.MembershipForm;

import io.quarkus.qute.TemplateData;

@TemplateData
public class FormMailerData {
    public final String name;
    public final MembershipForm form;
    public final FormOrganization org;
    public final List<FormWorkingGroup> wgs;
    public final List<Contact> contacts;

    public FormMailerData(String name, MembershipForm form, FormOrganization org, List<FormWorkingGroup> wgs,
            List<Contact> contacts) {
        this.name = name;
        this.form = form;
        this.org = org;
        this.wgs = Collections.unmodifiableList(wgs != null ? wgs : new ArrayList<>());
        this.contacts = Collections.unmodifiableList(contacts != null ? contacts : new ArrayList<>());
    }
}
