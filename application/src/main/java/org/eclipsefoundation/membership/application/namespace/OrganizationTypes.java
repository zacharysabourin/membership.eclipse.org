package org.eclipsefoundation.membership.application.namespace;

/**
 * Represents the different types of organizations that may be part of the memebership application process.
 * 
 * @author Martin Lowe
 *
 */
public enum OrganizationTypes {
    NON_PROFIT_OPEN_SOURCE, ACADEMIC, STANDARDS, GOVERNMENT_ORGANIZATION_AGENCY_NGO, MEDIA_ORGANIZATION, RESEARCH,
    OTHER;
}
