package org.eclipsefoundation.membership.application.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.membership.application.model.FormMailerData;
import org.eclipsefoundation.membership.application.model.MailerDataContext;
import org.eclipsefoundation.membership.application.service.MailerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openhtmltopdf.outputdevice.helper.BaseRendererBuilder.PageSizeUnits;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import io.quarkus.qute.Location;
import io.quarkus.qute.Template;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;
import io.smallrye.jwt.auth.principal.DefaultJWTCallerPrincipal;

/**
 * Default implementation of the mailer service using the Qute templating engine and the mailer extensions built into
 * Quarkus.
 * 
 * @author Martin Lowe
 *
 */
@Dependent
public class DefaultMailerService implements MailerService {
    public static final Logger LOGGER = LoggerFactory.getLogger(DefaultMailerService.class);

    private static final String EMAIL_DATA_VAR = "data";
    private static final String CONTEXT_VAR = "context";

    @ConfigProperty(name = "eclipse.api.application-group")
    String applicationGroup;

    @Inject
    EclipseMailerConfig config;
    @Inject
    SecurityIdentity ident;
    @Inject
    Mailer mailer;

    // Qute templates, generates email bodies
    @Location("emails/membership/form_membership_email_web_template")
    Template membershipTemplateWeb;
    @Location("emails/membership/form_membership_email_template")
    Template membershipTemplate;

    @Override
    public void sendToFormAuthor(FormMailerData data) {
        if (data == null || data.form == null) {
            throw new IllegalStateException("A form is required to submit for mailing");
        } else if (data.org == null || data.wgs == null || data.contacts == null || data.contacts.isEmpty()) {
            throw new IllegalStateException(
                    "Could not find a fully complete form for form with ID '" + data.form.getId() + "'");
        } else if (ident.isAnonymous()) {
            // in the future we should fall back to accounts API
            throw new IllegalStateException("A user must be logged in to send the author form message");
        }
        // convert the logged in user into a JWT token to read user claims
        DefaultJWTCallerPrincipal defaultPrin = (DefaultJWTCallerPrincipal) ident.getPrincipal();
        Mail m = getMembershipFormMail(defaultPrin.getClaim("email"),
                "Thank you for completing the member enrollment form", data, true);
        // add reply to and bcc if set
        config.membership().authorMessage().bcc().ifPresent(m::setBcc);
        config.membership().authorMessage().replyTo().ifPresent(m::setReplyTo);
        mailer.send(m);
    }

    @Override
    public void sendToMembershipTeam(FormMailerData data) {
        if (data == null || data.form == null) {
            throw new IllegalStateException("A form is required to submit for mailing");
        } else if (data.org == null || data.wgs == null || data.contacts == null || data.contacts.isEmpty()) {
            throw new IllegalStateException(
                    "Could not find a fully complete form for form with ID '" + data.form.getId() + "'");
        }
        Mail m = getMembershipFormMail(config.membership().inbox(),
                "New Request to join working group(s) - " + data.name, data, false);
        // add BCC + reply to fields if set
        config.membership().membershipMessage().bcc().ifPresent(m::setBcc);
        config.membership().membershipMessage().replyTo().ifPresent(m::setReplyTo);
        // add the PDF attachment
        m.addAttachment("membership-enrollment-" + data.name.toLowerCase().replace(" ", "-") + ".pdf",
                renderHTMLPDF(
                        membershipTemplateWeb
                                .data(EMAIL_DATA_VAR, data, CONTEXT_VAR,
                                        new MailerDataContext(applicationGroup, LocalDateTime.now(), true, false))
                                .render()),
                "application/pdf");
        mailer.send(m);
    }

    /**
     * Centralize the creation of the mail object to reduce repetition. A preamble may be included at the top of the
     * message based on the includePreamble argument.
     * 
     * @param recipient the recipient of the mail message
     * @param subject the subject line for the message
     * @param data the collected form data that is the base of the email.
     * @param includePreamble whether or not to include the preamble in the email.
     * @return the mail message with text and HTML versions set.
     */
    private Mail getMembershipFormMail(String recipient, String subject, FormMailerData data, boolean includePreamble) {
        // generate the mail message, sending the messsage to the membershipMailbox
        Mail m = Mail.withHtml(recipient, subject,
                membershipTemplateWeb
                        .data(EMAIL_DATA_VAR, data, CONTEXT_VAR,
                                new MailerDataContext(applicationGroup, LocalDateTime.now(), false, includePreamble))
                        .render());
        m.setText(
                membershipTemplate
                        .data(EMAIL_DATA_VAR, data, CONTEXT_VAR,
                                new MailerDataContext(applicationGroup, LocalDateTime.now(), false, includePreamble))
                        .render());
        return m;
    }

    /**
     * Render the PDF document using HTML generated by the Qute template engine.
     * 
     * @param html the HTML for the PDF document.
     * @return the file that represents the document. This should be in a temporary directory so that it gets cleaned on
     * occasion.
     */
    private File renderHTMLPDF(String html) {
        // create a unique file name for temporary storage
        File f = new File(config.membership().docStorageRoot() + '/' + UUID.randomUUID().toString() + ".pdf");
        try (OutputStream os = new FileOutputStream(f)) {
            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.withHtmlContent(html, config.membership().docStorageRoot());
            // using a4 measurements
            builder.useDefaultPageSize(210, 297, PageSizeUnits.MM);
            builder.toStream(os);
            builder.run();
        } catch (IOException e) {
            throw new RuntimeException("Could not build PDF document", e);
        }
        return f;
    }

    /**
     * Represents configuration for the default mailer service.
     * 
     * @author Martin Lowe
     *
     */
    @ConfigMapping(prefix = "eclipse.mailer")
    public interface EclipseMailerConfig {
        public Membership membership();

        /**
         * Represents configurations for the membership form messages.
         */
        public interface Membership {
            public String inbox();

            public MessageConfiguration authorMessage();

            public MessageConfiguration membershipMessage();

            @WithDefault("/tmp")
            public String docStorageRoot();
        }

        public interface MessageConfiguration {
            public Optional<String> replyTo();

            public Optional<List<String>> bcc();
        }
    }

}
