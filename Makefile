SHELL = /bin/bash
compile-java:compile-test-resources;
	mvn compile package
compile-java-quick: ;
	mvn compile package -Dmaven.test.skip=true
compile-react: install-react;
	yarn --cwd src/main/www build
compile: clean compile-react compile-java;
compile-test-resources:;
	yarn --cwd application run generate-json-schema
	yarn --cwd portal run generate-json-schema
compile-quick: clean compile-react compile-java-quick;
clean:;
	mvn clean
	yarn --cwd application run clean
	yarn --cwd portal run clean
install-react:;
	yarn --cwd src/main/www install --frozen-lockfile --audit
	yarn --cwd application install --frozen-lockfile
	yarn --cwd portal install --frozen-lockfile
generate-cert:;
	rm -rf certs && mkdir -p certs
	openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout certs/www.rem.docker.key -out certs/www.rem.docker.crt
compile-start: compile-quick;
	docker-compose down
	docker-compose build
	docker-compose up
pre-setup:;
	@echo "Creating environment file from template"
	@rm -f .env && envsubst < config/.env.sample > .env
setup:;
	@echo "Generating secret files from templates using environment file + variables"
	@source .env && rm -f ./config/application/secret.properties && envsubst < config/application/secret.properties.sample > config/application/secret.properties
	@source .env && rm -f ./config/portal/secret.properties && envsubst < config/portal/secret.properties.sample > config/portal/secret.properties
	@source .env && rm -f ./config/foundationdb/secret.properties && envsubst < config/foundationdb/secret.properties.sample > config/foundationdb/secret.properties
	@mkdir -p volumes/imagestore
init-dev: pre-setup setup;
	@echo "Pre-setup complete"